# Fxt iOS App

This is Fxt iOS app, customer side app for home service bookings.

## Getting Started

Pull the code from the repository and follow the instructions to get the app running on local system.

### Get it up and running

To get any ionic app running on your system you need to install [node](https://nodejs.org/en/download/) on your system.

After installing node, run the following command on command prompt (windows) or in terminal (Mac). This will install ionic on your system.
```
npm install -g ionic
```
After installing ionic, change the current directory to your project folder (where 'www' folder resides) and run
```
npm install -g bower
bower install
```
This will install the required dependencies on the system

Run following command to install required plugins
```
ionic state restore --plugins
```
If the above command does not install all the plugins, install them one at a time using following command in the terminal
```
cordova plugin add {plugin id or url}
```
where, {plugin id or url} are as given below
* **cordova-plugin-compat** 
* **cordova-plugin-file** - 
* **ionic-plugin-file-transfer** 
* **cordova-plugin-media-capture**
* **cordova-plugin-splashscreen**
* **cordova-plugin-whitelist**

Example: 
```cordova plugin add cordova-plugin-device```


Run following command to install ios platform on the system
```
ionic platform add ios
```

You should have a resources folder in the root folder having an 'icon.png' and 'splash.png' for the app. Run following command to generate icons and splash screens for respective platforms
```
ionic resources
```
Run following command to run the app on device 
```
ionic run ios -device
```
OR
```
ionic run ios
```
to run it on default emulator. To run it on chrome browser use
```
ionic serve --lab
```
