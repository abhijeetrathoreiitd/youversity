// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in bookingCtrl.js
angular.module('starter', ['ionic', 'ionic.closePopup', 'ngResource', 'ngCordova', 'LocalForageModule', 'timer',
    'ngCordovaOauth', 'ngSanitize', 'chart.js'])

    .run(function ($ionicPlatform, $rootScope, $state, $ionicSideMenuDelegate, $ionicLoading, $ionicPopup,
                   loggedInUser, logoutService, loginService, $localForage, fetchedVideoInfo, sportsExperience, sportsPreExperience,
                   mySports, IonicClosePopupService, TagsAllLevel, allLevelTagsList, SportsList, Sports, backend, experienceList, preexperienceList) {
        // $localForage.clear();

        $rootScope.showfeedbackDirective=true;
        $rootScope.backend = backend;
        $ionicPlatform.ready(function () {
            AWS.config.update({
                accessKeyId: 'AKIAJLU25P6MIP34PTAQ',
                secretAccessKey: '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'
            });
            // new AWS.S3().listObjects({Bucket: 'config-bucket-570600719000'}, function(error, data) {
            //     if (error) {
            //         console.log(error); // an error occurred
            //     } else {
            //         console.log(data); // request succeeded
            //     }
            // });


            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);

            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }
        });


        experienceList.query(function (data) {
            console.log(data)
            sportsExperience.list = data;
        }, function (err) {
            console.log(err);
        })

        preexperienceList.query(function (data) {
            sportsPreExperience.list = data;
        }, function (err) {
            console.log(err);
        })

        $localForage.getItem('loggedInUser')
            .then(function (data) {
                if (window.cordova) {
                    navigator.splashscreen.hide();
                }
                $ionicLoading.show({'template': 'Loading'});
                if (data != null) {
                    loggedInUser.info = data;
                    loggedInUser.info.mySports = _.map(loggedInUser.info.sportsExperience, 'sportId');
                    loginService.login();

                    //if(data.player)
                    //    $state.go('sidemenu.chooseSport');
                    //else
                    //    $state.go('coachSidemenu.chooseSport')

                }
                else {
                    $ionicLoading.hide();

                }
            }).then(function () {

            $localForage.getItem('allLevelTagsList').then(function (forageData) {
                if (forageData == null) {
                    TagsAllLevel.get(function (data) {

                        $localForage.setItem('allLevelTagsList', data).then(function (done) {
                            //add to factory here
                            allLevelTagsList.list = data;
                        })
                    })

                }
                else {

                    allLevelTagsList.list = forageData;
                    //add to factory here
                }

            })

            Sports.query(function (data) {
                SportsList.list = data;
                console.log(SportsList.list)
            }, function (err) {
                console.log(err);
            })
        });
        document.addEventListener("deviceready", onDeviceReady, false);
        function onDeviceReady() {
            if (device.platform == "Android") {
                $rootScope.android = true;
            }
            if (device.platform == "iOS") {
                $rootScope.ios = true;
            }
        }

        $rootScope.showTabs = true;
        $rootScope.goToLogin = function () {
            logoutService.logout();
        };
        $rootScope.goToFaq = function () {
            $state.go('sidemenu.faq');
            $ionicSideMenuDelegate.toggleLeft();
        };
        $rootScope.showLeftMenu = function () {
            $ionicSideMenuDelegate.toggleLeft();
        };
        //$rootScope.sportSelected = false;
        $rootScope.imageSelected = false;
        $rootScope.videoSelected = false;
        $rootScope.keyboardOpen = false;
        $rootScope.registerAsCoach = true;
        $rootScope.registerAsBoth = false;

        $rootScope.tabState = 1;
        // $rootScope.player = true;
        // $rootScope.selectPlayer = function () {
        //     $rootScope.player = true;
        // }
        // $rootScope.selectCoach = function () {
        //     $rootScope.player = false;
        // }
        $rootScope.showPaid = true;

        $rootScope.selectSport = function () {
            var options =
            {
                title: 'Choose a sport',
                cssClass: 'sportsPickPopup', // String, The custom CSS class name
                subTitle: 'You have following sports in your profile', // String (optional). The sub-title of the popup.
                template: '<div pick-sport></div>',
            }
            alertPopup = $ionicPopup.alert(options);

            IonicClosePopupService.register(alertPopup);

        }
        $rootScope.selectImage = function (subtitle) {
            var options =
            {
                title: 'Choose an Image',
                cssClass: 'sportsPickPopup', // String, The custom CSS class name
                subTitle: '<span class="gray">' + subtitle + '</span>', // String (optional). The sub-title of the popup.
                template: '<div pick-image></div>',
            }
            imageAlertPopup = $ionicPopup.alert(options);

            IonicClosePopupService.register(imageAlertPopup);

        }

        $rootScope.stateChange = function (state) {
            $state.go(state);
        }

    })

    .config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
        $ionicConfigProvider.tabs.position('bottom')
        $ionicConfigProvider.views.maxCache(0);
        $ionicConfigProvider.views.transition('ios');
        // Ionic uses AngularUI Router which uses the concept of states
        // Learn more here: https://github.com/angular-ui/ui-router
        // Set up the various states which the app can be in.
        // Each state's controller can be found in bookingCtrl.js
        $stateProvider

        // setup an abstract state for the tabs directive

            .state('sidemenu.chooseSport', {
                url: '/choosesport',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/player/choosesport.html',
                        controller: 'chooseSportCtrl',
                        controllerAs: 'choose'
                    }
                }, params: {showButton: ''}
            })
            .state('sidemenu.login', {
                url: '/login',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/player/login.html',
                        controller: 'loginCtrl',
                        controllerAs: 'login'
                    }
                }
            })
            .state('sidemenu.signup', {
                url: '/signup',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/player/signup.html',
                        controller: 'signupCtrl',
                        controllerAs: 'signup'
                    }
                }
            })
            .state('sidemenu.profile', {
                url: '/profile',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/player/profile.html',
                        controller: 'profileCtrl',
                        controllerAs: 'profile'
                    }
                }
            })

            .state('sidemenu', {
                url: "/player/sidemenu",
                abstract: true,
                templateUrl: "templates/player/sidemenu.html"
            })
            .state('sidemenu.myProfile', {
                url: '/myprofile',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/player/myprofile.html',
                        controller: 'myProfileCtrl',
                        controllerAs: 'myProfile'
                    }
                }
            })
            .state('sidemenu.findPlayer', {
                url: '/findplayer',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/player/findplayer.html',
                        controller: 'findPersonCtrl',
                        controllerAs: 'findPerson'
                    }
                }
            })
            .state('sidemenu.playerPlayerProfile', {
                url: '/playerprofile',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/player/playerProfile.html',
                        controller: 'playerProfileCtrl',
                        controllerAs: 'playerProfile'
                    }
                }
            })
            .state('sidemenu.inbox', {
                url: '/inbox',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/player/inbox.html',
                        controller: 'inboxCtrl',
                        controllerAs: 'inbox'
                    }
                }
            })
            .state('sidemenu.followers', {
                url: '/followers',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/player/followers.html',
                        controller: 'followersCtrl',
                        controllerAs: 'followers'
                    }
                }
            })
            .state('sidemenu.following', {
                url: '/following',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/player/following.html',
                        controller: 'followingCtrl',
                        controllerAs: 'following'
                    }
                }
            })
            .state('sidemenu.selectForReview', {
                url: '/selectforreview',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/player/selectForReview.html',
                        controller: 'followingCtrl',
                        controllerAs: 'following'
                    }
                }
            })
            .state('sidemenu.faq', {
                url: '/faq',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/player/faq.html',
                        controller: 'faqCtrl',
                        controllerAs: 'faq'
                    }
                }
            })
            .state('sidemenu.chat', {
                url: '/chat',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/player/chat.html',
                        controller: 'chatCtrl',
                        controllerAs: 'chat'
                    }
                }
            })
            .state('sidemenu.tabs', {
                url: '/tabs',
                views: {
                    'menuContent': {
                        templateUrl: "templates/player/tabs.html",
                        controller: 'tabsCtrl'
                    }
                }
            })
            .state('sidemenu.tabs.home', {
                url: '/home',
                views: {
                    'home-tab': {
                        templateUrl: "templates/player/home.html",
                        controller: 'homeCtrl',
                        controllerAs: 'home'
                    }
                }
            })
            .state('sidemenu.tabs.training', {
                url: '/training',
                views: {
                    'training-tab': {
                        templateUrl: "templates/player/training.html",
                        controller: 'trainingCtrl',
                        controllerAs: 'training'
                    }
                },
                params: {
                    openTab: ''
                }
            })
            .state('sidemenu.tabs.trainingDetail', {
                url: '/trainingdetail',
                views: {
                    'training-tab': {
                        templateUrl: "templates/player/trainingDetail.html",
                        controller: 'trainingDetailCtrl',
                        controllerAs: 'trainingDetail'
                    }
                },
                params: {
                    trainingPlan: {}
                }
            })
            .state('sidemenu.tabs.mytrainingDetail', {
                url: '/mytrainingdetail',
                views: {
                    'training-tab': {
                        templateUrl: "templates/player/mytrainingdetail.html",
                        controller: 'myTrainingDetailCtrl',
                        controllerAs: 'myTraining'
                    }
                }
            })
            // .state('sidemenu.tabs.mytrainingDetail', {
            //     url: '/mytrainingdetail',
            //     views: {
            //         'training-tab': {
            //             templateUrl: "templates/player/mytrainingdetail.html",
            //             controller: 'myTrainingDetailCtrl',
            //             controllerAs: 'myTraining'
            //         }
            //     }
            // })
            .state('sidemenu.tabs.findCoach', {
                url: '/findcoach',
                views: {
                    'find-coach-tab': {
                        templateUrl: "templates/player/findCoach.html",
                        controller: 'findPersonCtrl',
                        controllerAs: 'findPerson'
                    }
                }
            })

            .state('sidemenu.tabs.playerCoachProfile', {
                url: '/coachprofile',
                views: {
                    'find-coach-tab': {
                        templateUrl: "templates/player/coachprofile.html",
                        controller: 'coachProfileCtrl',
                        controllerAs: 'coachProfile'
                    }
                }

            })
            .state('coachSidemenu.coachCoachProfile', {
                url: '/coachprofile',
                views: {
                    'menuContent': {
                        templateUrl: "templates/coach/coachprofile.html",
                        controller: 'coachProfileCtrl',
                        controllerAs: 'coachProfile'
                    }
                }

            })
            .state('sidemenu.tabs.videoLib', {
                url: '/videolib',
                views: {
                    'video-lib': {
                        templateUrl: "templates/player/videolib.html",
                        controller: 'videoLibCtrl',
                        controllerAs: 'videoLib'
                    }
                },
                params: {
                    activeTab: ''
                }
            })
            .state('sidemenu.tabs.playVideo', {
                url: '/playvideo',
                views: {
                    'video-lib': {
                        templateUrl: "templates/player/playvideo.html",
                        controller: 'playVideoCtrl',
                        controllerAs: 'playVideo'
                    }
                },
                params: {
                    videoInfo: {},
                    activeTab: ''
                }
            })
            .state('coachSidemenu.tabs.playVideoPlayerProfileSearch', {
                url: '/playVideoCoachProfileSearch',
                views: {
                    'find-player-tab': {
                        templateUrl: "templates/player/playVideoPlayerProfileSearch.html",
                        controller: 'playVideoPlayerProfileSearchCtrl',
                        controllerAs: 'playVideo'
                    }
                },
                params: {
                    videoInfo: {}
                }
            })
            .state('coachSidemenu.playVideoCoachProfileSearch', {
                url: '/playVideoCoachProfileSearch',
                views: {
                    'menuContent': {
                        templateUrl: "templates/player/playVideoCoachProfileSearch.html",
                        controller: 'playVideoCoachProfileSearchCtrl',
                        controllerAs: 'playVideo'
                    }
                },
                params: {
                    videoInfo: {}
                }
            })

            .state('sidemenu.playVideoPlayerProfileSearch', {
                url: '/playVideoCoachProfileSearch',
                views: {
                    'menuContent': {
                        templateUrl: "templates/player/playVideoPlayerProfileSearch.html",
                        controller: 'playVideoPlayerProfileSearchCtrl',
                        controllerAs: 'playVideo'
                    }
                },
                params: {
                    videoInfo: {}
                }
            })
            .state('sidemenu.tabs.playVideoCoachProfileSearch', {
                url: '/playVideoCoachProfileSearch',
                views: {
                    'find-coach-tab': {
                        templateUrl: "templates/player/playVideoCoachProfileSearch.html",
                        controller: 'playVideoCoachProfileSearchCtrl',
                        controllerAs: 'playVideo'
                    }
                },
                params: {
                    videoInfo: {}
                }
            })
            .state('sidemenu.tabs.myVideos', {
                url: '/myvideos',
                views: {
                    'video-lib': {
                        templateUrl: 'templates/player/myvideos.html',
                        controller: 'myVideosCtrl',
                        controllerAs: 'myVideos'
                    }
                }
            })
            .state('sidemenu.tabs.activityDetails', {
                url: '/activitydetails',
                views: {
                    'training-tab': {
                        templateUrl: 'templates/player/activityDetail.html',
                        controller: 'activityDetailCtrl',
                        controllerAs: 'activityDetail'
                    }
                },
                params: {
                    details: {}
                }

            })
            .state('sidemenu.tabs.myActivityDetails', {
                url: '/myactivitydetails',
                views: {
                    'training-tab': {
                        templateUrl: 'templates/player/myActivityDetail.html',
                        controller: 'myActivityDetailCtrl',
                        controllerAs: 'myActivityDetail'
                    }
                },
                params: {
                    activityList: {},
                    planDetails: {},
                    activityDescription: {},
                    day: '',
                    week: '',
                    status: '',
                    lastIndex: ''
                }

            })



            //////////////
            .state('coachSidemenu.chooseSport', {
                url: '/choosesport',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/coach/choosesport.html',
                        controller: 'chooseSportCtrl',
                        controllerAs: 'choose'
                    }
                }, params: {showButton: ''}
            })
            .state('coachSidemenu.login', {
                url: '/login',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/coach/login.html',
                        controller: 'loginCtrl',
                        controllerAs: 'login'
                    }
                }
            })
            .state('coachSidemenu.signup', {
                url: '/signup',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/coach/signup.html',
                        controller: 'signupCtrl',
                        controllerAs: 'signup'
                    }
                }
            })
            .state('coachSidemenu.profile', {
                url: '/profile',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/coach/profile.html',
                        controller: 'profileCtrl',
                        controllerAs: 'profile'
                    }
                }
            })

            .state('coachSidemenu', {
                url: "/coach/sidemenu",
                abstract: true,
                templateUrl: "templates/coach/sidemenu.html"
            })
            .state('coachSidemenu.myProfile', {
                url: '/myprofile',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/coach/myprofile.html',
                        controller: 'myProfileCtrl',
                        controllerAs: 'myProfile'
                    }
                }
            })

            .state('coachSidemenu.inbox', {
                url: '/inbox',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/coach/inbox.html',
                        controller: 'inboxCtrl',
                        controllerAs: 'inbox'
                    }
                }
            })
            .state('coachSidemenu.followers', {
                url: '/followers',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/coach/followers.html',
                        controller: 'followersCtrl',
                        controllerAs: 'followers'
                    }
                }
            })
            .state('coachSidemenu.following', {
                url: '/following',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/coach/following.html',
                        controller: 'followingCtrl',
                        controllerAs: 'following'
                    }
                }
            })
            .state('coachSidemenu.faq', {
                url: '/faq',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/coach/faq.html',
                        controller: 'faqCtrl',
                        controllerAs: 'faq'
                    }
                }
            })
            .state('coachSidemenu.chat', {
                url: '/chat',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/coach/chat.html',
                        controller: 'chatCtrl',
                        controllerAs: 'chat'
                    }
                }
            })
            .state('coachSidemenu.findCoach', {
                url: '/findcoach',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/coach/findcoach.html',
                        controller: 'findPersonCtrl',
                        controllerAs: 'findPerson'
                    }
                }
            })
            .state('coachSidemenu.tabs', {
                url: "/tabs",
                views: {
                    'menuContent': {
                        templateUrl: "templates/coach/tabs.html",
                        controller: 'tabsCtrl'
                    }
                }
            })
            .state('coachSidemenu.tabs.home', {
                url: "/home",
                views: {
                    'home-tab': {
                        templateUrl: "templates/coach/home.html",
                        controller: 'homeCtrl',
                        controllerAs: 'home'
                    }
                }
            })
            .state('coachSidemenu.tabs.findPlayer', {
                url: "/findplayer",
                views: {
                    'find-player-tab': {
                        templateUrl: "templates/coach/findplayer.html",
                        controller: 'findPersonCtrl',
                        controllerAs: 'findPerson'
                    }
                }
            })

            // .state('coachSidemenu.tabs.coachProfile', {
            //     url: "/coachprofile",
            //     views: {
            //         'find-coach-tab': {
            //             templateUrl: "templates/coach/coachprofile.html",
            //             controller: 'coachProfileCtrl',
            //             controllerAs: 'coachProfile'
            //         }
            //     }
            // })
            .state('coachSidemenu.tabs.coachPlayerProfile', {
                url: "/playerprofile",
                views: {
                    'find-player-tab': {
                        templateUrl: "templates/coach/playerprofile.html",
                        controller: 'playerProfileCtrl',
                        controllerAs: 'playerProfile'
                    }
                }
            })
            .state('coachSidemenu.tabs.videoLib', {
                url: "/videolib",
                views: {
                    'video-lib': {
                        templateUrl: "templates/coach/videolib.html",
                        controller: 'videoLibCoachCtrl',
                        controllerAs: 'videoLib'
                    }
                },
                params: {
                    activeTab: ''
                }
            })
            .state('coachSidemenu.tabs.reviewVideos', {
                url: "/reviewvideos",
                views: {
                    'video-lib': {
                        templateUrl: "templates/coach/reviewvideos.html",
                        controller: 'reviewVideosCtrl',
                        controllerAs: 'reviewVideos'
                    }
                }
            })
            .state('coachSidemenu.tabs.playVideo', {
                url: "/playvideo",
                views: {
                    'video-lib': {
                        templateUrl: "templates/coach/playvideo.html",
                        controller: 'playVideoCtrl',
                        controllerAs: 'playVideo'
                    }
                },
                params: {
                    videoInfo: {}
                }
            })
            .state('coachSidemenu.tabs.playReviewVideo', {
                url: "/playreviewvideo",
                views: {
                    'video-lib': {
                        templateUrl: "templates/coach/playreviewvideo.html",
                        controller: 'playReviewVideoCtrl',
                        controllerAs: 'playReviewVideo'
                    }
                },
                params: {
                    playThisVideo: {}
                }
            })
            .state('coachSidemenu.tabs.myVideos', {
                url: '/myvideos',
                views: {
                    'video-lib': {
                        templateUrl: 'templates/coach/myvideos.html',
                        controller: 'myVideosCtrl',
                        controllerAs: 'myVideos'
                    }
                }
            })
            .state('coachSidemenu.tabs.training', {
                url: '/coachtraining',
                views: {
                    'training-tab': {
                        templateUrl: 'templates/coach/training.html',
                        controller: 'coachTrainingCtrl',
                        controllerAs: 'coachTraining'
                    }
                }
            })
            .state('coachSidemenu.tabs.trainingDetail', {
                url: '/coachtrainingdetail',
                views: {
                    'training-tab': {
                        templateUrl: 'templates/coach/trainingDetail.html',
                        controller: 'coachTrainingDetailCtrl',
                        controllerAs: 'coachTrainingDetail'
                    }
                }
            });

        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/player/sidemenu/login');


        //$urlRouterProvider.otherwise('/player/sidemenu/choosesport');

    })

    .constant('calenderName', 'Sparup Calender')
    // .constant('backend', 'http://localhost');
    .constant('backend', 'http://ec2-35-161-139-251.us-west-2.compute.amazonaws.com:8085');

