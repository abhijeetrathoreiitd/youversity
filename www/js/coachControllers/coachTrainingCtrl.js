angular.module('starter')

    .controller('coachTrainingCtrl', coachTrainingCtrl);

function coachTrainingCtrl($window, $ionicScrollDelegate, $state, $timeout, loggedInUser,
                           $rootScope, $ionicModal, $scope, $ionicNavBarDelegate,$cordovaToast,
                           trainingPlanCategory, trainingPlanCategoryList, trainingPlan,trainingPlanParams,SportsList) {
    var coachTraining = this;
    coachTraining.videos = 0;
    coachTraining.status = 1;
    coachTraining.prevStatus = 1;
    $rootScope.showTabs = true;

    //Whether search bar is opened or not
    coachTraining.searchOpen = false;
    coachTraining.searchOpenLate = false;
    coachTraining.menuDropped = false;
    coachTraining.backdrop = false;
    coachTraining.streamChosen = false;
    coachTraining.deltaRating = 5;
    coachTraining.cost = {
        min: '0',
        max: '10000',
        value: '10000'
    }
    coachTraining.duration = {
        min: '1',
        max: '20',
        value: '20'
    }


    coachTraining.costFilter = coachTraining.cost.value;
    coachTraining.durationFilter = coachTraining.duration.value;
    coachTraining.deltaPaidFilter = coachTraining.cost.value;
    coachTraining.paidFilter = coachTraining.cost.value;
    coachTraining.paidCheckBoxVal = true;
    coachTraining.ratingFilter = 5;


    $timeout(function () {
        coachTraining.loadMoreViews = true;
    }, 300);

    $scope.$on('$ionicView.enter', function (e) {
        console.log('bar shown')
        $ionicNavBarDelegate.showBar(true);
    });

    // coachTraining.toggleLeft = function () {
    //     console.log('toggle left')
    //     $ionicSideMenuDelegate.toggleLeft();
    // };


    $ionicModal.fromTemplateUrl('templates/player/trainingmodal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modal) {
        coachTraining.modal = modal;
    });
    coachTraining.showFilters = function () {
        coachTraining.modal.show();
    }
    coachTraining.hideFilters = function () {
        console.log('hide modal');
        coachTraining.modal.hide();
    }

    coachTraining.changePrevStatus = function (number) {
        coachTraining.status = number;
        $timeout(function () {
            coachTraining.prevStatus = number;
        }, 500)
    }


    coachTraining.showSearch = function () {
        coachTraining.searchOpen = true;
        $timeout(function () {
            coachTraining.searchOpenLate = true;
        }, 400)
    }
    coachTraining.hideSearch = function () {
        coachTraining.searchOpen = false;
        $timeout(function () {
            coachTraining.searchOpenLate = false;
        }, 100)
    }

    coachTraining.selectSport = function (sport) {
        $rootScope.sportSelected = sport;
        coachTraining.streamChosen = false;
        coachTraining.getTrainingPlanCategory();

        coachTraining.backdrop = false;
        $timeout(function () {
            coachTraining.menuDropped = false;
        }, 500)
    }



    coachTraining.dropMenu = function () {
        coachTraining.backdrop = true;
        $timeout(function () {
            coachTraining.menuDropped = true;
        }, 100)    }
    coachTraining.hideMenu = function () {
        coachTraining.backdrop = false;
        $timeout(function () {
            coachTraining.menuDropped = false;
        }, 500)
    }

    coachTraining.selectRating = function (rating) {
        coachTraining.deltaRating = rating;
    }


    coachTraining.sortBy = 'trainingName';
    coachTraining.deltaSortBy = 'trainingName';
    coachTraining.sortByFunction = function (sortBy) {
        coachTraining.deltaSortBy = sortBy;

    }


    coachTraining.changePaidFilter = function () {
        if (coachTraining.paidCheckBoxVal) {
            coachTraining.deltaPaidFilter = coachTraining.cost.value;
        }
        else {
            coachTraining.deltaPaidFilter = 0;
        }
    }


    coachTraining.applyFilter = function () {
        coachTraining.sortBy = coachTraining.deltaSortBy;
        coachTraining.modal.hide();
        coachTraining.costFilter = coachTraining.cost.value;
        coachTraining.durationFilter = coachTraining.duration.value;
        coachTraining.paidFilter = coachTraining.deltaPaidFilter;
        coachTraining.ratingFilter = coachTraining.deltaRating;


        $ionicScrollDelegate.resize();
    }
    //-------------------------DESIGN CALCULATION START-------------------------//
    var devHeight = $window.innerHeight;
    var devWidth = $window.innerWidth;

    coachTraining.logoDiv = {
        'margin-top': 0.1 * devHeight + 'px',
        'height': 0.2 * devHeight + 'px',
        'margin-bottom': 0.04 * devHeight + 'px'
    };
    coachTraining.datum = {
        'left': devWidth + 'px'
    };
    coachTraining.sportScroll = {
        'width': 0.7 * devWidth + 'px'
    };
    coachTraining.placeholder = {
        'position': 'absolute',
        'top': 0.3 * (devHeight - 44 - 49) - 44 + 'px',
        'width': 100 + '%'
    };
   
    coachTraining.slidermin = {'left': 0.2 * (devWidth - 22) + 13 + 'px'};
    coachTraining.sliderWidth = devWidth - (0.2 * (devWidth - 22) + 20 + 48 + 22 + 3) - 30;
    coachTraining.slidervalue = {'left': 0.2 * (devWidth - 22) + 11 + 24 + 10 + ((parseInt(coachTraining.cost.value, 10) - 20) * coachTraining.sliderWidth / 80) + 'px'};
    coachTraining.slidervalue2 = {'left': 0.2 * (devWidth - 22) + 11 + 24 + 10 + ((parseInt(coachTraining.duration.value, 10) - 1) * coachTraining.sliderWidth / 19) + 'px'};
    coachTraining.general = {'height': devHeight - 88 + 'px'};
    coachTraining.placeholder = {
        'position': 'absolute',
        'top': 0.3 * (devHeight - 44 - 49) + 'px',
        'width': 100 + '%'
    };

    //-------------------------DESIGN CALCULATION END-------------------------//
    $scope.$watch(angular.bind(coachTraining, function () {
        return coachTraining.cost.value;
    }), function (value) {
        coachTraining.slidervalue = {'left': 0.2 * (devWidth - 22) + 11 + 24 + 10 + ((parseInt(coachTraining.cost.value, 10) - 20) * coachTraining.sliderWidth / 80) + 'px'};
    });
    $scope.$watch(angular.bind(coachTraining, function () {
        return coachTraining.duration.value;
    }), function (value) {
        coachTraining.slidervalue2 = {'left': 0.2 * (devWidth - 22) + 11 + 24 + 10 + ((parseInt(coachTraining.duration.value, 10) - 1) * coachTraining.sliderWidth / 19) + 'px'};
    });

    coachTraining.seeDetail = function (trainingPlan) {
        $rootScope.showTabs = false;
        trainingPlanParams.params = trainingPlan
        currentWeekParams.week = 0;
        $state.go('sidemenu.tabs.trainingDetail');
    }

    coachTraining.seeOngoing = function () {
        $rootScope.showTabs = false;
        $state.go('sidemenu.tabs.mytrainingDetail');
    }


    $ionicModal.fromTemplateUrl('templates/coach/addTrainingPlanModal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modal) {
        coachTraining.modal = modal;
    });


    coachTraining.levelList = ['beginner', 'intermediate', 'advanced'];
    coachTraining.showAddTrainingPlanModal = function () {

        coachTraining.trainingPlan = {
            trainingName: '',
            trainingBy: loggedInUser.info._id,
            duration: '',
            cost: 0,
            category: coachTraining.trainingPlanCategoryList[0],
            sport: coachTraining.mySports[0],
            description: '',
            level: coachTraining.levelList[0]
        }


        coachTraining.showLevel2Div=false;
        coachTraining.showLevel3Div=false;
        coachTraining.deltaSelectedTags = [];
        coachTraining.deltaSelectedTagsIds = {level1:[],level2:[],level3:[]};
        coachTraining.modal.show();
    }


    coachTraining.hideAddTrainingPlanModal = function () {
        coachTraining.modal.hide();
    }

    function checkValidField() {
        var errorOccurred = false;

        if (coachTraining.trainingPlan.trainingName == '') {
            coachTraining.trainingNameError = true;
            errorOccurred = true;
        }

        if (coachTraining.trainingPlan.description == '') {
            coachTraining.descriptionError = true;
            errorOccurred = true;
        }

        if (coachTraining.trainingPlan.duration == '') {
            coachTraining.durationError = true;
            errorOccurred = true;
        }
        return errorOccurred;
    }


    coachTraining.addTrainingPlan = function () {

        if (!checkValidField()) {

            var savePlan = {
                trainingName: coachTraining.trainingPlan.trainingName,
                trainingBy: coachTraining.trainingPlan.trainingBy,
                duration: parseInt(coachTraining.trainingPlan.duration),
                cost: parseInt(coachTraining.trainingPlan.cost),
                category: coachTraining.trainingPlan.category._id,
                sport: coachTraining.trainingPlan.sport._id,
                description: coachTraining.trainingPlan.description,
                level: coachTraining.trainingPlan.level,
                tagsLevel1: coachTraining.trainingPlan.tagsLevel1,
                tagsLevel2: coachTraining.trainingPlan.tagsLevel2,
                tagsLevel3: coachTraining.trainingPlan.tagsLevel3,
            };
            trainingPlan.save({savePlan:savePlan}, function (data) {
                data.sport=$rootScope.sportSelected;

                coachTraining.coachTrainingPlanList.push(data);
                if(window.cordova) {
                    $cordovaToast.showShortBottom('Training plan created');
                }
                else{
                    alert('Training plan created');
                }
                coachTraining.hideAddTrainingPlanModal();
            }, function (err) {

                console.log(err);
            })
        }

    }

    coachTraining.mySports = [];
        console.log(SportsList.list);
        console.log(angular.copy(coachTraining.mySports));
        loggedInUser.info.mySports = _.uniq(loggedInUser.info.mySports);
        console.log(loggedInUser.info.mySports);

    _.each(SportsList.list, function (sport) {
        _.each(loggedInUser.info.mySports, function (mySport,index) {
            if (mySport == sport._id)
                coachTraining.mySports.push(sport);
                console.log(index);
        })
        console.log(angular.copy(coachTraining.mySports));
    });


    trainingPlan.coachTrainingPlan({coachId: loggedInUser.info._id}, function (result) {
            coachTraining.coachTrainingPlanList = result;
        }, function (err) {
            console.log(err);
        }
    )

    coachTraining.trainingPlanCategoryList = trainingPlanCategoryList.category;

    coachTraining.getTrainingPlanCategory = function () {
        if ($rootScope.sportSelected && _.indexOf(trainingPlanCategoryList.sportsList, $rootScope.sportSelected._id) == -1) {
            trainingPlanCategoryList.sportsList.push(sportsList, $rootScope.sportSelected._id);
            trainingPlanCategory.query({sportId: $rootScope.sportSelected._id}, function (data) {
                _.each(data, function (category) {
                    coachTraining.trainingPlanCategoryList.push(category);
                })
            }, function (err) {
                console.log(err);
            })
        }
        else {
            console.log('no sport selected');
        }
    }
    
    //call  coachTraining.getTrainingPlanCategory(); if sport is select in video screen
    if(typeof $rootScope.sportSelected!='undefined')
    {
        coachTraining.getTrainingPlanCategory();
    }

    coachTraining.goTrainingDetails=function(plan)
    {
        trainingPlanParams.params=plan;
        $state.go('coachSidemenu.tabs.trainingDetail')
    }




}
