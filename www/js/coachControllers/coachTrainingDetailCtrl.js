angular.module('starter')

    .controller('coachTrainingDetailCtrl', coachTrainingDetailCtrl);
function coachTrainingDetailCtrl($window, $scope, Video, loggedInUser, backend,Unit1,Unit2,
                                 playerUploadedVideos, $ionicModal, Activity, trainingPlanParams, 
                                 currentWeekParams, $rootScope, $state, $timeout, $ionicLoading,$sce) {
    var coachTrainingDetail = this;
    coachTrainingDetail.videos = 0;
    $rootScope.showTabs = false;


    //-------------------------DESIGN CALCULATION START-------------------------//
    var devHeight = $window.innerHeight;
    var devWidth = $window.innerWidth;
    var height1 = document.querySelector('#coachInfo').offsetHeight;

    coachTrainingDetail.videoFrame= {'width': devWidth + 'px', 'height': 9 * devWidth / 16 + 'px'};

    coachTrainingDetail.unit1=Unit1.units;
    coachTrainingDetail.unit2=Unit2.units;
    console.log(Unit1);
    console.log(Unit2);

    $timeout(function () {
        coachTrainingDetail.activityScroller = {'height': devHeight - 44 - 44 - 44 - 44 - height1  + 'px'};
        coachTrainingDetail.myactivityScroller = {'height': devHeight - 44 - 44 - height1  + 'px'};
    }, 300);
    //-------------------------DESIGN CALCULATION END-------------------------//

    coachTrainingDetail.return = function () {
        $rootScope.showTabs = true;
        $state.go('coachSidemenu.tabs.training');
    };


    coachTrainingDetail.trainingPlanDetails = trainingPlanParams.params;


    coachTrainingDetail.activityList = [];//array of array
    $ionicLoading.show({"template": "Loading"});
    Activity.query({trainingPlanId: coachTrainingDetail.trainingPlanDetails._id}, function (data) {
        data=_.sortBy(data, [function(o) { return o.day; }]);
        var totalWeek = data.length / 7;
        for (var i = 1; i <= totalWeek; i++) {
            coachTrainingDetail.activityList[i - 1] = _.filter(data, function (activity) {
                return activity.day > 7 * (i - 1) && activity.day <= 7 * i;
            })
        }
        $ionicLoading.hide();
    }, function (err) {
    }, function (err) {
        $ionicLoading.hide();
        console.log(err);
    });

    coachTrainingDetail.week = 0;
    coachTrainingDetail.selectWeek = function (week) {
        coachTrainingDetail.week = week;
    };


    coachTrainingDetail.week = currentWeekParams.week;
    coachTrainingDetail.selectWeek = function (week) {

        coachTrainingDetail.week = week;
        currentWeekParams.week = week;
    };


    $ionicModal.fromTemplateUrl('templates/coach/selectVideoModal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modal) {
        coachTrainingDetail.modalVideo = modal;
    });


    coachTrainingDetail.showSelectVideoModal = function () {
        coachTrainingDetail.fetchUploadVideos();
        coachTrainingDetail.modalVideo.show();
    };
    coachTrainingDetail.hideSelectVideoModal = function () {
        coachTrainingDetail.modalVideo.hide();
    };


    $ionicModal.fromTemplateUrl('templates/coach/addActivityModal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modal) {
        coachTrainingDetail.modal = modal;
    });



    $ionicModal.fromTemplateUrl('templates/coach/activityDetailModal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modal) {
        coachTrainingDetail.activityDetailModal = modal;
    });


    var updateActivityIds=[];


    coachTrainingDetail.showActivityDetailModal = function (activity) {
        console.log(activity)
        coachTrainingDetail.activityDetailInModal = activity;
        coachTrainingDetail.activityDetailModal.show();

            if (coachTrainingDetail.activityDetailInModal.videoId != null && coachTrainingDetail.activityDetailInModal.videoId.url.includes('youtube.com')) {
                coachTrainingDetail.isYoutubeVideo = true;
                coachTrainingDetail.trustedLink = $sce.trustAsResourceUrl(coachTrainingDetail.activityDetailInModal.videoId.url + '?showinfo=0');
            }
            else {
                coachTrainingDetail.isYoutubeVideo = false;
            }

            if (coachTrainingDetail.activityDetailInModal.videoId != null && !coachTrainingDetail.isYoutubeVideo) {
                $timeout(function () {
                    var player = jwplayer("videoPlayerdiv").setup({
                        "file": coachTrainingDetail.activityDetailInModal.videoId.url,
                        "image": "img/icon.png",
                        "height": 360 * devWidth / 640,
                        "width": devWidth
                    });

                    jwplayer().on('play', function (e) {
                        //call increase view api here
                    });
                    if (document.cookie.indexOf("jwplayerAutoStart") == -1) {
                        document.cookie = "jwplayerAutoStart=1";
                        player.on('ready', function () {
                            player.play();
                        });
                    }
                    else {
                    }
                }, 200)
            }

        
        
        
    };
    coachTrainingDetail.hideActivityDetailModal = function () {
        coachTrainingDetail.activityDetailModal.hide();
    };
    

    coachTrainingDetail.showAddActivityModal = function (activity, index) {
        coachTrainingDetail.activityIndex = index;
        coachTrainingDetail.activityDetailInModal = {};
        coachTrainingDetail.activityListInModal = coachTrainingDetail.activityList[coachTrainingDetail.week][coachTrainingDetail.activityIndex].activities;
        coachTrainingDetail.modal.show();
    };
    coachTrainingDetail.hideAddActivityModal = function () {
        coachTrainingDetail.modal.hide();
    };


    function checkValidField()
    {
        coachTrainingDetail.activityNameError=false;
        coachTrainingDetail.activityDescriptionError=false;
        var errorOccurred=false;
        if(typeof coachTrainingDetail.activityDetailInModal.activityName=='undefined' || coachTrainingDetail.activityDetailInModal.activityName=='')
        {
            coachTrainingDetail.activityNameError=true;
            errorOccurred=true;
        }

        if(typeof coachTrainingDetail.activityDetailInModal.description=='undefined' || coachTrainingDetail.activityDetailInModal.description=='')
        {
            coachTrainingDetail.activityDescriptionError=true;
            errorOccurred=true;
        }
        return errorOccurred;

    }
    coachTrainingDetail.addActivity = function () {
        if(!checkValidField()) {
            coachTrainingDetail.activityListInModal.push(coachTrainingDetail.activityDetailInModal);
            updateActivityIds.push(coachTrainingDetail.activityList[coachTrainingDetail.week][coachTrainingDetail.activityIndex]._id)
            coachTrainingDetail.activityDetailInModal = {};
        }
    };


    coachTrainingDetail.addVideoToActivity = function (video) {
        coachTrainingDetail.activityDetailInModal.videoId = video._id;
        coachTrainingDetail.activityDetailInModal.videoUrl = video.url;
        coachTrainingDetail.activityDetailInModal.thumbnail = video.thumbnail;
        coachTrainingDetail.hideSelectVideoModal();
    };

    coachTrainingDetail.playerUploadedVideos = playerUploadedVideos;
    coachTrainingDetail.fetchUploadVideos = function () {

        Video.get({
                userId: loggedInUser.info._id,
                lastVideoFetchTime: coachTrainingDetail.playerUploadedVideos.lastVideoFetchTime
            },
            function (userVideos) {
                coachTrainingDetail.playerUploadedVideos.lastVideoFetchTime = userVideos.lastVideoFetchTime;
                _.each(userVideos.videoList, function (vdo) {
                    if (!vdo.url.includes('youtube.com'))
                        vdo.thumbnail = backend + '/uploads/' + vdo.thumbnail;
                    coachTrainingDetail.playerUploadedVideos.videosList.push(vdo);
                })
            }, function (error) {
                console.log(error);
                alert(error);
            })
    }

    coachTrainingDetail.saveActivity=function()
    {
        var activities=[];
        _.each(coachTrainingDetail.activityList,function(activityList){
            _.each(activityList,function(activity){
                if(_.indexOf(updateActivityIds,activity._id)!=-1)
                activities.push(activity);
            })
        })

        Activity.updateActivityCoach({activities:activities},function(data)
        {
        },function(err)
        {
            console.log(err);
        })
    }
}
