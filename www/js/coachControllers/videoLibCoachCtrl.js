angular.module('starter')

    .controller('videoLibCoachCtrl', videoLibCoachCtrl)
function videoLibCoachCtrl($window, uploadService, Video, $state, mySports, fetchedVideoInfo, backend,
                           $ionicPopup, IonicClosePopupService, $timeout, fetchedVideoList, $rootScope, videoForReview,
                           $ionicModal, $scope, $ionicNavBarDelegate, loggedInUser, playerUploadedVideos,
                           $ionicLoading, $cordovaToast, Review, Package, $ionicScrollDelegate, allLevelTagsList,
                           subscribedVideoList, myBookmarkedVideo, $stateParams, $sce, ConvertYoutubeDuration, uploadThumbnail) {
    console.log($state.current.name);

    var videoLib = this;


    videoLib.isCoach = true;//for showing add package in uploadVideoModal
    //-------------------------DESIGN CALCULATION START-------------------------//
    var devHeight = $window.innerHeight;
    var devWidth = $window.innerWidth;
    videoLib.add = false;
    $timeout(function () {
        videoLib.isPlayer = loggedInUser.info.player;
    }, 1000)


    videoLib.logoDiv = {
        'margin-top': 0.1 * devHeight + 'px',
        'height': 0.2 * devHeight + 'px',
        'margin-bottom': 0.04 * devHeight + 'px'
    };
    videoLib.datum = {
        'left': devWidth + 'px'
    };
    videoLib.fullDim = {'width': devWidth + 'px', 'height': devHeight + 'px'};
    videoLib.sportScroll = {
        'width': 0.7 * devWidth + 'px'
    };
    videoLib.placeholder = {
        'position': 'absolute',
        'top': 0.3 * (devHeight - 44 - 49) - 44 + 'px',
        'width': 100 + '%'
    };
    videoLib.videoFrame = {'width': devWidth - 30 + 'px', 'height': 9 * (devWidth - 30) / 16 + 'px'};

    var tabHeightCalculate = function () {
        $timeout(function () {
            var tabHeight1 = document.querySelector('#general').offsetHeight;
            var tabHeight2 = document.querySelector('#subscribed').offsetHeight;
            var tabHeight3 = document.querySelector('#uploaded').offsetHeight;
            var minHeight = Math.max(devHeight - 88 - 49, tabHeight1, tabHeight2, tabHeight3);

            videoLib.general = {
                'min-height': Math.max(minHeight, tabHeight1, tabHeight2, tabHeight3) + 'px'
            }
            videoLib.subscribed = {
                'min-height': Math.max(minHeight, tabHeight1, tabHeight2, tabHeight3) + 'px'
            }
            videoLib.uploaded = {
                'min-height': Math.max(minHeight, tabHeight1, tabHeight2, tabHeight3) + 'px'
            }

        }, 200);
    }

    tabHeightCalculate();

    //-------------------------DESIGN CALCULATION END-------------------------//
    videoLib.videos = 0;
    videoLib.status = 1;
    videoLib.showDivision = 1;
    videoLib.prevStatus = 1;
    //button to upload more videos
    videoLib.showUploadButton = false;
    //Whether search bar is opened or not
    videoLib.searchOpen = false;
    videoLib.searchOpenLate = false;
    videoLib.loadMoreViews = false;
    videoLib.menuDropped = false;
    videoLib.backdrop = false;
    videoLib.expandlist = false;
    videoLib.showReviewButton = false;
    videoLib.selectedRating = 3;
    videoLib.showPackages = true;// variables to controller whether to show uploadedVideoList as package or as list
    videoLib.youtube = false; // If youtube upload is selected
    videoLib.youtubeVideoLoaded = false; // If you tube link is added for upload

    var fullpath = '';
    var filepath = '';
    videoLib.toggle = false;
    videoLib.showPaid = false;
    videoLib.deltaShowPaid = false;
    videoLib.showSpinner = false;
    videoLib.searchResults = [];

    $timeout(function () {
        videoLib.loadMoreViews = true;
    }, 300);

    $scope.$on('$ionicView.enter', function (e) {
        $ionicNavBarDelegate.showBar(true);
    });

    videoLib.fromYoutube = function () {
        videoAlertPopup.close();
        videoLib.youtube = true;
        // start video capture
        videoLib.videoInfo = {
            refToTags: [],
            title: '',
            url: '',
            sport: $rootScope.sportSelected._id,
            paidVideo: true,
            package: videoLib.packageList[0],
            videoBy: loggedInUser.info._id,
            accountType: loggedInUser.info.player ? 'player' : 'coach',
            description: ''
        }

        videoLib.deltaSelectedTags = [];
        videoLib.deltaSelectedTagsIds = {level1: [], level2: [], level3: []};
        // $rootScope.showTagsDirective=true;
        uploadvideomodal.show();
    }
    videoLib.trustSrc = function (src) {
        console.log(src);
        return $sce.trustAsResourceUrl(src);
    }


    function successThumbnail(thumbnail) {
        var promise = uploadThumbnail.uploadImage(thumbnail);
        promise.then(function (thumbnail) {
            videoLib.videoInfo.thumbnail = thumbnail;
        })
    }

    function errorThumbnail(error) {
        console.log(error)
    }

    function successInfo(data) {
        videoLib.videoInfo.duration = data.duration;

        VideoEditor.createThumbnail(
            successThumbnail, // success cb
            errorThumbnail, // error cb
            {
                fileUri: fullpath, // the path to the video on the device
                outputFileName: 'output-name', // the file name for the JPEG image
                atTime: data.duration / 2, // optional, location in the video to create the thumbnail (in seconds)
                width: 100, // optional, width of the thumbnail
                height: 100, // optional, height of the thumbnail
                quality: 100 // optional, quality of the thumbnail (between 1 and 100)
            }
        );
    }

    function errorInfo(data) {
        console.log(data);
    }

    videoLib.getVideoFromGallery = function () {
        videoAlertPopup.close();
        navigator.camera.getPicture(onSuccess, onFail, {
            quality: 50,
            destinationType: Camera.DestinationType.FILE_URI,
            sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
            mediaType: Camera.MediaType.VIDEO
        });

        function onSuccess(videoPath) {
            fullpath = videoPath;
            filepath = videoPath;

            VideoEditor.getVideoInfo(
                successInfo, // success cb
                errorInfo, // error cb
                {
                    fileUri: 'file:' + fullpath, // the path to the video on the device
                })
            videoLib.videoInfo = {
                refToTags: [],
                title: '',
                url: '',
                sport: $rootScope.sportSelected._id,
                paidVideo: true,
                package: videoLib.packageList[0],
                videoBy: loggedInUser.info._id,
                accountType: loggedInUser.info.player ? 'player' : 'coach',
                description: ''
            }

            videoLib.deltaSelectedTags = [];
            videoLib.deltaSelectedTagsIds = {level1: [], level2: [], level3: []};
            // $rootScope.showTagsDirective=true;
            uploadvideomodal.show();


            $timeout(function () {
                var player = jwplayer("videoPlayerdivModal").setup({
                    "file": videoPath,
                    "image": "http://example.com/myImage.png",
                    "height": 360 * (devWidth - 40) / 640,
                    "width": devWidth - 40
                });

                jwplayer().on('play', function (e) {
                    //call increase view api here
                    playVideo.viewThisVideo(playVideo.videoInfo._id);
                });
                if (document.cookie.indexOf("jwplayerAutoStart") == -1) {
                    document.cookie = "jwplayerAutoStart=1";
                    player.on('ready', function () {
                        player.play();
                    });
                }
                else {
                }
            }, 200)
        }

        function onFail(message) {
            alert('Failed because: ' + message);
        }
    }


    videoLib.loadYoutubeVideo = function () {


        videoLib.untrustedLink = videoLib.youtubeLink + '';

        if (videoLib.untrustedLink.indexOf('watch?v=') != -1 && videoLib.untrustedLink.indexOf('&') != -1) {
            var video_id = videoLib.untrustedLink.substring(videoLib.untrustedLink.lastIndexOf('watch?v=') + 8, videoLib.untrustedLink.indexOf('&'));
        }
        else if
        (videoLib.untrustedLink.indexOf('watch?v=') != -1) {
            var video_id = videoLib.untrustedLink.substring(videoLib.untrustedLink.lastIndexOf('watch?v=') + 8, videoLib.untrustedLink.length);
        }
        else {
            var video_id = videoLib.untrustedLink.substring(videoLib.untrustedLink.lastIndexOf('/') + 1, videoLib.untrustedLink.length);
        }

        videoLib.videoInfo.thumbnail = 'http://img.youtube.com/vi/' + video_id + '/3.jpg';
        videoLib.untrustedLink = 'https://www.youtube.com/embed/' + video_id;
        videoLib.trustedLink = videoLib.trustSrc(videoLib.untrustedLink + '?showinfo=0');
        $timeout(function () {
            videoLib.youtubeVideoLoaded = true;
        }, 200)
    }
    videoLib.uploadYoutubeVideo = function () {


        videoLib.videoInfo.url = videoLib.untrustedLink;

        var videoId = videoLib.videoInfo.url.substring((videoLib.videoInfo.url.lastIndexOf('/') + 1))

        var promise = ConvertYoutubeDuration.convertTime(videoId);
        promise.then(function (duration) {
            videoLib.videoInfo.duration = duration;
        }).then(function () {

            // videoLib.videoInfo.package = videoLib.packageList[0];
            videoLib.videoInfo.url = videoLib.untrustedLink;
            console.log('uploading youtube video to AWS');
            videoLib.titleError = false;
            videoLib.descriptionError = false;

            if (videoLib.videoInfo.title != '' && videoLib.videoInfo.description != '') {

                $ionicLoading.show({'template': 'Uploading Video.....'});
                Video.save({videoInfo: videoLib.videoInfo}, function (data) {
                    // deferred.resolve(data);
                    $ionicLoading.hide();
                    if (window.cordova) {
                        $cordovaToast.showShortBottom('Video Uploaded Successfully')
                    }
                    else {
                        alert('Video Uploaded Successfully');
                    }

                    videoLib.hideUploadVideoModal();
                    var videoObject = {
                        _id: data._id,
                        accountType: data.accountType,
                        createdAt: data.createdAt,
                        description: data.description,
                        dislikes: data.dislikes,
                        likes: data.likes,
                        numberOfViews: data.numberOfViews,
                        paidVideo: data.paidVideo,
                        refToFeedback: data.refToFeedback,
                        refToPackage: data.refToPackage,
                        refToTags: data.refToTags,
                        sport: data.sport,
                        subscribers: data.subscribers,
                        title: data.title,
                        updatedAt: data.updatedAt,
                        url: data.url,
                        videoBy: loggedInUser.info,
                        duration: data.duration

                    };
                    if (!videoObject.url.includes('youtube.com'))
                        videoObject.thumbnail = backend + '/uploads/' + data.thumbnail;
                    else
                        videoObject.thumbnail = data.thumbnail;


                    videoLib.playerUploadedVideos.videosList.push(videoObject);
                    _.each(videoLib.packageList, function (package) {
                        if (package._id == videoLib.videoInfo.package._id) {
                            package.refToVideos.push(videoObject);
                        }
                    })

                }, function (err) {
                    console.log(err);
                });
            }
            else {
                if (videoLib.videoInfo.title == '')
                    videoLib.titleError = true;
                if (videoLib.videoInfo.description == '')
                    videoLib.descriptionError = true;

            }
        })

    }

    videoLib.changePackage = function () {
    }


    function checkPermissionCallback(status) {
        if (!status.hasPermission) {
            var errorCallback = function () {
                console.warn('storage permission is not turned on');
            }

            permissions.requestPermission(
                permissions.WRITE_EXTERNAL_STORAGE,
                function (status) {
                    if (!status.hasPermission) errorCallback();
                },
                errorCallback);
        }
    }

    if (window.cordova) {
        if ($rootScope.android) {
            var permissions = cordova.plugins.permissions;
            permissions.hasPermission(permissions.WRITE_EXTERNAL_STORAGE, checkPermissionCallback, null);
        }
    }

    // videoLib.toggleLeft = function () {
    //     console.log('toggle left')
    //     $ionicSideMenuDelegate.toggleLeft();
    // };


    videoLib.showAddVideoToPackageModal = function () {
        videoLib.deltaPackageVideoList = videoLib.packageVideoList;

        var packageVideoId = _.map(videoLib.packageVideoList, '_id');
        videoLib.deltaVideoForPackage = packageVideoId;
        videoLib.videoInModal = [];
        videoLib.videoInModal = _.filter(videoLib.playerUploadedVideos.videosList, function (video) {
            if (_.indexOf(packageVideoId, video._id) == -1) {
                video.inPackage = false;
            }
            else {
                video.inPackage = true;
            }
            return video.sport == $rootScope.sportSelected._id;
        })


        $ionicModal.fromTemplateUrl('templates/coach/addVideoToPackageModal.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            addVideoModal = modal;
        }).then(function () {
            addVideoModal.show();
        })

    }


    videoLib.hideAddVideoToPackageModal = function () {
        addVideoModal.hide();
    }


    videoLib.selectVideoForPackage = function (video) {


        if (_.indexOf(videoLib.deltaVideoForPackage, video._id) == -1) {
            videoLib.deltaVideoForPackage.push(video._id);
            videoLib.deltaPackageVideoList.push(video);
            video.inPackage = true;
        }
        else {
            videoLib.deltaVideoForPackage.splice(_.indexOf(videoLib.deltaVideoForPackage, video._id), 1);
            videoLib.deltaPackageVideoList = _.reject(videoLib.deltaPackageVideoList, function (vdo) {
                return video._id == vdo._id;
            });
            video.inPackage = false;
        }
    }

    videoLib.addVideoToExistingPackage = function () {
        videoLib.packageVideoList = videoLib.deltaPackageVideoList;
        _.each(videoLib.packageList, function (list) {
            if (list._id == videoLib.selectedPackageId)
                list.refToVideos = videoLib.packageVideoList;
        })
        Package.updateVideoInPackage({
            packageId: videoLib.selectedPackageId,
            videoList: videoLib.deltaVideoForPackage
        }, function (data) {
        }, function (err) {
            console.log(err);
        })
    }


    $ionicModal.fromTemplateUrl('templates/player/videofiltermodal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modal) {
        videoLib.modal = modal;
    });
    videoLib.showFilters = function () {
        videoLib.modal.show();
    }
    videoLib.hideFilters = function () {

        videoLib.deltaSortBy = videoLib.sortBy;
        videoLib.modal.hide();

        $ionicScrollDelegate.resize();
    }
    videoLib.playVideo = function (video) {
        $rootScope.showTabs = false;
        if (!loggedInUser.info.player) {
            $state.go('coachSidemenu.tabs.playVideo', {
                videoInfo: video,
                activeTab: videoLib.status,
                redirectTo: 'videoLib'
            });
        }
        else {
            $state.go('sidemenu.tabs.playVideo', {
                videoInfo: video,
                activeTab: videoLib.status,
                redirectTo: 'videoLib'
            });
        }
    }
    // videoLib.selectSport = function(){
    //     videoLib.sportSelected = true;
    // }

    videoLib.goTovideoLib = function () {
        $state.go('sidemenu.tabs.videoLib');
    }

    videoLib.changePrevStatus = function (number) {
        $ionicScrollDelegate.scrollTop();
        videoLib.showDivision = 0;
        videoLib.status = number;

        $timeout(function () {
            videoLib.showDivision = videoLib.status;
        }, 200);

        if (videoLib.status != videoLib.prevStatus) {
            videoLib.clearSearch();
        }

        switch (number) {
            case 1:
                console.log('general');
                videoLib.showUploadButton = false;
                break;
            case 2:
                console.log('review');
                videoLib.showUploadButton = false;
                if ($rootScope.sportSelected) {
                    videoLib.selectSport($rootScope.sportSelected);
                }
                break;
            case 3:
                console.log('uploaded');

                videoLib.expandlist = false;
                if ($rootScope.sportSelected) {
                    videoLib.showUploadButton = true;
                    videoLib.selectSport($rootScope.sportSelected);
                }
                break;

        }
        $timeout(function () {
            videoLib.prevStatus = number;
        }, 500)
    }
    videoLib.showUpload = function () {
        if (videoLib.uploaded) {
            $timeout(function () {
                videoLib.showUploadButton = true;
            }, 200)
        }
    }
    videoLib.hideUpload = function () {
        videoLib.showUploadButton = false;
    }
    videoLib.fetchReviewVideos = function () {
        videoLib.showSpinner = true;
        videoLib.videoForReview = videoForReview;
        Review.getVideoForReview({
            coachId: loggedInUser.info._id,
            lastVideoFetchTime: videoForReview.lastVideoFetchTime
        }, function (data) {
            videoForReview.lastVideoFetchTime = data.lastVideoFetchTime;
            _.each(data.videoList, function (video) {

                _.each(video.review.review, function (rev) {
                    if (rev.coachId == loggedInUser.info._id && rev.review) {
                        video.reviewed = true;
                    }
                })

                if (!video.url.includes('youtube.com'))
                    video.thumbnail = backend + '/uploads/' + video.thumbnail;


                videoLib.videoForReview.videoList.push(video);
            })
            videoLib.showSpinner = false;

        }, function (err) {
            alert(err);
            videoLib.showSpinner = false;

        })
    }

    videoLib.fetchUploadVideos = function () {
        videoLib.showSpinner = true;

        Video.get({
                userId: loggedInUser.info._id,
                lastVideoFetchTime: videoLib.playerUploadedVideos.lastVideoFetchTime
            },
            function (userVideos) {

                videoLib.playerUploadedVideos.lastVideoFetchTime = userVideos.lastVideoFetchTime;
                _.each(userVideos.videoList, function (vdo) {

                    // videoLib.searchByTitle.push({title: vdo.title, sport: vdo.sport, videoId: vdo._id});

                    if (!vdo.url.includes('youtube.com'))
                        vdo.thumbnail = backend + '/uploads/' + vdo.thumbnail;
                    videoLib.playerUploadedVideos.videosList.push(vdo);

                })
                videoLib.showSpinner = false;
                // console.log(videoLib.searchByTitle)
            }, function (error) {
                console.log(error);
                alert(error);
                videoLib.showSpinner = false;

            })
    }


    videoLib.showSearch = function () {
        videoLib.searchOpen = true;
        $timeout(function () {
            videoLib.searchOpenLate = true;
        }, 400)
    }
    videoLib.hideSearch = function () {
        videoLib.searchOpen = false;
        $timeout(function () {
            videoLib.searchOpenLate = false;
        }, 100)
    }

    videoLib.fetchedVideoList = fetchedVideoList.video;
    videoLib.subscribedVideoList = subscribedVideoList.video;
    // videoLib.subscribedVideoPackage = subscribedVideoPackage.video;
    // videoLib.searchByTitle = searchByTitle.titles;
    videoLib.selectSport = function (sport) {

        if (videoLib.status == 3) {
            videoLib.showUploadButton = true;
        }


        $ionicScrollDelegate.scrollTop();
        videoLib.showSpinner = true;
        videoLib.hideMenu();
        delete videoLib.searchThisVideo;
        $rootScope.sportSelected = sport;
        videoLib.selectTags(sport);
        // videoLib.subscribedVideoPackage = [];
        // videoLib.subscribedVideoList = [];
        var queryObject = _.find(fetchedVideoInfo.info, function (video) {
            return video.sportId == sport._id;
        });
        queryObject.userId = loggedInUser.info._id;
        //It does not show videos uploaded by LoggedInUser
        Video.getVideoBySportId(queryObject, function (data) {
            _.each(fetchedVideoInfo.info, function (list) {
                if (list.sportId == data.sportId) {
                    list.lastVideoFetchTime = data.lastVideoFetchTime;
                    _.each(data.videoList, function (video) {

                        if (!video.url.includes('youtube.com'))
                            video.thumbnail = backend + '/uploads/' + video.thumbnail
                        videoLib.fetchedVideoList.push(video);
                        if (_.indexOf(myBookmarkedVideo.bookmarkedVideos, video._id) != -1) {
                            video.subscribed = true;
                        }
                        else {
                            video.subscribed = false;
                        }
                        // videoLib.searchByTitle.push({title: video.title, sport: video.sport, videoId: video._id});
                        // tabHeightCalculate();
                    })

                    // console.log(videoLib.searchByTitle)

                    // _.each(Subscribed.Subscribed, function (subs) {
                    //     var package = [];
                    //     var packageBy;
                    //     var dataInserted = false;
                    //     _.each(videoLib.fetchedVideoList, function (video) {
                    //         if (subs == video.videoBy._id && video.sport == $rootScope.sportSelected._id) {
                    //             package.push(video);
                    //             packageBy = video.videoBy.firstName;
                    //             dataInserted = true;
                    //
                    //         }
                    //     })
                    //     if (dataInserted)
                    //         videoLib.subscribedVideoPackage.push({'packageBy': packageBy, 'package': package});
                    // })

                    // _.each(myBookmarkedVideo.bookmarkedVideos, function (book) {
                    //     _.each(videoLib.fetchedVideoList, function (video) {
                    //         if (book == video._id) {
                    //             videoLib.subscribedVideoList.push(video);
                    //         }
                    //     })
                    // })
                    // console.log(videoLib.fetchedVideoList)
                }
            })
            videoLib.showSpinner = false;
            switch (videoLib.status) {
                case 2:
                    videoLib.fetchReviewVideos();
                    break;
                case 3:
                    videoLib.fetchUploadVideos();
                    break;
            }
        }, function (err) {
            console.log(err);
            videoLib.showSpinner = false;
        })

        getCoachPackage();
    }

    videoLib.selectAction = function () {
        videoLib.add = !videoLib.add;
        videoLib.showActionDiv = true;
        videoLib.selectTags($rootScope.sportSelected);
    }

    videoLib.dropMenu = function () {
        videoLib.backdrop = true;
        $timeout(function () {
            videoLib.menuDropped = true;
        }, 100)
    }
    videoLib.hideMenu = function () {
        videoLib.backdrop = false;
        $timeout(function () {
            videoLib.menuDropped = false;
        }, 500)
    }
    videoLib.expand = function () {
        videoLib.expandlist = true;
    }
    videoLib.collapse = function () {
        videoLib.expandlist = false;
    }
    videoLib.selectRating = function (rating) {
        videoLib.selectedRating = rating;
    }
    videoLib.playReviewVideo = function (video) {
        $rootScope.showTabs = false;
        $state.go('coachSidemenu.tabs.playReviewVideo', {playThisVideo: video});
    }

    videoLib.addPackage = function () {
        videoLib.add = false;

        $scope.packageInfo =
        {
            tags: videoLib.selectedVideoTags,
            sportId: $rootScope.sportSelected._id,
            packageBy: loggedInUser.info._id
        }

        $scope.packageList = videoLib.packageList;
        console.log($scope.packageInfo);
        console.log($scope.packageList);
        var options =
        {
            title: 'Add Video Package',
            subTitle: $rootScope.sportSelected.sportName,
            cssClass: 'sportsPickPopup', // String, The custom CSS class name
            template: '<add-package package-info="packageInfo" package-list="packageList"></add-package>',
            scope: $scope
        }

        videoLib.showLevel2Div = false;
        videoLib.showLevel3Div = false;
        videoLib.deltaSelectedTags = [];
        videoLib.deltaSelectedTagsIds = {level1: [], level2: [], level3: []};
        packageAlertPopup = $ionicPopup.alert(options);
        IonicClosePopupService.register(packageAlertPopup);

    }


    videoLib.goToReview = function () {
        $state.go('coachSidemenu.tabs.reviewVideos')
    }
    videoLib.selectForReview = function () {
        $state.go('sidemenu.selectForReview')
    }


    //==================================================================================
    //==============================PLAYER's ACTIONS====================================


    videoLib.mySports = mySports.sports;
    videoLib.playerUploadedVideos = playerUploadedVideos;


    //==================================================================================
    //==============================Upload Player Video's ACTIONS====================================


    videoLib.selectVideo = function () {
        var options =
        {
            title: 'Choose a Video',
            cssClass: 'sportsPickPopup', // String, The custom CSS class name
            scope: $scope,
            template: '<div pick-video></div>',
        }
        videoAlertPopup = $ionicPopup.alert(options);

        IonicClosePopupService.register(videoAlertPopup);


    }


    videoLib.getPackageVideos = function (package) {
        videoLib.packageVideoList = [];
        videoLib.selectedPackage = package.packageName;
        videoLib.selectedPackageId = package._id;
        videoLib.packageVideoList = package.refToVideos;

        _.each(videoLib.packageVideoList, function (video) {

            if (!video.url.includes('youtube.com'))
                video.thumbnail = backend + '/uploads/' + video.thumbnail;


        })
    }

    videoLib.switchToVideoPackage = function () {
        videoLib.showPackages = true;
    }
    videoLib.switchToVideoList = function () {
        videoLib.showPackages = false;
    }


    videoLib.addVideoToPackage = function () {

    }


    var deltaSportSelected;

    function getCoachPackage() {
        Package.query({sportId: $rootScope.sportSelected._id, packageBy: loggedInUser.info._id},
            function (data) {
                videoLib.packageList = data;
            }, function (err) {
                console.log(err);
            })
    }

    videoLib.uploadVideo = function () {
        videoLib.add = false;
        videoLib.youtube = false;
        videoLib.youtubeVideoLoaded = false;
        videoLib.youtubeLink = '';
        videoLib.uploadVideoInfo = {};

        videoLib.mySportsListUpload = [];
        _.each(videoLib.mySports, function (sport) {
            if ($rootScope.sportSelected._id == sport._id)
                sport.selected = true;
            else
                sport.selected = false;
            videoLib.mySportsListUpload.push(sport)
        })

        deltaSportSelected = $rootScope.sportSelected;
        videoLib.selectVideo();
    };

    videoLib.changeVideoType = function () {
        videoLib.videoInfo.paidVideo = !videoLib.videoInfo.paidVideo;
    }


    $ionicModal.fromTemplateUrl('templates/player/uploadVideoModal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    })
        .then(function (modal) {
            uploadvideomodal = modal;
        });

    // capture callback
    var captureSuccess = function (mediaFiles) {
        var i, len;
        console.log('capture success')
        console.log(mediaFiles)
        for (i = 0, len = mediaFiles.length; i < len; i += 1) {
            filepath = mediaFiles[i].localURL;
            fullpath = mediaFiles[0].fullPath

            VideoEditor.getVideoInfo(
                successInfo, // success cb
                errorInfo, // error cb
                {
                    fileUri: fullpath, // the path to the video on the device
                }
            );
            // do something interesting with the file
        }
        videoLib.videoInfo = {
            refToTags: [],
            title: '',
            url: '',
            sport: $rootScope.sportSelected._id,
            paidVideo: true,
            package: videoLib.packageList[0],
            videoBy: loggedInUser.info._id,
            accountType: loggedInUser.info.player ? 'player' : 'coach',
            description: ''
        }

        videoLib.deltaSelectedTags = [];
        videoLib.deltaSelectedTagsIds = {level1: [], level2: [], level3: []};
        // $rootScope.showTagsDirective=true;
        uploadvideomodal.show();


        $timeout(function () {
            var player = jwplayer("videoPlayerdivModal").setup({
                "file": mediaFiles[0].fullPath,
                "image": "http://example.com/myImage.png",
                "height": 360 * (devWidth - 40) / 640,
                "width": devWidth - 40
            });

            jwplayer().on('play', function (e) {
                //call increase view api here
                playVideo.viewThisVideo(playVideo.videoInfo._id);
            });
            if (document.cookie.indexOf("jwplayerAutoStart") == -1) {
                document.cookie = "jwplayerAutoStart=1";
                player.on('ready', function () {
                    player.play();
                });
            }
            else {
            }
        }, 200)

        var v = "<video controls='controls'>";
        v += "<source src='" + mediaFiles[0].localURL + "' type='video/mp4'>";
        v += "</video>";
        document.querySelector("#videoArea").innerHTML = v;
    };

    // capture error callback
    var captureError = function (error) {
        navigator.notification.alert('Error code: ' + error.code, null, 'Capture Error');
    };

    videoLib.recordVideo = function () {
        videoAlertPopup.close();
        // start video capture
        navigator.device.capture.captureVideo(captureSuccess, captureError, {limit: 1});
    }
    videoLib.uploadThisVideo = function () {
        videoLib.titleError = false;
        // videoLib.videoInfo.package = videoLib.packageList[0];
        videoLib.descriptionError = false;
        console.log('uploading video to AWS S3')
        if (videoLib.videoInfo.title != '' && videoLib.videoInfo.description != '') {
            $ionicLoading.show({'template': '<div> Uploading {{percentageUploaded|number:0}}%</div>'});
            var test = uploadService.uploadImage(filepath, videoLib.videoInfo);
            test.then(function (data) {
                $ionicLoading.hide();
                if(window.cordova)
                {
                    $cordovaToast.showShortBottom('Video Uploaded Successfully');
                }
                else{
                    alert('Video Uploaded Successfully');
                }
                videoLib.hideUploadVideoModal();
                var videoObject = {
                    _id: data._id,
                    accountType: data.accountType,
                    createdAt: data.createdAt,
                    description: data.description,
                    dislikes: data.dislikes,
                    likes: data.likes,
                    numberOfViews: data.numberOfViews,
                    paidVideo: data.paidVideo,
                    refToFeedback: data.refToFeedback,
                    refToPackage: data.refToPackage,
                    refToTags: data.refToTags,
                    sport: data.sport,
                    subscribers: data.subscribers,
                    title: data.title,
                    updatedAt: data.updatedAt,
                    url: data.url,
                    videoBy: loggedInUser.info,
                    duration: data.duration
                };
                videoLib.playerUploadedVideos.videosList.push(videoObject);
                _.each(videoLib.packageList, function (package) {
                    if (package._id == videoLib.videoInfo.package._id) {
                        package.refToVideos.push(videoObject);
                    }
                })
            });
        }
        else {
            if (videoLib.videoInfo.title == '')
                videoLib.titleError = true;
            if (videoLib.videoInfo.description == '')
                videoLib.descriptionError = true;

        }

    }


    videoLib.selectTags = function (sport) {
        videoLib.selectedVideoTags = [];
        videoLib.selectedVideoTags = _.filter(tagList, function (tags) {
            tags.selected = false;
            return tags.sportId == sport._id
        })
    }

    videoLib.hideUploadVideoModal = function () {
        $rootScope.showTagsDirective = false;
        $rootScope.sportSelected = deltaSportSelected;
        uploadvideomodal.hide();
    }
    videoLib.addTags = function (tag) {
        tag.selected = !tag.selected;
        if (tag.selected)
            videoLib.videoInfo.refToTags.push(tag._id)
        else
            videoLib.videoInfo.refToTags = _.reject(videoLib.videoInfo.refToTags, function (myTag) {
                myTag == tag._id;
            })
    }

    videoLib.changeSport = function (sport) {
        videoLib.videoInfo.sport = sport._id;
        $rootScope.sportSelected = sport;
        _.each(videoLib.mySportsListUpload, function (mySport) {
            if (sport._id == mySport._id)
                mySport.selected = true;
            else
                mySport.selected = false;

        })
        videoLib.selectTags(sport)
    }

    videoLib.sortBy = -'numberOfViews';
    videoLib.deltaSortBy = -'numberOfViews';
    videoLib.sortByFunction = function (sortBy) {
        videoLib.deltaSortBy = sortBy;

    }


    videoLib.showFilterLevel2Div = false;
    videoLib.showFilterLevel3Div = false;
    videoLib.deltaFilterTags = [];
    videoLib.deltaFilterTagsIds = {level1: [], level2: [], level3: []};
    videoLib.deltaAppliedFilter = [];

    videoLib.filterByTags = function (tags) {
        tags.selected = !tags.selected;

        if (tags.selected)
            videoLib.deltaTagsFilter.push(tags._id)
        else
            videoLib.deltaTagsFilter = _.reject(videoLib.deltaTagsFilter, function (tagId) {
                return tagId == tags._id;
            })
    }

    videoLib.applyFilter = function () {
        videoLib.showPaid = videoLib.deltaShowPaid;
        videoLib.sortBy = videoLib.deltaSortBy;
        videoLib.appliedFilter = videoLib.deltaAppliedFilter;
        videoLib.modal.hide();

        $ionicScrollDelegate.resize();
    }


    //
    if ($stateParams.activeTab != '') {
        videoLib.status = $stateParams.activeTab;
        videoLib.selectSport($rootScope.sportSelected);//for updating the videolist with latest changes
    }

    videoLib.clearSearch = function () {
        videoLib.searchResults = [];
        videoLib.searchTitle = '';
        videoLib.showSpinner = false;
    }


    // videoLib.search = function (videoId) {
    //     console.log(videoId)
    //     videoLib.searchThisVideo = videoId;
    //     videoLib.searchTitle = ''
    // }

    videoLib.search = function (title) {
        videoLib.showSpinner = true;
        if (videoLib.searchResults.length != 0) {
            videoLib.clearSearch();
        }
        else {
            if (title.length > 2) {

                Video.searchVideoByTitle({title: title, sportId: $rootScope.sportSelected._id}, function (data) {

                        switch (videoLib.status) {
                            case 1:
                                videoLib.searchResults = data;
                                if (videoLib.searchResults.length == 0) {
                                    if(window.cordova) {
                                        $cordovaToast.showShortBottom('No results found');
                                    }
                                    else{
                                        alert('No results found');
                                    }
                                }
                                break;
                            case 2:
                                videoLib.searchResults.length = 0;
                                _.each(videoLib.videoForReview.videoList, function (video) {
                                    if (video.title.indexOf(title) !== -1) {

                                        if (!video.url.includes('youtube.com'))
                                            video.thumbnail = backend + '/uploads/' + video.thumbnail
                                        videoLib.searchResults.push(video);
                                    }
                                })
                                if (videoLib.searchResults.length == 0) {
                                    if(window.cordova) {
                                        $cordovaToast.showShortBottom('No results found');
                                    }
                                    else{
                                        alert('No results found');
                                    }                       
                                }

                                break;
                            case 3:
                                videoLib.searchResults = _.intersectionBy(data, videoLib.playerUploadedVideos.videosList, '_id');
                                if (videoLib.searchResults.length == 0) {
                                    if(window.cordova) {
                                        $cordovaToast.showShortBottom('No results found');
                                    }
                                    else{
                                        alert('No results found');
                                    }
                                }
                                break;
                        }

                    },
                    function (err) {
                        console.log(err);
                    })
                videoLib.showSpinner = false;
            }
            else {
                alert('minimum 3 character required');
                videoLib.showSpinner = false;
            }
        }
    }


    $ionicModal.fromTemplateUrl('templates/player/applyTagsModal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    })
        .then(function (modal) {
            applyTagsModal = modal;
            console.log('upload videos modal');
            videoLib.allLevelTagsList = allLevelTagsList.list;
        });
    videoLib.showTagsModal = function () {
        videoLib.getLevel1Tags($rootScope.sportSelected._id);
        applyTagsModal.show();
    };
    videoLib.hideTagsModal = function () {

        applyTagsModal.hide();

    };
    videoLib.getLevel3Tags = function (tagInfo) {

        _.each(videoLib.level2tags, function (tag) {
            if (tag._id == tagInfo._id) {
                tag.selected = true;
                videoLib.addLevel2Tag(tagInfo)
            }
            else
                tag.selected = false;
        })

        videoLib.level3tags = _.filter(videoLib.allLevelTagsList.level3Tags, function (tag) {
            tag.selected = false;
            return tag.tagLevel2Id == tagInfo._id;
        })

        videoLib.showLevel3Div = true;
    };
    videoLib.getLevel2Tags = function (tagInfo) {
        _.each(videoLib.level1tags, function (tag) {
            if (tag._id == tagInfo._id) {
                tag.selected = true;
                videoLib.addLevel1Tag(tagInfo)
            }
            else
                tag.selected = false;
        })

        videoLib.level2tags = _.filter(videoLib.allLevelTagsList.level2Tags, function (tag) {
            tag.selected = false;
            return tag.tagLevel1Id == tagInfo._id;
        })
        videoLib.level3tags = [];
        videoLib.showLevel2Div = true;
        videoLib.showLevel3Div = false;
    };
    videoLib.getLevel1Tags = function (sportId) {

        videoLib.level1tags = _.filter(videoLib.allLevelTagsList.level1Tags, function (tag) {
            tag.selected = false;
            return tag.sportId == sportId;
        })

        videoLib.showLevel2Div = false;
        videoLib.showLevel3Div = false;

    };
    videoLib.addLevel1Tag = function (tagInfo) {
        var index = _.findIndex(videoLib.deltaSelectedTags, function (tags) {
            return tagInfo._id == tags.level1;
        })

        if (index == -1) {
            videoLib.deltaSelectedTags.push({level1: tagInfo._id, level1Tag: tagInfo.tagLevel1});
            videoLib.deltaSelectedTagsIds.level1.push(tagInfo._id);
        }

    };
    videoLib.addLevel2Tag = function (tagInfo) {
        var index = _.findIndex(videoLib.deltaSelectedTags, function (tags) {
            return tagInfo.tagLevel1Id == tags.level1;
        })

        if (index != -1) {

            if (typeof videoLib.deltaSelectedTags[index].level2TagsList == 'undefined')//checking whether it already has some tags or not
                videoLib.deltaSelectedTags[index].level2TagsList = [];

            var tagIndex = _.findIndex(videoLib.deltaSelectedTags[index].level2TagsList, function (tags) {
                return tagInfo._id == tags.level2;
            })
            if (tagIndex == -1) {
                videoLib.deltaSelectedTags[index].level2TagsList.push({
                    level2: tagInfo._id,
                    level2Tag: tagInfo.tagLevel2
                });
                videoLib.deltaSelectedTagsIds.level2.push(tagInfo._id)
            }
        }


    };
    videoLib.addLevel3Tag = function (tagInfo) {

        _.each(videoLib.level3tags, function (tag) {
            if (tag._id == tagInfo._id) {
                tag.selected = true;
            }
            else
                tag.selected = false;
        })
        var level2Index = -1, level1Index = -1;
        _.each(videoLib.deltaSelectedTags, function (level1, index) {
            if (level2Index == -1) {
                level2Index = _.findIndex(level1.level2TagsList, function (level2) {
                    if (level2.level2 == tagInfo.tagLevel2Id) {
                        level1Index = index;
                        return level2;
                    }
                })

                if (level1Index != -1) {
                    if (typeof videoLib.deltaSelectedTags[level1Index].level2TagsList[level2Index].level3TagsList == 'undefined')//checking whether it already has some tags or not
                    {
                        videoLib.deltaSelectedTags[level1Index].level2TagsList[level2Index].level3TagsList = [];
                    }


                    var tagIndex = _.findIndex(videoLib.deltaSelectedTags[level1Index].level2TagsList[level2Index].level3TagsList, function (tags) {
                        return tagInfo._id == tags.level3;
                    })
                    if (tagIndex == -1) {
                        videoLib.deltaSelectedTags[level1Index].level2TagsList[level2Index].level3TagsList.push(
                            {
                                level3: tagInfo._id,
                                level3Tag: tagInfo.tagLevel3
                            }
                        )

                        videoLib.deltaSelectedTagsIds.level3.push(tagInfo._id)
                    }
                }
            }
        })

    };
    videoLib.removeLevel1Tags = function (removeLevel1TagId) {
        videoLib.deltaSelectedTags = _.reject(videoLib.deltaSelectedTags, function (tags) {
            return tags.level1 == removeLevel1TagId;
        })
        videoLib.deltaSelectedTagsIds.level1 = _.reject(videoLib.deltaSelectedTagsIds.level1, function (tags) {
            return tags == removeLevel1TagId;
        })

    };
    videoLib.removeLevel2Tags = function (removeLevel1TagId, removeLevel2TagId) {
        var indexLevel1 = _.findIndex(videoLib.deltaSelectedTags, function (tags) {
            return tags.level1 == removeLevel1TagId;
        })

        videoLib.deltaSelectedTags[indexLevel1].level2TagsList = _.reject(videoLib.deltaSelectedTags[indexLevel1].level2TagsList, function (tags) {
            return tags.level2 == removeLevel2TagId;
        })


        videoLib.deltaSelectedTagsIds.level2 = _.reject(videoLib.deltaSelectedTagsIds.level2, function (tags) {
            return tags == removeLevel2TagId;
        })
    };
    videoLib.removeLevel3Tags = function (removeLevel1TagId, removeLevel2TagId, removeLevel3TagId) {

        var indexLevel1 = _.findIndex(videoLib.deltaSelectedTags, function (tags) {
            return tags.level1 == removeLevel1TagId;
        })

        var indexLevel2 = _.findIndex(videoLib.deltaSelectedTags[indexLevel1].level2TagsList, function (tags) {
            return tags.level2 == removeLevel2TagId;
        })

        videoLib.deltaSelectedTags[indexLevel1].level2TagsList[indexLevel2].level3TagsList =
            _.reject(videoLib.deltaSelectedTags[indexLevel1].level2TagsList[indexLevel2].level3TagsList, function (tags) {
                return tags.level3 == removeLevel3TagId;
            })

        videoLib.deltaSelectedTagsIds.level3 = _.reject(videoLib.deltaSelectedTagsIds.level3, function (tags) {
            return tags == removeLevel3TagId;
        })
    };
    videoLib.applyTags = function () {

        videoLib.videoInfo.tagsLevel1 = videoLib.deltaSelectedTagsIds.level1;
        videoLib.videoInfo.tagsLevel2 = videoLib.deltaSelectedTagsIds.level2;
        videoLib.videoInfo.tagsLevel3 = videoLib.deltaSelectedTagsIds.level3;
        videoLib.hideTagsModal();
    };


    var deltaSportSelected;
    videoLib.showAddPackageModal = function () {

        $ionicModal.fromTemplateUrl('templates/coach/addPackageModal.html', {
            scope: $scope,
            animation: 'slide-in-up'
        })
            .then(function (modal) {
                addPackageModal = modal;
            }).then(function () {
            deltaSportSelected=$rootScope.sportSelected;
            videoLib.packageInfo = {};
            videoLib.deltaSelectedTags = [];
            videoLib.deltaSelectedTagsIds = {level1: [], level2: [], level3: []};
            addPackageModal.show();
        });
    }

    videoLib.hideAddPackageModal = function () {
        addPackageModal.hide();
        $rootScope.sportSelected=deltaSportSelected;
    }

    videoLib.uploadPackage = function () {
        videoLib.packageInfo.sportId=deltaSportSelected._id;
        videoLib.packageInfo.packageBy=loggedInUser.info._id;
        Package.save({packageInfo: videoLib.packageInfo}, function (data) {
            videoLib.packageList.push(data);
        }, function (err) {
            console.log(err);
        })
        videoLib.hideAddPackageModal();
    }


}
