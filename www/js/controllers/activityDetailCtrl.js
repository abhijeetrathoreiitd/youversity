angular.module('starter')

    .controller('activityDetailCtrl', activityDetailCtrl);

function activityDetailCtrl($stateParams, $scope, $ionicModal, currentWeekParams, $sce, $timeout, $window) {
    var activityDetail = this;
    var devHeight = $window.innerHeight;
    var devWidth = $window.innerWidth;

    activityDetail.videoFrame = {'width': devWidth + 'px', 'height': 9 * devWidth / 16 + 'px'};
    activityDetail.day = $stateParams.details.day;
    activityDetail.week = currentWeekParams.week + 1;
    activityDetail.activityList = $stateParams.details.activities;
    activityDetail.trainingPlanDetails = $stateParams.details.trainingPlanId;
    activityDetail.sport = $stateParams.details.sport;
    activityDetail.openActivityDetailModal = function (activity) {

        $ionicModal.fromTemplateUrl('templates/player/activityDetailModal.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            activityDetail.modal = modal;
            activityDetail.activityInModal = activity;
        }).then(function () {

            activityDetail.modal.show();
        }).then(function () {
            if (activityDetail.activityInModal.videoId != null && activityDetail.activityInModal.videoId.url.includes('youtube.com')) {
                activityDetail.isYoutubeVideo = true;
                activityDetail.trustedLink = $sce.trustAsResourceUrl(activityDetail.activityInModal.videoId.url + '?showinfo=0');
            }
            else {
                activityDetail.isYoutubeVideo = false;
            }

            if (activityDetail.activityInModal.videoId != null && !activityDetail.isYoutubeVideo) {
                $timeout(function () {
                    var player = jwplayer("videoPlayerdiv").setup({
                        "file": activityDetail.activityInModal.videoId.url,
                        "image": "img/icon.png",
                        "height": 360 * devWidth / 640,
                        "width": devWidth
                    });

                    jwplayer().on('play', function (e) {
                        //call increase view api here
                    });
                    if (document.cookie.indexOf("jwplayerAutoStart") == -1) {
                        document.cookie = "jwplayerAutoStart=1";
                        player.on('ready', function () {
                            player.play();
                        });
                    }
                    else {
                    }
                }, 200)
            }

        })

    }


    activityDetail.hideFilters = function () {

        activityDetail.modal.hide();
    }

    activityDetail.completeActivity = function (activity) {
        activity.done = true;
    }

}
