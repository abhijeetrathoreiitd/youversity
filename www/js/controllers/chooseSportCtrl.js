angular.module('starter')

    .controller('chooseSportCtrl',chooseSportCtrl);
function chooseSportCtrl ($window, $state,addSportsExperience,loggedInUser,SportsList,mySports,$stateParams) {

        var choose = this;
        choose.showCloseButton=$stateParams.showButton;
        //-------------------------DESIGN CALCULATION START-------------------------//
        var devHeight = $window.innerHeight;
        choose.title = {'margin-top':0.06*devHeight+'px'};
        choose.subtitle = {'margin-bottom': 0.04 * devHeight + 'px'};
        choose.fullHeight = {'height': devHeight + 'px'};
        choose.nextButton ={'bottom':0.02*devHeight+'px','width':100+'%'}

        //-------------------------DESIGN CALCULATION END-------------------------//

        choose.goToProfile = function () {
            $state.go('sidemenu.profile');
        }

        choose.goToChooseSport = function () {
            $state.go('sidemenu.chooseSport');
        }

    choose.sportsList=[];
    
    choose.return=function()
    {
        if(loggedInUser.info.player)
        $state.go('sidemenu.myProfile');
        else
            $state.go('coachSidemenu.myProfile')
    }
    
    _.each(SportsList.list,function(sport)
    {
        if(_.indexOf(_.map(mySports.sports,'_id'),sport._id)==-1)
        sport.selected=false;
        else
        sport.selected=true;
        choose.sportsList.push(sport)
    

    });
        choose.sport=function(id)
        {

            if(_.indexOf(_.map(mySports.sports,'_id'),id)==-1) {
                addSportsExperience.sportId = id;
                if (loggedInUser.info.player)
                    $state.go('sidemenu.profile');
                else
                    $state.go('coachSidemenu.profile');
            }
            else
            {
                console.log('added already')
            }
        }
    



    }
