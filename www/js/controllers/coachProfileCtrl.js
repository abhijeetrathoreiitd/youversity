angular.module('starter')

    .controller('coachProfileCtrl', coachProfileCtrl)
function coachProfileCtrl($window, $state,userProfileParams, loggedInUser, Follow, findPersonResult, Following,$timeout,Video) {
    console.log('coach profile ctrl');
    var coachProfile = this;
    coachProfile.videos = 0;
    coachProfile.status = 1;
    coachProfile.prevStatus = 1;
    coachProfile.menuDropped = false;
    coachProfile.backdrop = false;
    coachProfile.coachInfo=userProfileParams.params;

    //-------------------------DESIGN CALCULATION START-------------------------//
    var devHeight = $window.innerHeight;
    var devWidth = $window.innerWidth;


    coachProfile.logoDiv = {
        'margin-top': 0.1 * devHeight + 'px',
        'height': 0.2 * devHeight + 'px',
        'margin-bottom': 0.04 * devHeight + 'px'
    };
    coachProfile.datum = {
        'left': devWidth + 'px'
    };
    coachProfile.scrollerHeight = {
        'height': devHeight - 200 - 92 + 'px'
    };

    $timeout(function () {
        var tabHeight1 = document.querySelector('#info').offsetHeight;
        var tabHeight2 = document.querySelector('#exp').offsetHeight;
        var tabHeight3 = document.querySelector('#videos').offsetHeight;
        var minHeight = Math.max(devHeight - 200 - 88, tabHeight1, tabHeight2, tabHeight3)
        console.log(minHeight);

        coachProfile.info = {
            'min-height': Math.max(devHeight - 200 - 88, tabHeight1, tabHeight2, tabHeight3) + 'px'
        }
        coachProfile.exp = {
            'min-height': Math.max(devHeight - 200 - 88, tabHeight1, tabHeight2, tabHeight3) + 'px'
        }
        coachProfile.videos = {
            'min-height': Math.max(devHeight - 200 - 88, tabHeight1, tabHeight2, tabHeight3) + 'px'
        }
    }, 200);
    //-------------------------DESIGN CALCULATION END-------------------------//


    coachProfile.return = function () {
        $state.go('sidemenu.tabs.findCoach');
        // $ionicNavBarDelegate.showBar(true);
    }

    coachProfile.browseVideos = function () {
        coachProfile.videos = 1;
    }
    coachProfile.browseBasic = function () {
        coachProfile.videos = 2;
    }

    coachProfile.changePrevStatus = function (number) {
        coachProfile.status = number;
        $timeout(function () {
            coachProfile.prevStatus = number;
        }, 500)
    }

    coachProfile.dropMenu = function () {
        coachProfile.backdrop = true;
        coachProfile.menuDropped = true;
        console.log(coachProfile.backdrop);
    }
    coachProfile.hideMenu = function () {
        coachProfile.menuDropped = false;
        $timeout(function () {
            coachProfile.backdrop = false;
        }, 300)
    }



    coachProfile.coachVideoList=[];

    coachProfile.getVideos=function(coachId)
    {
        Video.get({userId:coachId,lastVideoFetchTime:''},function(data)
        {

            _.each(data.videoList,function(video)
            {
                coachProfile.coachVideoList.push(video);
            })
        },
        function(err)
        {
            console.log(err);
        })
    }

    coachProfile.getVideos(coachProfile.coachInfo._id);

    coachProfile.playVideo = function (video) {
        if (!loggedInUser.info.player) {
            $state.go('sidemenu.tabs.playVideoCoachProfileSearch', {videoInfo: video});
        }
        else {
            $state.go('sidemenu.tabs.playVideoCoachProfileSearch', {videoInfo: video});
        }
    };



    coachProfile.followUser=function(following)
    {

        Follow.save({following:following,userId:loggedInUser.info._id},function(data)
        {
            console.log(data);

            coachProfile.coachInfo.follow=true;
            Following.list.push(following);
        },function(err)
        {
            console.log(err);
        })
    }


    console.log(findPersonResult.coaches)
    coachProfile.unFollowUser=function(following)
    {

        Follow.unfollow({following:following,userId:loggedInUser.info._id},function(data)
        {
            coachProfile.coachInfo.follow=false;

            Following.list=_.reject(Following.list,function(item)
            {
                return item==following;
            });
        },function(err)
        {
            console.log(err);
        })
    }
}
