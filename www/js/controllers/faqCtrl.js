angular.module('starter')

    .controller('faqCtrl',faqCtrl)
function faqCtrl ($window,$ionicModal, $state, $ionicPopup, IonicClosePopupService) {
        var faq = this;
        faq.expanded= false;

        faq.questions = [];
        for (var i=0; i<10; i++) {
            faq.questions[i] = {
                name: i,
                answers: [],
                show: false
            };
            for (var j=0; j<3; j++) {
                faq.questions[i].answers.push(i + '-' + j);
            }
        }

        faq.askQuestion = function () {
            var options =
            {
                title: 'Ask Your Question',
                cssClass: 'sportsPickPopup', // String, The custom CSS class name
                template: '<div ask-question></div>'
            }
            questionAlertPopup = $ionicPopup.alert(options);

            IonicClosePopupService.register(questionAlertPopup);

        }

        //-------------------------DESIGN CALCULATION START-------------------------//
        var devHeight = $window.innerHeight;
        var devWidth = $window.innerWidth;
        
        //-------------------------DESIGN CALCULATION END-------------------------//
        
        faq.return = function () {
            $state.go('sidemenu.tabs.training');
        }
        faq.toggleQuestion = function(question){
            question.show = !question.show;
        }
        faq.isQuestionShown = function(question) {
            return question.show;
        };

    }
