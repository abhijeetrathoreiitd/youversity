angular.module('starter')

    .controller('findCoachCtrl',findCoachCtrl)

function findCoachCtrl($window,$timeout, $state,SportsList, loggedInUser, $ionicModal, $scope, $rootScope, $ionicNavBarDelegate) {
        var findCoach = this;
        //Whether search bar is opened or not
        findCoach.searchOpen = false;
        findCoach.searchOpenLate = false;
        findCoach.menuDropped = false;
        findCoach.backdrop = false;
        
        
        $scope.$on('$ionicView.enter', function (e) {
            $ionicNavBarDelegate.showBar(true);

        });

        $ionicModal.fromTemplateUrl('templates/findcoachmodal.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            findCoach.modal = modal;
        });
        findCoach.showFilters = function () {
            findCoach.modal.show();
        }
        findCoach.hideFilters = function () {
            findCoach.modal.hide();
        }
        findCoach.showSearch = function(){
            findCoach.searchOpen=!findCoach.searchOpen;
            $timeout(function() {
                findCoach.searchOpenLate = !findCoach.searchOpenLate;
            },200)
        }
        // findCoach.hideSearch = function(){
        //     findCoach.searchOpen=false;
        //     $timeout(function(){
        //         findCoach.searchOpenLate=false;
        //     },200)
        // }

        findCoach.selectSport = function (sport) {
            $rootScope.sportSelected = sport;
        }

        findCoach.dropMenu = function(){
            findCoach.backdrop = true;
            findCoach.menuDropped = true;
        }
        findCoach.hideMenu = function(){
            findCoach.menuDropped = false;
            $timeout(function(){
                findCoach.backdrop = false;
            },500)
        }


        //-------------------------DESIGN CALCULATION START-------------------------//
        var devHeight = $window.innerHeight;
        var devWidth = $window.innerWidth;
        console.log('width:'+devWidth + 'height:' + devHeight);

        findCoach.logoDiv = {
            'margin-top': 0.1 * devHeight + 'px',
            'height': 0.2 * devHeight + 'px',
            'margin-bottom': 0.04 * devHeight + 'px'
        };

        findCoach.sportScroll = {'width': 0.7 * devWidth + 'px'};
        findCoach.placeholder = {
            'position': 'absolute',
            'top': 0.3 * (devHeight - 44 - 49) + 'px',
            'width': 100 + '%'
        };
        findCoach.general = {'height': devHeight-88 + 'px'};

        //-------------------------DESIGN CALCULATION END-------------------------//

        findCoach.goToCoachProfile = function () {
            $state.go('sidemenu.tabs.coachProfile');
            // $ionicNavBarDelegate.showBar(false);
        }




    //==================================================================================
    //==============================PLAYER's ACTIONS====================================


    findCoach.mySports=[];

    _.each(SportsList.list,function(sport)
    {
        _.each(loggedInUser.info.mySports,function(mySport)
        {
            if(mySport==sport._id)
                findCoach.mySports.push(sport);
        })
    })



    //==================================================================================
    //==============================PLAYER's END========================================

    }
