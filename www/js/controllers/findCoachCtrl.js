angular.module('starter')

    .controller('findCoachCtrl', findCoachCtrl)

function findCoachCtrl($window, $timeout, FindCoach,SportsList, findCoachResult, GetUserStats, userProfileParams,
                       Following, $state, loggedInUser, $ionicModal, $scope, $rootScope, $ionicNavBarDelegate) {


    var findCoach = this;
    //Whether search bar is opened or not
    findCoach.searchOpen = false;
    findCoach.searchOpenLate = false;
    findCoach.menuDropped = false;
    findCoach.backdrop = false;
    findCoach.selectedRating = 3;
    findCoach.cost = {
        min: '20',
        max: '100',
        value: '50'
    }
    findCoach.distance = {
        min: '5',
        max: '100',
        value: '20'
    }


    findCoach.following = Following.list;
    console.log(findCoach.following);

    $scope.$on('$ionicView.enter', function (e) {
        $ionicNavBarDelegate.showBar(true);

    });

    $ionicModal.fromTemplateUrl('templates/player/findcoachmodal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modal) {
        findCoach.modal = modal;
    });
    findCoach.showFilters = function () {
        findCoach.modal.show();
    }
    findCoach.hideFilters = function () {
        findCoach.modal.hide();
    }
    findCoach.showSearch = function () {
        findCoach.searchOpen = true;
        $timeout(function () {
            findCoach.searchOpenLate = true;
        }, 400)
    }
    findCoach.hideSearch = function () {
        findCoach.searchOpen = false;
        $timeout(function () {
            findCoach.searchOpenLate = false;
        }, 100)
    }

    findCoach.selectSport = function (sport) {
        $rootScope.sportSelected = sport;

        findCoach.backdrop = false;
        $timeout(function () {
            findCoach.menuDropped = false;
        }, 500)


        findCoach.coachList = [];
        if (_.indexOf(_.map(findCoachResult.coaches, 'sportId'), $rootScope.sportSelected._id) == -1) {
            console.log('api call');
            FindCoach.query({sportId: $rootScope.sportSelected._id, player: false,searchText:''}, function (data) {
                _.each(data, function (coach) {
                    if (_.indexOf(findCoach.following, coach._id) == -1) {
                        coach.follow = false;
                    }
                    else {
                        coach.follow = true;
                    }
                    findCoach.coachList.push(coach);
                })


                findCoachResult.coaches.push({coachList: data, sportId: $rootScope.sportSelected._id});
                console.log(findCoach.coachList);
            }, function (err) {
                console.log(err);
            })
        }
        else {
            console.log('service');
            var filterResult = _.filter(findCoachResult.coaches, function (coach) {
                return coach.sportId == $rootScope.sportSelected._id;
            });

            _.each(filterResult[0].coachList, function (coach) {
                if (_.indexOf(findCoach.following, coach._id) == -1) {
                    coach.follow = false;
                }
                else {
                    coach.follow = true;
                }
                findCoach.coachList.push(coach);
            })


            console.log(findCoach.coachList);

        }
    }

    if ($rootScope.sportSelected)//calling selectSport whenever page is loaded
        findCoach.selectSport($rootScope.sportSelected);


    findCoach.dropMenu = function () {
        findCoach.backdrop = true;
        $timeout(function () {
            findCoach.menuDropped = true;
        }, 100)
    }
    findCoach.hideMenu = function () {
        findCoach.backdrop = false;
        $timeout(function () {
            findCoach.menuDropped = false;
        }, 500)
    }

    findCoach.selectRating = function (rating) {
        findCoach.selectedRating = rating;
    }

    //-------------------------DESIGN CALCULATION START-------------------------//
    var devHeight = $window.innerHeight;
    var devWidth = $window.innerWidth;
    console.log('width:' + devWidth + 'height:' + devHeight);

    findCoach.logoDiv = {
        'margin-top': 0.1 * devHeight + 'px',
        'height': 0.2 * devHeight + 'px',
        'margin-bottom': 0.04 * devHeight + 'px'
    };


    findCoach.sportScroll = {'width': 0.7 * devWidth + 'px'};
    findCoach.placeholder = {
        'position': 'absolute',
        'top': 0.3 * (devHeight - 44 - 49) + 'px',
        'width': 100 + '%'
    };
    findCoach.general = {'height': devHeight - 88 + 'px'};
    findCoach.slidermin = {'left': 0.2 * (devWidth - 22) + 13 + 'px'};
    findCoach.sliderWidth = devWidth - (0.2 * (devWidth - 22) + 20 + 48 + 22 + 3) - 30;
    findCoach.slidervalue = {'left': 0.2 * (devWidth - 22) + 11 + 24 + 10 + ((parseInt(findCoach.cost.value, 10) - 20) * findCoach.sliderWidth / 80) + 'px'};
    findCoach.slidervalue2 = {'left': 0.2 * (devWidth - 22) + 11 + 24 + 10 + ((parseInt(findCoach.distance.value, 10) - 5) * findCoach.sliderWidth / 95) + 'px'};

    // console.log(0.2*(devWidth-22)+11+24+10)
    // console.log((parseInt(findCoach.cost.value,10)*findCoach.sliderWidth/100))
    // console.log(0.2*(devWidth-22)+11+24+10+(parseInt(findCoach.cost.value,10)*findCoach.sliderWidth/100))
    //
    // console.log(findCoach.sliderWidth)

    //-------------------------DESIGN CALCULATION END-------------------------//

    $scope.$watch(angular.bind(findCoach, function () {
        return findCoach.cost.value;
    }), function (value) {
        findCoach.slidervalue = {'left': 0.2 * (devWidth - 22) + 11 + 24 + 10 + ((parseInt(findCoach.cost.value, 10) - 20) * findCoach.sliderWidth / 80) + 'px'};
    });
    $scope.$watch(angular.bind(findCoach, function () {
        return findCoach.distance.value;
    }), function (value) {
        findCoach.slidervalue2 = {'left': 0.2 * (devWidth - 22) + 11 + 24 + 10 + ((parseInt(findCoach.distance.value, 10) - 5) * findCoach.sliderWidth / 95) + 'px'};
    });
    findCoach.goToCoachProfile = function (coachInfo) {
        userProfileParams.params = coachInfo;

        var currentlyTrainingPromise = GetUserStats.get({coachId: coachInfo._id}, function (result) {
            console.log(result);
            coachInfo.currentlyTraining = result.currentlyTraining;
            coachInfo.followerCount = result.followerCount;
            coachInfo.followingCount = result.followingCount;
            coachInfo.tournamentList = result.tournamentList;
        }, function (err) {
            console.log(err);
        })

        currentlyTrainingPromise['$promise'].then(function () {

            if (!loggedInUser.info.player) {
                $state.go('coachSidemenu.coachCoachProfile');
            }
            else {

                $state.go('sidemenu.tabs.playerCoachProfile');
            }
        })
    }


    findCoach.mySports = [];

    _.each(SportsList.list, function (sport) {
        _.each(loggedInUser.info.mySports, function (mySport) {
            if (mySport == sport._id)
                findCoach.mySports.push(sport);
        })
    })

    findCoach.coachSearchList=[];
    findCoach.findSearchResults = function (searchText) {
        console.log(searchText)
        console.log($rootScope.sportSelected._id)
        FindCoach.query({sportId: $rootScope.sportSelected._id, player: false,searchText:searchText},function(result)
        {
            findCoach.coachSearchList=result;
        },function(err)
        {
            console.log(err);
        })
    }


    findCoach.clearSearch = function () {
        findCoach.coachSearchList=[];
    }

}
