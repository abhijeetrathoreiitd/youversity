angular.module('starter')

    .controller('findPersonCtrl', findPersonCtrl)

function findPersonCtrl($window, $timeout, FindPerson,SportsList, findPersonResult, GetUserStats, userProfileParams,
                       Following, $state, loggedInUser, $ionicModal, $scope, $rootScope, $ionicNavBarDelegate) {


    var findPerson = this;
    //Whether search bar is opened or not
    findPerson.searchOpen = false;
    findPerson.searchOpenLate = false;
    findPerson.menuDropped = false;
    findPerson.backdrop = false;
    findPerson.selectedRating = 3;
    findPerson.cost = {
        min: '20',
        max: '100',
        value: '50'
    }
    findPerson.distance = {
        min: '5',
        max: '100',
        value: '20'
    }


    findPerson.following = Following.list;
    console.log(findPerson.following);

    $scope.$on('$ionicView.enter', function (e) {
        $ionicNavBarDelegate.showBar(true);

    });

    $ionicModal.fromTemplateUrl('templates/player/findPersonModal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modal) {
        findPerson.modal = modal;
    });
    findPerson.showFilters = function () {
        findPerson.modal.show();
    }
    findPerson.hideFilters = function () {
        findPerson.modal.hide();
    }
    findPerson.showSearch = function () {
        findPerson.searchOpen = true;
        $timeout(function () {
            findPerson.searchOpenLate = true;
        }, 400)
    }
    findPerson.hideSearch = function () {
        findPerson.searchOpen = false;
        $timeout(function () {
            findPerson.searchOpenLate = false;
        }, 100)
    }

    findPerson.selectSport = function (sport) {
        $rootScope.sportSelected = sport;

        findPerson.backdrop = false;
        $timeout(function () {
            findPerson.menuDropped = false;
        }, 500)


        findPerson.coachList = [];
        if (_.indexOf(_.map(findPersonResult.coaches, 'sportId'), $rootScope.sportSelected._id) == -1) {
            console.log('api call');
            FindPerson.query({sportId: $rootScope.sportSelected._id,searchText:''}, function (data) {
                console.log(data)
                _.each(data, function (coach) {
                    if (_.indexOf(findPerson.following, coach._id) == -1) {
                        coach.follow = false;
                    }
                    else {
                        coach.follow = true;
                    }
                    findPerson.coachList.push(coach);
                })


                findPersonResult.coaches.push({coachList: data, sportId: $rootScope.sportSelected._id});
                console.log(findPerson.coachList);
            }, function (err) {
                console.log(err);
            })
        }
        else {
            console.log('service');
            var filterResult = _.filter(findPersonResult.coaches, function (coach) {
                return coach.sportId == $rootScope.sportSelected._id;
            });

            _.each(filterResult[0].coachList, function (coach) {
                if (_.indexOf(findPerson.following, coach._id) == -1) {
                    coach.follow = false;
                }
                else {
                    coach.follow = true;
                }
                findPerson.coachList.push(coach);
            })


            console.log(findPerson.coachList);

        }
    }

    if ($rootScope.sportSelected)//calling selectSport whenever page is loaded
        findPerson.selectSport($rootScope.sportSelected);


    findPerson.dropMenu = function () {
        findPerson.backdrop = true;
        $timeout(function () {
            findPerson.menuDropped = true;
        }, 100)
    }
    findPerson.hideMenu = function () {
        findPerson.backdrop = false;
        $timeout(function () {
            findPerson.menuDropped = false;
        }, 500)
    }

    findPerson.selectRating = function (rating) {
        findPerson.selectedRating = rating;
    }

    //-------------------------DESIGN CALCULATION START-------------------------//
    var devHeight = $window.innerHeight;
    var devWidth = $window.innerWidth;
    console.log('width:' + devWidth + 'height:' + devHeight);

    findPerson.logoDiv = {
        'margin-top': 0.1 * devHeight + 'px',
        'height': 0.2 * devHeight + 'px',
        'margin-bottom': 0.04 * devHeight + 'px'
    };


    findPerson.sportScroll = {'width': 0.7 * devWidth + 'px'};
    findPerson.placeholder = {
        'position': 'absolute',
        'top': 0.3 * (devHeight - 44 - 49) + 'px',
        'width': 100 + '%'
    };
    findPerson.general = {'height': devHeight - 88 + 'px'};
    findPerson.slidermin = {'left': 0.2 * (devWidth - 22) + 13 + 'px'};
    findPerson.sliderWidth = devWidth - (0.2 * (devWidth - 22) + 20 + 48 + 22 + 3) - 30;
    findPerson.slidervalue = {'left': 0.2 * (devWidth - 22) + 11 + 24 + 10 + ((parseInt(findPerson.cost.value, 10) - 20) * findPerson.sliderWidth / 80) + 'px'};
    findPerson.slidervalue2 = {'left': 0.2 * (devWidth - 22) + 11 + 24 + 10 + ((parseInt(findPerson.distance.value, 10) - 5) * findPerson.sliderWidth / 95) + 'px'};

    // console.log(0.2*(devWidth-22)+11+24+10)
    // console.log((parseInt(findPerson.cost.value,10)*findPerson.sliderWidth/100))
    // console.log(0.2*(devWidth-22)+11+24+10+(parseInt(findPerson.cost.value,10)*findPerson.sliderWidth/100))
    //
    // console.log(findPerson.sliderWidth)

    //-------------------------DESIGN CALCULATION END-------------------------//

    $scope.$watch(angular.bind(findPerson, function () {
        return findPerson.cost.value;
    }), function (value) {
        findPerson.slidervalue = {'left': 0.2 * (devWidth - 22) + 11 + 24 + 10 + ((parseInt(findPerson.cost.value, 10) - 20) * findPerson.sliderWidth / 80) + 'px'};
    });
    $scope.$watch(angular.bind(findPerson, function () {
        return findPerson.distance.value;
    }), function (value) {
        findPerson.slidervalue2 = {'left': 0.2 * (devWidth - 22) + 11 + 24 + 10 + ((parseInt(findPerson.distance.value, 10) - 5) * findPerson.sliderWidth / 95) + 'px'};
    });
    findPerson.goToCoachProfile = function (coachInfo) {
        userProfileParams.params = coachInfo;

        var currentlyTrainingPromise = GetUserStats.get({coachId: coachInfo._id}, function (result) {
            coachInfo.currentlyTraining = result.currentlyTraining;
            coachInfo.followerCount = result.followerCount;
            coachInfo.followingCount = result.followingCount;
            coachInfo.tournamentList = result.tournamentList;
        }, function (err) {
            console.log(err);
        })

        currentlyTrainingPromise['$promise'].then(function () {

            if (!loggedInUser.info.player) {
                $state.go('coachSidemenu.coachCoachProfile');
            }
            else {

                $state.go('sidemenu.tabs.playerCoachProfile');
            }
        })
    }


    findPerson.mySports = [];

    _.each(SportsList.list, function (sport) {
        _.each(loggedInUser.info.mySports, function (mySport) {
            if (mySport == sport._id)
                findPerson.mySports.push(sport);
        })
    })

    findPerson.personSearchList=[];
    findPerson.findSearchResults = function (searchText) {
        FindPerson.query({sportId: $rootScope.sportSelected._id,searchText:searchText},function(result)
        {
            findPerson.personSearchList=result;
        },function(err)
        {
            console.log(err);
        })
    }


    findPerson.clearSearch = function () {
        findPerson.personSearchList=[];
    }

}
