angular.module('starter')

    .controller('findPlayer2Ctrl',findPlayer2Ctrl)

function findPlayer2Ctrl($window,$timeout, $state, loggedInUser, $ionicModal, $scope, $rootScope, $ionicNavBarDelegate,SportsList) {
        var findPlayer2 = this;
        //Whether search bar is opened or not
        findPlayer2.searchOpen = false;
        findPlayer2.searchOpenLate = false;
        findPlayer2.menuDropped = false;
        findPlayer2.backdrop = false;
        
        
        $scope.$on('$ionicView.enter', function (e) {
            $ionicNavBarDelegate.showBar(true);

        });

        $ionicModal.fromTemplateUrl('templates/findPlayer2modal.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            findPlayer2.modal = modal;
        });
        findPlayer2.showFilters = function () {
            findPlayer2.modal.show();
        }
        findPlayer2.hideFilters = function () {
            findPlayer2.modal.hide();
        }
        findPlayer2.showSearch = function(){
            findPlayer2.searchOpen=!findPlayer2.searchOpen;
            $timeout(function() {
                findPlayer2.searchOpenLate = !findPlayer2.searchOpenLate;
            },200)
        }
        findPlayer2.hideSearch = function(){
            findPlayer2.searchOpen=false;
            $timeout(function(){
                findPlayer2.searchOpenLate=false;
            },200)
        }

        findPlayer2.selectSport = function (sport) {
            $rootScope.sportSelected = sport;
        }

        findPlayer2.dropMenu = function(){
            findPlayer2.backdrop = true;
            findPlayer2.menuDropped = true;
        }
        findPlayer2.hideMenu = function(){
            findPlayer2.menuDropped = false;
            $timeout(function(){
                findPlayer2.backdrop = false;
            },500)
        }


        //-------------------------DESIGN CALCULATION START-------------------------//
        var devHeight = $window.innerHeight;
        var devWidth = $window.innerWidth;
        console.log('width:'+devWidth + 'height:' + devHeight);

        findPlayer2.logoDiv = {
            'margin-top': 0.1 * devHeight + 'px',
            'height': 0.2 * devHeight + 'px',
            'margin-bottom': 0.04 * devHeight + 'px'
        };

        findPlayer2.sportScroll = {'width': 0.7 * devWidth + 'px'};
        findPlayer2.placeholder = {
            'position': 'absolute',
            'top': 0.3 * (devHeight - 44 - 49) + 'px',
            'width': 100 + '%'
        };
        findPlayer2.general = {'height': devHeight-88 + 'px'};

        //-------------------------DESIGN CALCULATION END-------------------------//

        findPlayer2.goToPlayerProfile = function () {
            $state.go('sidemenu.tabs.playerProfile');
            // $ionicNavBarDelegate.showBar(false);
        }


    //==================================================================================
    //==============================PLAYER's ACTIONS====================================


    findPlayer2.mySports=[];

    _.each(SportsList.list,function(sport)
    {
        _.each(loggedInUser.info.mySports,function(mySport)
        {
            if(mySport==sport._id)
                findPlayer2.mySports.push(sport);
        })
    })



    //==================================================================================
    //==============================PLAYER's END=++++===================================
    }
