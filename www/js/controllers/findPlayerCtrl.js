angular.module('starter')

    .controller('findPlayerCtrl', findPlayerCtrl)
function findPlayerCtrl($window, $timeout, GetUserStats, userProfileParams, $state, SportsList,Following, FindCoach,findPlayerResult, loggedInUser, $ionicModal, $scope, $rootScope, $ionicNavBarDelegate) {
    var findPlayer = this;
    //Whether search bar is opened or not
    findPlayer.searchOpen = false;
    findPlayer.searchOpenLate = false;
    findPlayer.menuDropped = false;
    findPlayer.backdrop = false;


    $scope.$on('$ionicView.enter', function (e) {
        $ionicNavBarDelegate.showBar(true);

    });


    findPlayer.following = Following.list;
    console.log(findPlayer.following);
    $ionicModal.fromTemplateUrl('templates/coach/findplayermodal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modal) {
        findPlayer.modal = modal;
    });
    findPlayer.showFilters = function () {
        findPlayer.modal.show();
    }
    findPlayer.hideFilters = function () {
        findPlayer.modal.hide();
    }
    findPlayer.showSearch = function () {
        findPlayer.searchOpen = true;
        $timeout(function () {
            findPlayer.searchOpenLate = true;
        }, 400)
    }
    findPlayer.hideSearch = function () {
        findPlayer.searchOpen = false;
        $timeout(function () {
            findPlayer.searchOpenLate = false;
        }, 100)
    }

    findPlayer.dropMenu = function () {
        findPlayer.backdrop = true;
        $timeout(function () {
            findPlayer.menuDropped = true;
        }, 100)
    }
    findPlayer.hideMenu = function () {
        findPlayer.backdrop = false;
        $timeout(function () {
            findPlayer.menuDropped = false;
        }, 500)
    }


    //-------------------------DESIGN CALCULATION START-------------------------//
    var devHeight = $window.innerHeight;
    var devWidth = $window.innerWidth;
    console.log('width:' + devWidth + 'height:' + devHeight);

    findPlayer.logoDiv = {
        'margin-top': 0.1 * devHeight + 'px',
        'height': 0.2 * devHeight + 'px',
        'margin-bottom': 0.04 * devHeight + 'px'
    };

    findPlayer.sportScroll = {'width': 0.7 * devWidth + 'px'};
    findPlayer.placeholder = {
        'position': 'absolute',
        'top': 0.3 * (devHeight - 44 - 49) + 'px',
        'width': 100 + '%'
    };
    findPlayer.general = {'height': devHeight - 88 + 'px'};

    //-------------------------DESIGN CALCULATION END-------------------------//

    // findPlayer.goToPlayerProfile = function () {
    //     if($rootScope.registerAsCoach){
    //         $state.go('coachSidemenu.tabs.playerProfile');
    //     }
    //     else{
    //         $state.go('sidemenu.playerProfile');
    //     }
    //     // $ionicNavBarDelegate.showBar(false);
    // }


    findPlayer.goToPlayerProfile = function (playerInfo) {
        userProfileParams.params = playerInfo;

        var currentlyTrainingPromise = GetUserStats.get({coachId: playerInfo._id}, function (result) {
            playerInfo.currentlyTraining = result.currentlyTraining;
            playerInfo.followerCount = result.followerCount;
            playerInfo.followingCount = result.followingCount;
            playerInfo.tournamentList = result.tournamentList;
        }, function (err) {
            console.log(err);
        })

        currentlyTrainingPromise['$promise'].then(function () {

            if (!loggedInUser.info.player) {
                $state.go('coachSidemenu.tabs.coachPlayerProfile');
            }
            else {

                $state.go('sidemenu.playerPlayerProfile');
            }
        })
    }


    findPlayer.mySports = [];

    _.each(SportsList.list, function (sport) {
        _.each(loggedInUser.info.mySports, function (mySport) {
            if (mySport == sport._id)
                findPlayer.mySports.push(sport);
        })
    })


    findPlayer.playerSearchList = [];


    findPlayer.findSearchResults = function (searchText) {
        FindCoach.query({
            sportId: $rootScope.sportSelected._id,
            player: true,
            searchText: searchText
        }, function (result) {
            console.log(result);
            findPlayer.playerSearchList = result;
        }, function (err) {
            console.log(err);
        })
    }


    findPlayer.clearSearch = function () {
        findPlayer.playerSearchList = [];
    }


    findPlayer.selectSport = function (sport) {
        $rootScope.sportSelected = sport;

        findPlayer.backdrop = false;
        $timeout(function () {
            findPlayer.menuDropped = false;
        }, 500)


        findPlayer.playerList = [];
        if (_.indexOf(_.map(findPlayerResult.players, 'sportId'), $rootScope.sportSelected._id) == -1) {
            console.log('api call');
            FindCoach.query({sportId: $rootScope.sportSelected._id, player: true, searchText: ''}, function (data) {
                _.each(data, function (coach) {
                    if (_.indexOf(findPlayer.following, coach._id) == -1) {
                        coach.follow = false;
                    }
                    else {
                        coach.follow = true;
                    }
                    findPlayer.playerList.push(coach);
                })


                findPlayerResult.players.push({playerList: data, sportId: $rootScope.sportSelected._id});
                console.log(findPlayer.playerList);
            }, function (err) {
                console.log(err);
            })
        }
        else {
            console.log('service');
            var filterResult = _.filter(findPlayerResult.players, function (coach) {
                return coach.sportId == $rootScope.sportSelected._id;
            });

            _.each(filterResult[0].playerList, function (coach) {
                if (_.indexOf(findPlayer.following, coach._id) == -1) {
                    coach.follow = false;
                }
                else {
                    coach.follow = true;
                }
                findPlayer.playerList.push(coach);
            })


            console.log(findPlayer.playerList);

        }
    }

    if ($rootScope.sportSelected)//calling selectSport whenever page is loaded
        findPlayer.selectSport($rootScope.sportSelected);


}
