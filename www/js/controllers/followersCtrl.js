angular.module('starter')

    .controller('followersCtrl',followersCtrl)
function followersCtrl($window,$ionicModal, $state) {
        var followers = this;
        followers.videos = 0;

        //-------------------------DESIGN CALCULATION START-------------------------//
        var devHeight = $window.innerHeight;
        var devWidth = $window.innerWidth;
        
        //-------------------------DESIGN CALCULATION END-------------------------//
        
        followers.return = function () {
            $state.go('sidemenu.myProfile');
        }

        followers.openChat = function () {
            $state.go('sidemenu.chat');
        }

    }
