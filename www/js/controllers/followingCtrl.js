angular.module('starter')

    .controller('followingCtrl',followingCtrl)

function followingCtrl($window,$ionicModal, $state) {
        var following = this;
        following.videos = 0;

        //-------------------------DESIGN CALCULATION START-------------------------//
        var devHeight = $window.innerHeight;
        var devWidth = $window.innerWidth;
        
        //-------------------------DESIGN CALCULATION END-------------------------//
        
        following.return = function () {
            $state.go('sidemenu.myProfile');
        }

        following.openChat = function () {
            $state.go('sidemenu.chat');
        }

        following.goToVideoLib = function () {
            $state.go('sidemenu.tabs.videoLib');
        }
    }
