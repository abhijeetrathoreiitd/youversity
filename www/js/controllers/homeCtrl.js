angular.module('starter')

    .controller('homeCtrl', homeCtrl)
function homeCtrl($window, $scope, $rootScope, myNewsFeed, $ionicNavBarDelegate, NewsFeed,$timeout,SportsList) {
    var home = this;
    $scope.$on('$ionicView.enter', function (e) {
        $ionicNavBarDelegate.showBar(true);
    });

    //-------------------------DESIGN CALCULATION START-------------------------//
    var devHeight = $window.innerHeight;
    home.logoDiv = {
        'margin-top': 0.1 * devHeight + 'px',
        'height': 0.2 * devHeight + 'px',
        'margin-bottom': 0.04 * devHeight + 'px'
    };


    //-------------------------DESIGN CALCULATION END-------------------------//

    // home.goToSignup = function () {
    //     $state.go('signup');
    // }

    var query = {};
    home.loadMoreNewsFeed = true;

    home.newsFeedList = myNewsFeed.newsFeedList;

    function fetchNews() {

        NewsFeed.get(query, function (result) {
            _.each(result.newsFeed, function (feed) {

                var newsFeedSportId=typeof feed.trainingPlanId!='undefined'?feed.trainingPlanId.sport:(typeof feed.videoId!='undefined'?feed.videoId.sport:'sample.png');

                console.log(SportsList.list)
                if(newsFeedSportId=='sample.png')
                    feed.image=newsFeedSportId
                else
                feed.image= _.find(SportsList.list,function(item)
                {
                    console.log(newsFeedSportId);
                    console.log(item._id);
                    return newsFeedSportId==item._id;

                }).image;
                console.log(feed);

                var localNewsFeed = '';
                var message = _.find(newsFeed, function (item) {
                    return item.code == feed.newsFeedCode
                }).newsFeed;

                if (typeof feed.videoId != 'undefined' || feed.videoId != null)
                    localNewsFeed = feed.userId.firstName + ' ' + message + ' ' + feed.videoId.title;

                else if (typeof feed.trainingPlanId != 'undefined' && feed.trainingPlanId != null) {
                    localNewsFeed = feed.userId.firstName + ' ' + message + ' ' + feed.trainingPlanId.trainingName;
                }
                else
                    localNewsFeed = feed.userId.firstName + ' ' + message;


                if (query.fetchcase == 1) {
                    home.newsFeedList.unshift({
                        feed: {
                            feed:localNewsFeed,
                            image:feed.image
                        },
                        createdAt: feed.createdAt, updatedAt: feed.updatedAt
                    });

                    myNewsFeed.newFetchTime = result.newsFeed[0].createdAt;

                }

                if (query.fetchcase == 2)// updating oldFetchTime only if old news are fetched.
                {
                    home.newsFeedList.push({
                        feed: {
                            feed:localNewsFeed,
                            image:feed.image
                        },

                        createdAt: feed.createdAt, updatedAt: feed.updatedAt
                    });
                    myNewsFeed.oldFetchTime = feed.createdAt;
                    myNewsFeed.newFetchTime = result.lastFetchTime;
                }

            });

            $timeout(function()
            {
                home.loadMoreNewsFeed = result.loadMoreNewsFeed;
            },100);

            
            $rootScope.$broadcast('scroll.refreshComplete');
            $rootScope.$broadcast('scroll.infiniteScrollComplete');
        });
    }

    home.getNewNewsFeed = function () {
        query = {
            fetchcase: 1,
            newFetchTime: myNewsFeed.newFetchTime
        }
        fetchNews();
    }


    home.getOldNewsFeed = function () {
        query = {
            fetchcase: 2,
            oldFetchTime: myNewsFeed.oldFetchTime
        }

        fetchNews();
    }


}
