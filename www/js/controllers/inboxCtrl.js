angular.module('starter')

    .controller('inboxCtrl',inboxCtrl)

function inboxCtrl($window,$ionicModal, $state) {
        var inbox = this;
        inbox.videos = 0;



        //-------------------------DESIGN CALCULATION START-------------------------//
        var devHeight = $window.innerHeight;
        var devWidth = $window.innerWidth;
        
        //-------------------------DESIGN CALCULATION END-------------------------//
        
        inbox.return = function () {
            $state.go('sidemenu.tabs.videoLib');
        }

        inbox.openChat = function () {
            $state.go('sidemenu.chat');
        }

    }
