angular.module('starter')

    .controller('loginCtrl', loginCtrl)
function loginCtrl($window, $state, $rootScope, Login, loggedInUser, $localForage,
                   $ionicLoading, loginService, $timeout, UserService, gmailLogin, $q, User) {
    var login = this;
    console.log($state.current.name)
    //-------------------------DESIGN CALCULATION START-------------------------//
    var devHeight = $window.innerHeight;
    login.logoDiv = {
        'margin-top': 0.04 * devHeight + 'px',
        'height': 0.2 * devHeight + 'px',
        'margin-bottom': 0.03 * devHeight + 'px'
    };
    login.formDiv = {'margin-top': 0.01 * devHeight + 'px'};
    login.fullHeight = {'height': devHeight + 'px'};
    login.forgotDiv = {'width': 100 + '%'};
    login.forgotMargin = {'margin-top': 0.01 * devHeight + 'px'};
    login.signup = {'margin-top': 0.01 * devHeight + 'px'};
    login.username = {'margin-bottom': 0.01 * devHeight + 'px'};
    login.socialLoginButton = {'height': 0.1 * devHeight + 'px'};


    //-------------------------DESIGN CALCULATION END-------------------------//

    login.goToSignup = function (param) {
        if (param == 'coach') {
            $rootScope.registerAsCoach = true;
            $state.go('coachSidemenu.signup');
        }
        else {
            $rootScope.registerAsCoach = false;
            $state.go('sidemenu.signup');
        }
    }

    login.user = {};
    login.loginFunction = function () {

        $ionicLoading.show({'template': 'Signing in'});
        Login.save({email: login.user.email, password: login.user.password},
            function (data) {
                loggedInUser.info = data.user;
                loggedInUser.info.mySports = _.map(loggedInUser.info.sportsExperience, 'sportId');
                if (typeof loggedInUser.info.mySports == 'undefined') {
                    loggedInUser.info.mySports = [];
                }
                $localForage.setItem('loggedInUser', data.user)
                    .then(function (data) {
                        loginService.login();
                    });
            },
            function (err) {
                login.invalidPasswordError = true;

                console.log(err);
                if (err.data == 'Unauthorized')

                    $timeout(function () {
                        login.invalidPasswordError = false;
                    }, 2000)
                $ionicLoading.hide();
            })
    }

    // login.goToHome = function () {
    //     $state.go('sidemenu.tabs.videoLib');
    // }


    // This method is to get the user profile info from the facebook api
    var getFacebookProfileInfo = function (authResponse) {
        var info = $q.defer();

        facebookConnectPlugin.api('/me?fields=email,name&access_token=' + authResponse.accessToken, null,
            function (response) {

                info.resolve(response);
            },
            function (response) {
                info.reject(response);
            }
        );
        return info.promise;
    };

    // This is the success callback from the login method
    var fbLoginSuccess = function (response) {
        console.dir(response);
        if (!response.authResponse) {
            fbLoginError("Cannot find the authResponse");
            return;
        }

        var authResponse = response.authResponse;

        getFacebookProfileInfo(authResponse)
            .then(function (profileInfo) {

                console.log('after login')
                console.dir(profileInfo)

                //Check if this is social login email exists, and from which social login
                User.checkType({email:profileInfo.email},function(data){
                    console.log(data);
                    if(!data.socialLogin){
                        // For the purpose of this example I will store user data on local storage
                        UserService.setUser({
                            authResponse: authResponse.authResponse,
                            userID: profileInfo.id,
                            name: profileInfo.name,
                            email: profileInfo.email
                        });
                        var onConfirm = function (buttonIndex) {
                            if(buttonIndex==1){
                                login.player = false;
                                proceedWithLoginType(profileInfo,false);
                            }
                            if(buttonIndex==2){
                                login.player = true;
                                proceedWithLoginType(profileInfo,true);
                            }
                        }
                        navigator.notification.confirm(
                            'Please select your login profile type', // message
                            onConfirm,            // callback to invoke with index of button pressed
                            'Select Profile',           // title
                            ['Coach','Player']     // buttonLabels
                        );
                    }
                    else{
                        proceedWithLoginType(profileInfo,data.player);
                    }
                },
                    function(err){
                        
                    })

            }, function (err) {
                $ionicLoading.hide();

                console.log(5)
            });
    };

    var proceedWithLoginType = function (profileInfo,player) {

        loggedInUser.info = {
            'email': profileInfo.email,
            'userId': profileInfo.id,
            'name': profileInfo.name,
            'mySports': [],
            'player':player
        };
        
        $localForage.setItem('loggedInUser', loggedInUser.info).then(function (data) {

        })

        loggedInUser.info.player = login.player;
        loggedInUser.info.coach = !login.player;

        User.fbSignUp({user: loggedInUser.info,player:player}, function (data) {
            console.dir(data);
            data.userData.mySports = [];
            $localForage.setItem('loggedInUser', data.userData).then(function (forageData) {
                loggedInUser.info = forageData;
                loggedInUser.info.mySports = _.map(forageData.sportsExperience, 'sportId');//contains only list of sports ID
            });
            $rootScope.loggedInUserName = loggedInUser.info.name;
            if (data.firstLogin) {
                if (login.player)
                    $state.go('sidemenu.chooseSport');
                else
                    $state.go('coachSidemenu.chooseSport')
            }
            else {
                loginService.login();
            }
            $ionicLoading.hide();

        }, function (err) {
            console.log(err);
        });
    }

// This is the fail callback from the login method
    var fbLoginError = function (error) {

        $ionicLoading.hide();
    };


//This method is executed when the user press the "Login with facebook" button
    login.facebookSignIn = function () {

        facebookConnectPlugin.getLoginStatus(function (success) {
            if (success.status === 'connected') {

                console.log('already connected');
                // The user is logged in and has authenticated your app, and response.authResponse supplies
                // the user's ID, a valid access token, a signed request, and the time the access token
                // and signed request each expire

                // Check if we have our user saved in database (we should)
                login.user = UserService.getUser('facebook');

                //No, user is not saved
                if (!login.user.userID) {
                    getFacebookProfileInfo(success.authResponse)
                        .then(function (profileInfo) {

                            // Store user data on local storage
                            UserService.setUser({
                                authResponse: success.authResponse,
                                userID: profileInfo.id,
                                name: profileInfo.name,
                                email: profileInfo.email
                            });

                            loggedInUser.info = {
                                'email': profileInfo.email,
                                'userId': profileInfo.id,
                                'name': profileInfo.name,
                                'mySports': []
                            };
                            $localForage.setItem('loggedInUser', loggedInUser.info).then(function (data) {

                            })
                            // login.user = UserService.getUser('facebook');

                            console.log('if')
                            console.dir(login.user)

                            if (login.player)
                                $state.go('sidemenu.chooseSport');
                            else
                                $state.go('coachSidemenu.chooseSport')

                        }, function (fail) {
                            // Fail get profile info
                        });
                }

                // Yes user is saved
                else {
                    console.log(login.user);
                    loggedInUser.info = {
                        'email': login.user.email,
                        'userId': login.user.userID,
                        'name': login.user.name,
                        'mySports': []
                    };
                    $localForage.setItem('loggedInUser', loggedInUser.info).then(function (data) {
                        
                    })
                    console.log('else')

                    console.dir(login.user)
                    if (login.player)
                        $state.go('sidemenu.tabs.training');
                    else
                        $state.go('coachSidemenu.tabs.training')


                }
            }
            else {
                // If (success.status === 'not_authorized') the user is logged in to Facebook,
                // but has not authenticated your app
                // Else the person is not logged into Facebook,
                // so we're not sure if they are logged into this app or not.


                $ionicLoading.show({
                    template: 'Logging in...'
                });

                // Ask the permissions you need. You can learn more about
                // FB permissions here: https://developers.facebook.com/docs/facebook-login/permissions/v2.4
                facebookConnectPlugin.login(['email', 'public_profile'], fbLoginSuccess, fbLoginError);
            }
        });
    };

    login.googleLogin = function () {
        gmailLogin.login();
    }
}
