angular.module('starter')

    .controller('myActivityDetailCtrl', myActivityDetailCtrl);

function myActivityDetailCtrl($stateParams, $scope, $window, $timeout, $sce, $ionicModal, TrainingPlanList, playerTrainingPlanList) {
    var myActivityDetail = this;
    myActivityDetail.activityList = [];
    myActivityDetail.planDetails = $stateParams.planDetails;
    myActivityDetail.modalActivity = null;
    var devHeight = $window.innerHeight;
    var devWidth = $window.innerWidth;

    myActivityDetail.videoFrame = {'width': devWidth + 'px', 'height': 9 * devWidth / 16 + 'px'};

    myActivityDetail.planStatus = $stateParams.status;//just used to hide check box if planStatus is not  'started'
    myActivityDetail.lastIndex = $stateParams.lastIndex;
    myActivityDetail.day = $stateParams.day;
    myActivityDetail.week = $stateParams.week;
    myActivityDetail.activityDescription = $stateParams.activityDescription;
    _.each(myActivityDetail.activityDescription, function (activity) {
        _.each($stateParams.activityList, function (act) {
            console.log($stateParams.activityList.index)

            if (act.activityId == activity._id) {
                myActivityDetail.activityList.push({
                    activityDescription: activity.description,
                    activityName: activity.activityName,
                    status: act.completed,
                    activityId: act.activityId,
                    round: activity.round,
                    unit1: activity.unit1,
                    unit2: activity.unit2,
                    videoId: activity.videoId,
                    index: $stateParams.activityList.index
                });
            }
        })
        console.log(myActivityDetail.activityList)

    })
    myActivityDetail.openActivityDetailModal = function (activity) {
        myActivityDetail.modalActivity = activity;
        $ionicModal.fromTemplateUrl('templates/player/myActivityDetailModal.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            myActivityDetail.modal = modal;
            myActivityDetail.activityInModal = activity;
        }).then(function () {

            myActivityDetail.modal.show();
        }).then(function () {
            if (myActivityDetail.activityInModal.videoId != null && myActivityDetail.activityInModal.videoId.url.includes('youtube.com')) {
                myActivityDetail.isYoutubeVideo = true;
                myActivityDetail.trustedLink = $sce.trustAsResourceUrl(myActivityDetail.activityInModal.videoId.url + '?showinfo=0');
            }
            else {
                myActivityDetail.isYoutubeVideo = false;
            }


            if (myActivityDetail.activityInModal.videoId != null && !myActivityDetail.isYoutubeVideo) {
                $timeout(function () {
                    var player = jwplayer("videoPlayerdiv").setup({
                        "file": myActivityDetail.activityInModal.videoId.url,
                        "image": "img/icon.png",
                        "height": 360 * devWidth / 640,
                        "width": devWidth
                    });

                    jwplayer().on('play', function (e) {
                        //call increase view api here
                    });
                    if (document.cookie.indexOf("jwplayerAutoStart") == -1) {
                        document.cookie = "jwplayerAutoStart=1";
                        player.on('ready', function () {
                            player.play();
                        });
                    }
                    else {
                    }
                }, 200)
            }

        })

    }


    myActivityDetail.changeActivityStatusOnConfirm = function (activity) {
        activity.status = !activity.status;
        _.each(playerTrainingPlanList.list, function (plan) {
            if (plan.trainingPlanId._id == myActivityDetail.planDetails._id) {
                _.each(plan.activity, function (act) {
                    if (act.activityId == activity.activityId) {
                        act.completed = activity.status;
                        TrainingPlanList.updateTrainingPlanList({
                            myPlanId: plan._id,
                            activityList: plan.activity
                        }, function (data) {
                        }, function (err) {
                            console.log(err);
                        })
                    }
                })

                var percentageCompleted = (_.filter(plan.activity, function (act) {
                    return act.completed == true;
                }).length * 100 / plan.activity.length)
                plan.percentageCompleted = percentageCompleted;
            }
        })
    }

    myActivityDetail.changeActivityStatus = function (activity) {

        if (!activity.status) {
            if (window.cordova) {
                var alertDismissed = function (buttonIndex) {
                    if (buttonIndex == 1) {
                        myActivityDetail.changeActivityStatusOnConfirm(activity);
                    }
                };

                navigator.notification.confirm(
                    'Are you sure you want this activity as complete?',  // message
                    alertDismissed,         // callback
                    'Activity Update',            // title
                    ['OK', 'Cancel']     // buttonName
                );
            }
            else {
                if (window.confirm('Are you sure you want this activity as complete?')) {
                    myActivityDetail.changeActivityStatusOnConfirm(activity);
                }
                else {
                    // They clicked no
                }
            }
        }
        else {
            if (window.cordova) {
                $cordovaToast.showShortBottom('Activity already completed');
            }
            else {
                alert('activity already done');
            }
        }


    }


    myActivityDetail.hideFilters = function () {

        myActivityDetail.modal.hide();
    }


}
