angular.module('starter')

    .controller('myProfileCtrl', myProfileCtrl)
function myProfileCtrl($window, $ionicModal, myStats, mySports, SportsList, 
                       loggedInUser, GetUserStats, NewsFeed, $state, $scope, $ionicPopup, 
                       IonicClosePopupService, $timeout, $ionicScrollDelegate, $rootScope ) {
    var myProfile = this;
    myProfile.videos = 0;
    myProfile.status = 1;
    myProfile.prevStatus = 1;
    myProfile.menuDropped = false;
    myProfile.backdrop = false;
    myProfile.showDivision = 0;
    myProfile.myInfo = loggedInUser.info;
    myProfile.myStats = myStats.list;

    myProfile.mySports = mySports.sports;
    console.log(_.isEmpty(myStats.list))
    if (_.isEmpty(myStats.list)) {
        console.log(myProfile.myInfo._id);
        GetUserStats.get({coachId: myProfile.myInfo._id}, function (result) {
            console.log(result);
            myProfile.myStats.currentlyTraining = result.currentlyTraining;
            myProfile.myStats.followerCount = result.followerCount;
            myProfile.myStats.followingCount = result.followingCount;
            myProfile.myStats.tournamentList = result.tournamentList;
            myProfile.myStats.academyList = result.academyList;

        }, function (err) {
            console.log(err);
        })
    }

    myProfile.selectSport = function (sport) {
        myProfile.showSpinner = true;
        $rootScope.sportSelected = sport;
        myProfile.backdrop = false;
        $timeout(function () {
            myProfile.menuDropped = false;
        }, 500)
    }

    myProfile.goToSports = function () {

        if ((loggedInUser.info.player))
            $state.go('sidemenu.chooseSport',{'showButton':true});
        else
            $state.go('coachSidemenu.chooseSport',{'showButton':true})
    }

    myProfile.uploadVideo = function () {
        $rootScope.selectVideo();
    }

    
    myProfile.chooseCoverImage = function () {
        $rootScope.selectImage('Cover Pic');
    }
    myProfile.chooseProfileImage = function () {
        $rootScope.selectImage('Profile Pic');
    }

    myProfile.dropMenu = function () {
        myProfile.backdrop = true;
        $timeout(function () {
            myProfile.menuDropped = true;
        }, 100)    }
    myProfile.hideMenu = function () {
        myProfile.backdrop = false;
        $timeout(function () {
            myProfile.menuDropped = false;
        }, 500)
    }

    //-------------------------DESIGN CALCULATION START-------------------------//
    var devHeight = $window.innerHeight;
    var devWidth = $window.innerWidth;

    $timeout(function () {
        var tabHeight1 = document.querySelector('#info').offsetHeight;
        var tabHeight2 = document.querySelector('#exp').offsetHeight;
        var minHeight = Math.max(devHeight - 200 - 88, tabHeight1, tabHeight2)
        console.log(minHeight);

        myProfile.info = {
            'min-height': Math.max(minHeight, tabHeight1, tabHeight2) + 'px'
        }
        myProfile.exp = {
            'min-height': Math.max(minHeight, tabHeight1, tabHeight2) + 'px'
        }
        myProfile.videos = {
            'min-height': Math.max(minHeight, tabHeight1, tabHeight2) + 'px'
        }
    }, 200);

    myProfile.logoDiv = {
        'margin-top': 0.1 * devHeight + 'px',
        'height': 0.2 * devHeight + 'px',
        'margin-bottom': 0.04 * devHeight + 'px'
    };
    myProfile.scrollerHeight = {
        'height': devHeight - 200 - 88 + 'px'
    };
    myProfile.general = {'height': devHeight - 88 + 'px'};

    myProfile.changePrevStatus = function (number) {
        $ionicScrollDelegate.scrollTop();
        myProfile.showDivision = 0;
        console.log(myProfile.showDivision);
        myProfile.status = number;
        $timeout(function () {
            myProfile.showDivision = myProfile.status;
            myProfile.prevStatus = number;
            console.log(myProfile.showDivision);
        }, 200);
    }

    myProfile.addTournament = function () {
        var options =
        {
            title: 'Add tournament',
            subtitle: 'Fill in the details',
            cssClass: 'sportsPickPopup', // String, The custom CSS class name
            template: '<div enter-tournament></div>'
        }
        tournamentAlertPopup = $ionicPopup.alert(options);

        IonicClosePopupService.register(tournamentAlertPopup);

    }

    myProfile.addAcademy = function () {
        var options =
        {
            title: 'Add Academy',
            cssClass: 'sportsPickPopup', // String, The custom CSS class name
            template: '<div add-academy></div>'
        }
        academyAlertPopup = $ionicPopup.alert(options);

        IonicClosePopupService.register(academyAlertPopup);

    }
    //-------------------------DESIGN CALCULATION END-------------------------//

    myProfile.return = function () {
        $state.go('sidemenu.tabs.myProfile');
    }

    myProfile.goToFollowers = function () {
        $state.go('sidemenu.followers');
    }
    myProfile.goToFollowing = function () {
        $state.go('sidemenu.following');
    }


    myProfile.newsFeedList = [];
    var oldFetchTime = ''
    myProfile.loadMoreNewsFeed = true;

    function fetchNews() {
        console.log(query);
        NewsFeed.get(query, function (result) {
            console.log(result)
            _.each(result.newsFeed, function (feed) {

                var newsFeedSportId = typeof feed.trainingPlanId != 'undefined' ? feed.trainingPlanId.sport : (typeof feed.videoId != 'undefined' ? feed.videoId.sport : 'sample.png');

                if(newsFeedSportId=='sample.png')
                    feed.image=newsFeedSportId
                else {
                    feed.image = _.find(SportsList.list, function (item) {
                        return newsFeedSportId == item._id;

                    }).image;
                }
                console.log(feed.image);

                var localNewsFeed = '';
                var message = _.find(newsFeed, function (item) {
                    return item.code == feed.newsFeedCode
                }).newsFeed;

                if (typeof feed.videoId != 'undefined' || feed.videoId != null)
                    localNewsFeed = feed.userId.firstName + ' ' + message + ' ' + feed.videoId.title;

                else if (typeof feed.trainingPlanId != 'undefined' && feed.trainingPlanId != null) {
                    localNewsFeed = feed.userId.firstName + ' ' + message + ' ' + feed.trainingPlanId.trainingName;
                }
                else
                    localNewsFeed = feed.userId.firstName + ' ' + message;


                myProfile.newsFeedList.push({
                    feed: {
                        feed: localNewsFeed,
                        image: feed.image
                    },
                    createdAt: feed.createdAt, updatedAt: feed.updatedAt
                });

                oldFetchTime = feed.createdAt;
                $timeout(function () {
                    myProfile.loadMoreNewsFeed = result.loadMoreNewsFeed;
                }, 100);

                $rootScope.$broadcast('scroll.infiniteScrollComplete');

            });


        });


    }

    myProfile.getOldNewsFeed = function () {
        query = {
            userId: loggedInUser.info._id,
            fetchcase: 3,
            oldFetchTime: oldFetchTime
        }
        fetchNews();
    }

}
