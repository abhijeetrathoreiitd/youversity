angular.module('starter')

    .controller('myTrainingDetailCtrl', myTrainingDetailCtrl)
function myTrainingDetailCtrl($window, myTrainingParams, currentWeekParams, ActivityListByTrainingPlan, TrainingPlanList, loggedInUser,
                              $ionicLoading, playerTrainingPlanList, Activity, $cordovaCalendar, calenderName, $rootScope, $state, $timeout, $q, $scope, $ionicScrollDelegate, $ionicModal) {
    var myTraining = this;
    myTraining.videos = 0;
    myTraining.showActivities = true;
    myTraining.showDetails = true;
    var trainingPlanId = myTrainingParams.params.trainingPlanId._id;
    console.log(myTrainingParams.params);
    myTraining.trainingDetails = {
        trainingName: myTrainingParams.params.trainingPlanId.trainingName,
        coachName: myTrainingParams.params.trainingPlanId.trainingBy.firstName,
        duration: myTrainingParams.params.trainingPlanId.duration,
        level: myTrainingParams.params.trainingPlanId.level,
        description: myTrainingParams.params.trainingPlanId.description,
        status: myTrainingParams.params.status,
        noOfSubscriber: myTrainingParams.params.trainingPlanId.noOfSubscriber,
        planId: myTrainingParams.params._id
    }


    switch (myTraining.trainingDetails.status) {
        case 'notStarted':
            myTraining.trainingDetails.statusText = 'start';
            break;

        case 'started':
            myTraining.trainingDetails.statusText = 'pause';
            break;

        case 'paused':
            myTraining.trainingDetails.statusText = 'start';
            break;

    }
    var calenderActivityList = [];

    myTraining.activityList = [];//array of array
    if (!_.find(ActivityListByTrainingPlan.list, function (list) {
            return list.trainingPlanId == trainingPlanId;
        })
    ) {
        $ionicLoading.show({"template": "Loading"});
        Activity.query({trainingPlanId: trainingPlanId}, function (data) {
            ActivityListByTrainingPlan.list.push({'trainingPlanId': trainingPlanId, activities: data})
            console.log(ActivityListByTrainingPlan.list);
            calenderActivityList = _.filter(ActivityListByTrainingPlan.list, function (list) {
                return list.trainingPlanId = trainingPlanId
            });

            $ionicLoading.hide();
        }, function (err) {
            $ionicLoading.hide();
            console.log(err);
        })
    }

    //-------------------------DESIGN CALCULATION START-------------------------//
    var devHeight = $window.innerHeight;
    var devWidth = $window.innerWidth;
    myTraining.fullDim = {'width': devWidth + 'px', 'height': devHeight + 'px'};
    myTraining.confirmation = {'height': 0.5 * devHeight + 'px'};
    var height2, height3;

    $timeout(function () {
        height2 = document.querySelector('#coachInfo').offsetHeight;
        height3 = document.querySelector('#description').offsetHeight;
        myTraining.activityScroller = {'height': devHeight - 44 - 44 - 44 - 44 - height2 - height3 + 'px'}
        myTraining.myactivityScroller = {'height': devHeight - 44 - 44 - height2 - height3 + 'px'}
        myTraining.activities = {'height': document.querySelector('#activityList').offsetHeight + 'px'}

    }, 300);

    //-------------------------DESIGN CALCULATION END-------------------------//

    myTraining.return = function () {
        $rootScope.showTabs = true;
        $state.go('sidemenu.tabs.training',{openTab:2});
    }

    myTraining.week = currentWeekParams.week;
    myTraining.selectWeek = function (week) {

        myTraining.week = week;
        currentWeekParams.week = week;
    }

    myTraining.activityList = [];


    function createActivityList() {
        var activityArray = [];
        var deferred = $q.defer();
        var allActivity;
        var dailyCompleted;
        myTraining.lastIndexOfCompletedActivity = -1;
        var index = 0;
        for (var i = 1; i <= myTrainingParams.params.trainingPlanId.duration; i++) {
            var localGroup = _.groupBy(_.filter(myTrainingParams.params.activity, function (activity) {
                return activity.day > 7 * (i - 1) && activity.day <= 7 * i;
            }), 'day');
            _.each(localGroup, function (grp) {
                grp.completed = _.filter(grp, function (act) {
                    return act.completed == true;
                }).length;
                grp.allActivity = grp.length;
                if (grp.completed == grp.length && grp.completed != 0) {
                    grp.allActivityDone = true;
                    myTraining.lastIndexOfCompletedActivity++;
                }
                else {
                    grp.allActivityDone = false;
                }
                grp.index = index++;
            });

            console.log(myTraining.lastIndexOfCompletedActivity);
            activityArray.push(localGroup);
            if (i == myTrainingParams.params.trainingPlanId.duration) {
                console.log(activityArray)
                deferred.resolve(activityArray);
            }
        }
        return deferred.promise;
    }


    var createActivityPromise = createActivityList();
    createActivityPromise.then(function (data) {
        myTraining.activityList = data;
    })


    myTraining.goToMyActivityDetails = function (activityList, index) {
        console.log(activityList)
        $state.go('sidemenu.tabs.myActivityDetails', {
            activityList: activityList,
            planDetails: myTrainingParams.params.trainingPlanId,
            activityDescription: myTraining.activityInfoList[myTraining.week * 7 + index].activities,
            day: myTraining.week * 7 + index + 1,
            week: myTraining.week + 1,
            status: myTraining.trainingDetails.status,
            lastIndex: myTraining.lastIndexOfCompletedActivity
        });
    }


    myTraining.activityInfoList = [];//contains completed Information of the activity.
    Activity.query({trainingPlanId: myTrainingParams.params.trainingPlanId._id}, function (data) {
        data = _.sortBy(data, [function (o) {
            return o.day;
        }]);
        myTraining.activityInfoList = data;

        $ionicLoading.hide();
    }, function (err) {
    }, function (err) {
        $ionicLoading.hide();
        console.log(err);
    })

    myTraining.toggleDetails = function () {
        myTraining.showDetails = !myTraining.showDetails;
        if (!myTraining.showDetails) {
            myTraining.activityScroller = {'height': devHeight - 44 - 44 - 44 - 44 + 'px'}
        }
        else {
            myTraining.activityScroller = {'height': devHeight - 44 - 44 - 44 - 44 - height2 - height3 + 'px'};
        }
        $ionicScrollDelegate.$getByHandle('activity').scrollTop(true);

    }
    myTraining.toggleActivities = function () {
        myTraining.showActivities = !myTraining.showActivities;
    }


    myTraining.statusUpdateOnConfirm = function (plan, status, updateStartTime) {
        if (status == 'ended') {
            console.log(plan)
            myTraining.planInModal = plan;
            myTraining.showRatingModal();
        }
        plan.status = status;
        console.log(status)
        //updateStartTime-->Set the start time of the plan. It is set only once.

        switch (plan.status) {
            case 'notStarted':
                plan.statusText = 'start';
                break;

            case 'started':
                plan.statusText = 'pause';
                break;

            case 'paused':
                plan.statusText = 'start';
                break;

        }

        TrainingPlanList.myTrainingPlanUpdateStatus({
            myPlanId: plan.planId,
            status: status,
            planId:trainingPlanId,
            updateStartTime: updateStartTime,
            userId: loggedInUser.info._id
        }, function (result) {
            myTraining.trainingDetails.status = plan.status;
            myTraining.trainingDetails.statusText = plan.statusText;
            _.each(playerTrainingPlanList.list, function (planInList) {
                if (planInList._id == plan.planId) {
                    planInList.status = plan.status;
                    planInList.statusText = plan.statusText;
                }
            })
        }, function (err) {
            console.log(err);
        })
    }


    myTraining.statusUpdate = function (plan, status, updateStartTime, $event) {

        console.log(plan)
        var localText;
        switch (status) {
            case 'notStarted':
                localText = 'start';
                break;
            case 'started':
                localText = 'start';
                break;
            case 'paused':
                localText = 'pause';
                break;
            case 'ended':
                localText = 'end';
                break;

        }

        if (window.cordova) { 
            var alertDismissed = function (buttonIndex) {
                if (buttonIndex == 1) {
                    if (plan.status == 'notStarted') {
                        var someDate = new Date();
                        var calenderDate = new Date();
                        var numberOfDaysToAdd = 0;
                        _.each(calenderActivityList[0].activities, function (activities) {

                            var startTime = 9;
                            var endTime = 9;
                            _.each(activities.activities, function (act) {
                                calenderDate.setDate(someDate.getDate() + numberOfDaysToAdd);
                                console.log(calenderDate);
                                console.log(act)
                                $cordovaCalendar.createEventInNamedCalendar({
                                    title: act.activityName,
                                    notes: act.description,
                                    startDate: calenderDate,
                                    endDate: calenderDate,
                                    calendarName: calenderName
                                }).then(function (result) {
                                    // success
                                }, function (err) {
                                    // error
                                });

                            })
                            console.log(numberOfDaysToAdd)
                            numberOfDaysToAdd++;


                        })
                    }
                    
                    myTraining.statusUpdateOnConfirm(plan, status, updateStartTime, $event);
                }
            };

            navigator.notification.confirm(
                'Are you sure you want to ' + localText + ' the plan?',  // message
                alertDismissed,         // callback
                'Status Update',            // title
                ['OK', 'Cancel']     // buttonName
            );
        }
        else {
            if (window.confirm('Are you sure you want to ' + localText + ' the plan?')) {
                myTraining.statusUpdateOnConfirm(plan, status, updateStartTime, $event);
            }
            else {
                // They clicked no
            }
        }
    }


    $ionicModal.fromTemplateUrl('templates/player/ratingTrainingPlan.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modal) {
        myTraining.ratingModal = modal;
    });
    myTraining.showRatingModal = function () {

        myTraining.ratingModal.show();
    }
    myTraining.hideRatingModal = function () {
        console.log('hide rating modal');
        myTraining.ratingModal.hide();
    };
    myTraining.rateSP = function (rate) {
        myTraining.planInModal.rating = rate;
        myTraining.rate1 = false;
        myTraining.rate2 = false;
        myTraining.rate3 = false;
        myTraining.rate4 = false;
        myTraining.rate5 = false;
        switch (rate) {
            case 5:
                myTraining.rate5 = true;
            case 4:

                myTraining.rate4 = true;
            case 3:

                myTraining.rate3 = true;
            case 2:

                myTraining.rate2 = true;
            case 1:
                myTraining.rate1 = true;
        }
    }

    myTraining.submitRating = function () {
        console.log(myTraining.planInModal);

        TrainingPlanList.rateMyTrainingPlan({
            myPlanId: myTraining.planInModal.planId,
            rating: myTraining.planInModal.rating,
            userId: loggedInUser.info._id,
            trainingPlanId: trainingPlanId,
            feedback: myTraining.planInModal.feedback
        }, function (data) {
            console.log(data);
        }, function (err) {
            console.log(err);
        })


        myTraining.hideRatingModal();
    }


}
