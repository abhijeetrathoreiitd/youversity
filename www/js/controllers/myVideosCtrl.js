angular.module('starter')

    .controller('myVideosCtrl',myVideosCtrl)
function myVideosCtrl($window, $state, $timeout, $ionicPopup, $ionicHistory,
                                          $ionicSideMenuDelegate, $rootScope, $ionicModal, $scope, 
                                          $ionicNavBarDelegate, IonicClosePopupService) {
        var myVideos = this;
        myVideos.videos = 0;

        //Whether any video is subscribed or not
        myVideos.subscribed = true;
        //whether any video is uploaded or not
        myVideos.uploaded = true;
        //which sub-category is seleceted
        myVideos.status = 'start';
        //button to upload more videos
        myVideos.showUploadButton = false;


        $scope.$on('$ionicView.enter', function (e) {
            $ionicNavBarDelegate.showBar(true);
        });

        myVideos.showUpload = function () {
            if (myVideos.uploaded) {
                $timeout(function () {
                    myVideos.showUploadButton = true;
                }, 200)
            }
        }
        myVideos.hideUpload = function () {
            myVideos.showUploadButton = false;
        }


        myVideos.playVideo = function () {
            $rootScope.showTabs = false;
            $state.go('sidemenu.tabs.playVideo');
        }
        // myVideos.selectSport = function(){
        //     myVideos.sportSelected = true;
        // }
        myVideos.goBack = function () {
            $state.go('sidemenu.tabs.videoLib');
        };


        $ionicModal.fromTemplateUrl('templates/myvideosfiltermodal.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            myVideos.videofiltermodal = modal;
            console.log('my videos filter modal');
        });
        myVideos.showFilters = function () {
            myVideos.videofiltermodal.show();
            console.log('show my videos filter modal');
        }
        myVideos.hideFilters = function () {
            myVideos.videofiltermodal.hide();
        }


        

        myVideos.uploadVideo = function () {
            $rootScope.selectVideo();
        }

        //-------------------------DESIGN CALCULATION START-------------------------//
        var devHeight = $window.innerHeight;
        var devWidth = $window.innerWidth;

        myVideos.logoDiv = {
            'margin-top': 0.1 * devHeight + 'px',
            'height': 0.2 * devHeight + 'px',
            'margin-bottom': 0.04 * devHeight + 'px'
        };
        myVideos.datum = {
            'left': devWidth + 'px'
        };
        myVideos.sportScroll = {
            'width': 0.7 * devWidth + 'px'
        };
        myVideos.placeholder = {
            'position': 'absolute',
            'top': 0.4 * (devHeight - 44 - 67) + 'px',
            'width': 100 + '%'
        };
        myVideos.fullHeight = {
            'height': devHeight - 88 - 49 + 'px'
        };
        //-------------------------DESIGN CALCULATION END-------------------------//


    }
