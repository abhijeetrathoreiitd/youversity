angular.module('starter')

    .controller('playReviewVideoCtrl',playReviewVideoCtrl)
function playReviewVideoCtrl($window,$sce, $state,$timeout,uploadAudioReviewService,$cordovaToast,$stateParams,$rootScope,Review,loggedInUser,videoForReview) {

        var playReviewVideo = this;

        //-------------------------DESIGN CALCULATION START-------------------------//
        var devHeight = $window.innerHeight;
        var devWidth = $window.innerWidth;
        // playReviewVideo.scrollHeight='';
        playReviewVideo.scrollTop = true;
        playReviewVideo.scrollHeight = {'height': devHeight - (9 * devWidth/16)+ 'px'};
        playReviewVideo.videoFrame = {'width':devWidth+'px','height': 9 * devWidth/16+ 'px'};
        playReviewVideo.player = {'min-height': 360 * devWidth / 640+'px'};
        //-------------------------DESIGN CALCULATION END-------------------------//


        playReviewVideo.return = function () {
            $rootScope.showTabs = true;
            $state.go('coachSidemenu.tabs.videoLib');
        }



    playReviewVideo.playThisVideo=$stateParams.playThisVideo;
    playReviewVideo.review=$stateParams.playThisVideo.review.review;
    playReviewVideo.disabledReviewBox=false;

    _.each(playReviewVideo.review,function(rev)
    {
       if(rev.coachId==loggedInUser.info._id && rev.review)
       {
           playReviewVideo.disabledReviewBox = true;
           playReviewVideo.reviewText=rev.review;
       }

    });



    if (playReviewVideo.playThisVideo.url.includes('youtube.com')) {
        playReviewVideo.isYoutubeVideo = true;
        playReviewVideo.trustedLink = $sce.trustAsResourceUrl(playReviewVideo.playThisVideo.url + '?showinfo=0');
    }
    else {
        playReviewVideo.isYoutubeVideo = false;
    }

    if (!playReviewVideo.isYoutubeVideo) {
        $timeout(function () {
            var player = jwplayer("videoPlayerdiv").setup({
                "file": playReviewVideo.playThisVideo.url,
                "image": "img/icon.png",
                "height": 360 * devWidth / 640,
                "width": devWidth
            });
            //
            //jwplayer().on('play', function (e) {
            //    //call increase view api here
            //    playReviewVideo.viewThisVideo();
            //});
            if (document.cookie.indexOf("jwplayerAutoStart") == -1) {
                document.cookie = "jwplayerAutoStart=1";
                player.on('ready', function () {
                    player.play();
                });
            }
            else {
            }
        }, 200);
    }


    playReviewVideo.sendReview=function()
    {

        Review.sendReview({review:playReviewVideo.reviewText,coachId:loggedInUser.info._id,
            videoId:playReviewVideo.playThisVideo._id,videoBy:playReviewVideo.playThisVideo.videoBy._id},function(data)
        {
            var index=_.indexOf(_.map(videoForReview.videoList,'_id'),playReviewVideo.playThisVideo._id);
            videoForReview.videoList[index].reviewed=true;
            var reviewIndex=_.indexOf(_.map(videoForReview.videoList[index].review.review,'coachId'),loggedInUser.info._id);
            videoForReview.videoList[index].review.review[reviewIndex].review=playReviewVideo.reviewText;
            playReviewVideo.disabledReviewBox = true;
            if(window.cordova) {
                $cordovaToast.showShortBottom('Thanks for providing your feedback');
            }
            else{
                alert('Thanks for providing your feedback');
            }
        },function(err)
        {
            console.log(err);
        })
    }

    playReviewVideo.feedbackInstructions = function(){
        function understood(buttonIndex) {
            if (buttonIndex == 1) {
                
            }
        }
        
        navigator.notification.alert(
            'You need to cover following points in your feedback\n - Review the video\n - Review the skills\n - Review something else', // message
            understood,            // callback to invoke with index of button pressed
            'Feedback Instructions',           // title
            'Ok'     // buttonLabels
        );
    }


    var captureError = function(e) {
        console.log('captureError' ,e);
    }

    var captureSuccess = function(e) {
        playReviewVideo.sound={};
        console.log('captureSuccess');console.dir(e);
        playReviewVideo.sound.file = e[0].localURL;
        playReviewVideo.sound.filePath = e[0].fullPath;

        var videoReviewObject={coachId:loggedInUser.info._id,
            videoId:playReviewVideo.playThisVideo._id,videoBy:playReviewVideo.playThisVideo.videoBy._id}
        var test = uploadAudioReviewService.uploadAudio(playReviewVideo.sound.filePath,videoReviewObject);
        test.then(function (data) { console.log(data)})
    }

    playReviewVideo.record = function() {
        navigator.device.capture.captureAudio(
            captureSuccess,captureError,{duration:60});
    }



}
