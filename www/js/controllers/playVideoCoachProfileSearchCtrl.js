angular.module('starter')

    .controller('playVideoCoachProfileSearchCtrl', playVideoCoachProfileSearchCtrl)
function playVideoCoachProfileSearchCtrl($window, $stateParams, viewVideo, dislikeVideo, playerVideoStats,
                       $state, likeVideo, $timeout, Subscribe, $rootScope, loggedInUser,
                       $filter, playerUploadedVideos, fetchedVideoList, Comment,
                       Subscribed, myBookmarkedVideo, Bookmark, subscribedVideoPackage, $sce) {
    var playVideo = this;
    var activeTab = $stateParams.activeTab;
    playVideo.showListenButton = true;
    playVideo.videoInfo = $stateParams.videoInfo;
    if (typeof playVideo.videoInfo.comment != 'undefined' && playVideo.videoInfo.comment.length != 0)
        playVideo.commentsList = playVideo.videoInfo.comment.comment;
    else
        playVideo.commentsList = [];

// Record audio
    playVideo.play = function (audioUrl) {
        console.log('play called');
        console.log(audioUrl);
    }


    if (typeof playVideo.videoInfo.review != 'undefined') {
        playVideo.reviewList = playVideo.videoInfo.review.review;
        _.each(playVideo.reviewList, function (review) {
            review.audioUrl = $sce.trustAsResourceUrl(review.audioUrl);
        })

        if (_.map(playVideo.reviewList, 'review').length > 0)
            playVideo.showReviewList = true;
        else
            playVideo.showReviewList = false;
    }
    playVideo.likes = playVideo.videoInfo.likes;
    playVideo.dislikes = playVideo.videoInfo.dislikes;

    playVideo.thisUserId = loggedInUser.info._id;
    playVideo.isSubscribedAsSingle = false;
    playVideo.isSubscribedAsPackage = false;

    //-------------------------DESIGN CALCULATION START-------------------------//
    var devHeight = $window.innerHeight;
    var devWidth = $window.innerWidth;
    // playVideo.scrollHeight='';
    playVideo.scrollTop = true;
    playVideo.scrollHeight = {'height': devHeight - 9 * devWidth / 16 + 'px'};
    playVideo.videoFrame = {'width': devWidth + 'px', 'height': 9 * devWidth / 16 + 'px'};
    playVideo.player = {'min-height': 360 * devWidth / 640 + 'px'};
    //-------------------------DESIGN CALCULATION END-------------------------//

    if (playVideo.videoInfo.url.includes('youtube.com')) {
        playVideo.isYoutubeVideo = true;
        playVideo.trustedLink = $sce.trustAsResourceUrl(playVideo.videoInfo.url + '?showinfo=0');
    }
    else {
        playVideo.isYoutubeVideo = false;
    }
    playVideo.goToSignup = function () {
        $state.go('signup');
    }

    playVideo.goToHome = function () {
        $state.go('sidemenu.tabs.home');
    }

    playVideo.isSubscribed = false;
    if (_.indexOf(myBookmarkedVideo.bookmarkedVideos, playVideo.videoInfo._id) != -1) {
        playVideo.isSubscribed = true;
        playVideo.isSubscribedAsSingle = true;
    }
    _.each(subscribedVideoPackage.video, function (n) {
        _.each(n.package, function (video) {
            if (playVideo.videoInfo._id == video._id) {
                playVideo.isSubscribed = true;
                playVideo.isSubscribedAsPackage = true;
            }
        })
    })

    playVideo.return = function () {
        if (!loggedInUser.info.player) {
            $state.go('coachSidemenu.coachCoachProfile');
        }
        else {
            $state.go('sidemenu.tabs.playerCoachProfile');
        }
    }


    playVideo.viewThisVideo = function (videoId) {
        viewVideo.get({
                userId: loggedInUser.info._id,
                videoId: videoId,
                view: _.indexOf(playerVideoStats.viewedVideos, videoId) == -1 ? true : false
            },
            function (viewedVideos) {
                _.each(playerUploadedVideos.videosList, function (video) {
                    if (video._id == videoId) {
                        video.numberOfViews = video.numberOfViews + 1;
                        playerVideoStats.viewedVideos.push(video._id);
                    }
                })
            }, function (err) {
                console.log(err);
            })
    }

    if (!playVideo.isYoutubeVideo) {
        $timeout(function () {
            var player = jwplayer("videoPlayerdiv").setup({
                "file": playVideo.videoInfo.url,
                "image": "img/icon.png",
                "height": 360 * devWidth / 640,
                "width": devWidth
            });

            jwplayer().on('play', function (e) {
                //call increase view api here
                playVideo.viewThisVideo(playVideo.videoInfo._id);
            });
            if (document.cookie.indexOf("jwplayerAutoStart") == -1) {
                document.cookie = "jwplayerAutoStart=1";
                player.on('ready', function () {
                    player.play();
                });
            }
            else {
            }
        }, 200)
    }

    playVideo.likeHighlight = $filter('likeVideoFilter')(playVideo.videoInfo._id);

    playVideo.dislikeHighlight = $filter('dislikeVideoFilter')(playVideo.videoInfo._id);


    playVideo.dislikeThisVideo = function (videoId, disliked) {
        var initiallyLiked = false;


        // if initially video is disliked and player now likes that videos, then the _id of the videos is removed from
        // dislikedVideos and is added into likedVideos. and dislike variable is set to true. It is used to update the like and
        // dislike count in DB
        if (disliked) {
            playerVideoStats.dislikedVideos.splice(_.indexOf(playerVideoStats.dislikedVideos, videoId), 1);
            playVideo.dislikeHighlight = false;
            playVideo.dislikes = playVideo.dislikes - 1;
        }
        else {
            playerVideoStats.dislikedVideos.push(videoId);
            playVideo.dislikeHighlight = true;
            playVideo.dislikes = playVideo.dislikes + 1;

            if (_.indexOf(playerVideoStats.likedVideos, videoId) != -1) {
                initiallyLiked = true;
                playVideo.likeHighlight = false;
                playerVideoStats.likedVideos.splice(_.indexOf(playerVideoStats.likedVideos, videoId), 1);
                playVideo.likes = playVideo.likes - 1;
            }
        }


        dislikeVideo.get({
            userId: loggedInUser.info._id,
            videoId: videoId,
            dislike: playVideo.dislikeHighlight,
            like: initiallyLiked
        }, function (data) {

            if (activeTab == 3) {
                _.each(playerUploadedVideos.videosList, function (video) {
                    if (video._id == videoId) {
                        if (disliked) {
                            video.dislikes = video.dislikes - 1;
                        }
                        else {
                            video.dislikes = video.dislikes + 1;
                        }

                        if (initiallyLiked) {
                            video.likes = video.likes - 1;
                        }
                    }
                })
            }
            else {
                _.each(fetchedVideoList.video, function (video) {
                    if (video._id == videoId) {
                        if (disliked) {
                            video.dislikes = video.dislikes - 1;
                        }
                        else {
                            video.dislikes = video.dislikes + 1;
                        }

                        if (initiallyLiked) {
                            video.likes = video.likes - 1;
                        }
                    }
                })
            }

            // playVideo.dislikeHighlight = $filter('dislikeVideoFilter')(playVideo.videoInfo._id);
            //
            // playVideo.likeHighlight = $filter('likeVideoFilter')(playVideo.videoInfo._id);


        }, function (err) {
            console.log(err)
        })

    }


    playVideo.likeThisVideo = function (videoId, liked) {
        var initiallyDisliked = false;

        // if initially video is disliked and player now likes that videos, then the _id of the videos is removed from
        // dislikedVideos and is added into likedVideos. and dislike variable is set to true. It is used to update the like and
        // dislike count in DB
        if (liked) {
            playerVideoStats.likedVideos.splice(_.indexOf(playerVideoStats.likedVideos, videoId), 1);
            playVideo.likeHighlight = false;
            playVideo.likes = playVideo.likes - 1;
        }
        else {
            playerVideoStats.likedVideos.push(videoId);
            playVideo.likeHighlight = true;
            playVideo.likes = playVideo.likes + 1;

            if (_.indexOf(playerVideoStats.dislikedVideos, videoId) != -1) {
                initiallyDisliked = true;
                playVideo.dislikeHighlight = false;
                playerVideoStats.dislikedVideos.splice(_.indexOf(playerVideoStats.dislikedVideos, videoId), 1);
                playVideo.dislikes = playVideo.dislikes - 1;
            }
        }


        likeVideo.get({
            userId: loggedInUser.info._id,
            videoId: videoId,
            like: playVideo.likeHighlight,
            dislike: initiallyDisliked
        }, function (data) {


            // playVideo.likeHighlight = $filter('likeVideoFilter')(playVideo.videoInfo._id);
            //
            // playVideo.dislikeHighlight = $filter('dislikeVideoFilter')(playVideo.videoInfo._id);
            if (activeTab == 3) {
                _.each(playerUploadedVideos.videosList, function (video) {
                    if (video._id == videoId) {
                        if (liked) {
                            video.likes = video.likes - 1;
                        }
                        else {
                            video.likes = video.likes + 1;
                        }
                        if (initiallyDisliked) {
                            video.dislikes = video.dislikes - 1;
                        }
                    }
                })
            }
            else {
                _.each(fetchedVideoList.video, function (video) {
                    if (video._id == videoId) {
                        if (liked) {
                            video.likes = video.likes - 1;
                        }
                        else {
                            video.likes = video.likes + 1;
                        }
                        if (initiallyDisliked) {
                            video.dislikes = video.dislikes - 1;
                        }
                    }
                });
            }
        }, function (err) {
            console.log(err)
        });


    }
    playVideo.subscribe = function (subscribed, verdict) {
        Subscribe.save({userId: loggedInUser.info._id, subscribed: subscribed, verdict: verdict}, function (data) {
                if (verdict)
                    Subscribed.Subscribed.push(subscribed)
                else
                    Subscribed.Subscribed.splice(_.indexOf(Subscribed.Subscribed, subscribed), 1);

            }, function (err) {
                console.log(err);
            }
        )
    }

    var myPopup;
    playVideo.showPopup = function () {
        function onConfirmSubscribe(buttonIndex) {
            // alert('You selected button ' + buttonIndex);

            if (buttonIndex == 1) {

                var addBookmark;
                if (_.indexOf(myBookmarkedVideo.bookmarkedVideos, playVideo.videoInfo._id) == -1) {
                    myBookmarkedVideo.bookmarkedVideos.push(playVideo.videoInfo._id);
                    addBookmark = true;
                }
                else {
                    addBookmark = false;
                    myBookmarkedVideo.bookmarkedVideos.splice(_.indexOf(myBookmarkedVideo.bookmarkedVideos, playVideo.videoInfo._id), 1)
                }
                Bookmark.save({
                    userId: loggedInUser.info._id,
                    videoId: playVideo.videoInfo._id,
                    addBookmark: addBookmark
                }, function (data) {

                })
                playVideo.isSubscribed = !playVideo.isSubscribed;

            }
            if (buttonIndex == 2) {

                var verdict;
                if (_.indexOf(Subscribed.Subscribed, playVideo.videoInfo.videoBy._id) == -1)
                    verdict = true;
                else
                    verdict = false;
                playVideo.subscribe(playVideo.videoInfo.videoBy._id, verdict);
                playVideo.isSubscribed = !playVideo.isSubscribed;

            }
        }

        //unsubscribing single video
        function onConfirmUnsubscribeSingle(buttonIndex) {
            if (buttonIndex == 1) {
                var addBookmark = false;
                myBookmarkedVideo.bookmarkedVideos.splice(_.indexOf(myBookmarkedVideo.bookmarkedVideos, playVideo.videoInfo._id), 1)

                Bookmark.save({
                    userId: loggedInUser.info._id,
                    videoId: playVideo.videoInfo._id,
                    addBookmark: addBookmark
                }, function (data) {
                    playVideo.isSubscribedAsSingle = false;
                    playVideo.isSubscribed = false;
                })
            }
        }

        //unsubscribing whole package
        function onConfirmUnsubscribePackage(buttonIndex) {
            if (buttonIndex == 1) {
                playVideo.subscribe(playVideo.videoInfo.videoBy._id, false);
                playVideo.isSubscribedAsPackage = false;
                playVideo.isSubscribed = false;
            }
        }


        if (playVideo.isSubscribedAsPackage) {
            navigator.notification.confirm(
                'This video is a part of subscribed package. You will have to un-subscribe the whole package', // message
                onConfirmUnsubscribePackage,            // callback to invoke with index of button pressed
                'Confirm',           // title
                ['Unsubscribe package', 'Cancel']     // buttonLabels
            );
        }
        else if (playVideo.isSubscribedAsSingle) {
            navigator.notification.confirm(
                'Are you sure you want to un-subscribe this video', // message
                onConfirmUnsubscribeSingle,            // callback to invoke with index of button pressed
                'Confirm',           // title
                ['Yes', 'Cancel']     // buttonLabels
            );
        }
        else {
            navigator.notification.confirm(
                'Do you want to subscribe to this video alone or all videos of this user', // message
                onConfirmSubscribe,            // callback to invoke with index of button pressed
                'Confirm',           // title
                ['Just this video', 'All videos of this user']     // buttonLabels
            );
        }


    }

    playVideo.feedbackInstructions = function () {
        function understood(buttonIndex) {
            if (buttonIndex == 1) {

            }
        }

        navigator.notification.alert(
            'You need to cover following points in your feedback\n - Review the video\n - Review the skills\n - Review something else', // message
            understood,            // callback to invoke with index of button pressed
            'Feedback Instructions',           // title
            'Ok'     // buttonLabels
        );
    }


    playVideo.postComment = function () {
        if (typeof playVideo.comment != 'undefined' && playVideo.comment != '') {
            Comment.save({
                userId: loggedInUser.info._id,
                comment: playVideo.comment,
                videoId: playVideo.videoInfo._id
            }, function (data) {


                playVideo.commentsList.push({
                    comment: playVideo.comment,
                    userId: loggedInUser.info,
                    createdAt: new Date()
                });

                playVideo.comment = '';
            }, function (err) {
                console.log(err);
            })
        }
    }

}
