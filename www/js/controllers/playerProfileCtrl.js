angular.module('starter')

    .controller('playerProfileCtrl', playerProfileCtrl)

function playerProfileCtrl($window, $state, userProfileParams, $rootScope, Follow, NewsFeed, Following, loggedInUser, $timeout, Video, SportsList) {
    var playerProfile = this;
    playerProfile.videos = 0;
    playerProfile.status = 1;
    playerProfile.prevStatus = 1;
    playerProfile.playerInfo = userProfileParams.params;


    //-------------------------DESIGN CALCULATION START-------------------------//
    var devHeight = $window.innerHeight;
    var devWidth = $window.innerWidth;


    playerProfile.logoDiv = {
        'margin-top': 0.1 * devHeight + 'px',
        'height': 0.2 * devHeight + 'px',
        'margin-bottom': 0.04 * devHeight + 'px'
    };
    playerProfile.datum = {
        'left': devWidth + 'px'
    };
    playerProfile.scrollerHeight = {
        'height': devHeight - 200 - 92 + 'px'
    };

    $timeout(function () {
        var tabHeight1 = document.querySelector('#info').offsetHeight;
        var tabHeight2 = document.querySelector('#exp').offsetHeight;
        var tabHeight3 = document.querySelector('#videos').offsetHeight;
        var minHeight = Math.max(devHeight - 200 - 88, tabHeight1, tabHeight2, tabHeight3)
        console.log(minHeight);

        playerProfile.info = {
            'min-height': Math.max(devHeight - 200 - 88, tabHeight1, tabHeight2, tabHeight3) + 'px'
        }
        playerProfile.exp = {
            'min-height': Math.max(devHeight - 200 - 88, tabHeight1, tabHeight2, tabHeight3) + 'px'
        }
        playerProfile.videos = {
            'min-height': Math.max(devHeight - 200 - 88, tabHeight1, tabHeight2, tabHeight3) + 'px'
        }
    }, 200);
    //-------------------------DESIGN CALCULATION END-------------------------//

    playerProfile.return = function () {
        if (!loggedInUser.info.player) {
            $state.go('coachSidemenu.tabs.findPlayer');
        }
        else {
            $state.go('sidemenu.findPlayer');
        }
    }

    playerProfile.browseVideos = function () {
        playerProfile.videos = 1;
    }
    playerProfile.browseBasic = function () {
        playerProfile.videos = 2;
    }

    playerProfile.changePrevStatus = function (number) {
        playerProfile.status = number;
        $timeout(function () {
            playerProfile.prevStatus = number;
        }, 500)
    }


    playerProfile.followUser = function (following) {

        Follow.save({following: following, userId: loggedInUser.info._id}, function (data) {
            console.log(data);

            playerProfile.playerInfo.follow = true;
            Following.list.push(following);
        }, function (err) {
            console.log(err);
        })
    }


    playerProfile.unFollowUser = function (following) {

        Follow.unfollow({following: following, userId: loggedInUser.info._id}, function (data) {
            playerProfile.playerInfo.follow = false;

            Following.list = _.reject(Following.list, function (item) {
                return item == following;
            });
        }, function (err) {
            console.log(err);
        })
    }


    playerProfile.playVideo = function (video) {

        console.log(video);
        if (!loggedInUser.info.player) {
            $state.go('coachSidemenu.tabs.playVideoPlayerProfileSearch', {videoInfo: video});
        }
        else {
            $state.go('sidemenu.playVideoPlayerProfileSearch', {videoInfo: video});
        }
    }

    playerProfile.playerVideoList = [];

    playerProfile.getVideos = function (playerId) {
        Video.get({userId: playerId, lastVideoFetchTime: ''}, function (data) {

                _.each(data.videoList, function (video) {
                    playerProfile.playerVideoList.push(video);
                })
            },
            function (err) {
                console.log(err);
            })
    }

    playerProfile.getVideos(playerProfile.playerInfo._id);

    playerProfile.newsFeedList = [];
    var oldFetchTime = ''
    playerProfile.loadMoreNewsFeed = true;

    function fetchNews() {
        console.log(query);
        NewsFeed.get(query, function (result) {
            console.log(result)
            _.each(result.newsFeed, function (feed) {

                var newsFeedSportId = typeof feed.trainingPlanId != 'undefined' ? feed.trainingPlanId.sport : (typeof feed.videoId != 'undefined' ? feed.videoId.sport : 'sample.png');

                if (newsFeedSportId == 'sample.png')
                    feed.image = newsFeedSportId
                else {
                    feed.image = _.find(SportsList.list, function (item) {
                        return newsFeedSportId == item._id;

                    }).image;
                }
                console.log(feed.image);

                var localNewsFeed = '';
                var message = _.find(newsFeed, function (item) {
                    return item.code == feed.newsFeedCode
                }).newsFeed;

                if (typeof feed.videoId != 'undefined' || feed.videoId != null)
                    localNewsFeed = feed.userId.firstName + ' ' + message + ' ' + feed.videoId.title;

                else if (typeof feed.trainingPlanId != 'undefined' && feed.trainingPlanId != null) {
                    localNewsFeed = feed.userId.firstName + ' ' + message + ' ' + feed.trainingPlanId.trainingName;
                }
                else
                    localNewsFeed = feed.userId.firstName + ' ' + message;


                playerProfile.newsFeedList.push({
                    feed: {
                        feed: localNewsFeed,
                        image: feed.image
                    },
                    createdAt: feed.createdAt, updatedAt: feed.updatedAt
                });

                oldFetchTime = feed.createdAt;
                console.log(oldFetchTime)

                $timeout(function () {
                    playerProfile.loadMoreNewsFeed = result.loadMoreNewsFeed;


                }, 100);

                $rootScope.$broadcast('scroll.infiniteScrollComplete');

            });


        });


    }

    playerProfile.getOldNewsFeed = function () {
        query = {
            userId: playerProfile.playerInfo._id,
            fetchcase: 3,
            oldFetchTime: oldFetchTime
        }
        fetchNews();
    }


    // fetchNews();
}
