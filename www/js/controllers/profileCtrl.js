angular.module('starter')

    .controller('profileCtrl',profileCtrl)
function profileCtrl($window,$scope,SportExperience, $state,$rootScope,$localForage,mySports,fetchedVideoInfo,
                     loggedInUser,$ionicSlideBoxDelegate, sportsPreExperience,sportsExperience,SportsList,addSportsExperience, IonicClosePopupService,$cordovaToast, $ionicPopup) {
        var profile = this;
        console.log($state.current.name);
        profile.currentSlide=1;

    document.addEventListener("deviceready", onDeviceReady, false);
    function onDeviceReady() {
        console.log(device.cordova);
    }
        profile.skip = function () {
            console.log('mysports');
            console.dir(mySports);
            if(mySports.sports.length==0){
                if(!window.cordova){
                    alert('You need to answer questions for at least one sport');
                }
                else{
                    $cordovaToast.showShortBottom('You need to answer questions for at least one sport')
                }
// **************************** Uncomment for phone build - START ***************************** //

                // var onConfirm = function () {
                //     console.log('skipping skipped');
                // }
                //     navigator.notification.alert(
                //     'You need to add at least one sport to your profile to get started', // message
                //     onConfirm,            // callback to invoke with index of button pressed
                //     'No sport added',           // title
                //     'Ok'     // buttonLabels
                // );
// **************************** Uncomment for phone build - END ***************************** //

            }
            else {
                // var options =
                // {
                //     title: 'Choose a Video',
                //     cssClass: 'sportsPickPopup', // String, The custom CSS class name
                //     templateUrl: 'templates/player/skip-profile.html',
                //     scope: $scope
                // }
                var options = {
                    title: 'Skip profile',
                    cssClass: 'sportsPickPopup', // String, The custom CSS class name
                    scope: $scope,
                    template: '<div skip-profile></div>'
                }
                // var skipPopup = $ionicPopup.alert(options);
                skipPopup = $ionicPopup.alert(options);

                skipPopup.then(function (res) {
                });
                IonicClosePopupService.register(skipPopup);
            }

        };


        //-------------------------DESIGN CALCULATION START-------------------------//
        var devHeight = $window.innerHeight;
        var devWidth = $window.innerWidth;
        profile.title = {'margin-bottom': 0.03 * devHeight + 'px'};
        profile.fullHeight = {'height': devHeight-44-44 + 'px'};
        profile.maxWidth = {'max-width': devWidth + 'px'};
        // $timeout(function () {
        //     var tournamentAddHeight = document.querySelector('#addTournament').offsetHeight;
        //     var academyAddHeight = document.querySelector('#addAcademy').offsetHeight;
        //
        //     profile.scrollerHeight = {'height': devHeight-tournamentAddHeight-88 + 'px'};
        //     profile.scroller2Height = {'height': devHeight-academyAddHeight-88 + 'px'};
        //
        // },200);

        //-------------------------DESIGN CALCULATION END-------------------------//

        profile.nextSlide = function() {
            $ionicSlideBoxDelegate.next();
            profile.currentSlide = $ionicSlideBoxDelegate.currentIndex()+1;
        }

        profile.prevSlide = function() {
            $ionicSlideBoxDelegate.previous();
            profile.currentSlide = $ionicSlideBoxDelegate.currentIndex()+1;
        }

        profile.goToChooseSport = function () {
            $state.go('sidemenu.chooseSport');
        }

        profile.goToHome = function () {
            if($rootScope.registerAsCoach){
                $state.go('coachSidemenu.tabs.training');
            }
            else{
                $state.go('sidemenu.tabs.training');
            }
        }

        profile.previousExperience=[];
        profile.experience=[];
        profile.academy={'current':false};
        profile.tournament={};
        profile.details={previousExperience:'',
                        experience:'',
                        refToTournament:[],
                        refToAcademy:[]
        };

        _.each(_.map(sportsPreExperience.list,'preExperience'),function(exp){
            profile.previousExperience.push({previousExperience:exp,radioButtonVal:false});
        });
        _.each(_.map(sportsExperience.list,'experience'),function(exp){
            profile.experience.push({experience:exp,radioButtonVal:false});
        });
        profile.addPreviousExperience=function(obj)
        {
            _.each(profile.previousExperience,function(pre){
                if(pre.previousExperience==obj.previousExperience)
                {
                    pre.radioButtonVal=true;
                }
                else {
                    pre.radioButtonVal = false;
                }
            })
            profile.details.previousExperience=obj.previousExperience;
        }
        profile.addExperience=function(obj)
        {
            _.each(profile.experience,function(pre){
                if(pre.experience==obj.experience)
                {
                    pre.radioButtonVal=true;
                }
                else {
                    pre.radioButtonVal = false;
                }
            })
            profile.details.experience=obj.experience;
        }

        //
        //profile.addAcademy=function()
        //{
        //    profile.academy.sportId=addSportsExperience.sportId;
        //    profile.academy.userId=loggedInUser.info._id;
        //    if(!checkValidAcademy(profile.academy))
        //    {
        //        profile.details.refToAcademy.push(profile.academy);
        //        profile.academy={};
        //    }
        //}
        //
        //profile.addTournament=function()
        //{
        //    profile.tournament.sportId=addSportsExperience.sportId;
        //    profile.tournament.userId=loggedInUser.info._id;
        //    if(!checkValidTournament(profile.tournament))
        //    {
        //        profile.details.refToTournament.push(profile.tournament);
        //        profile.tournament={};
        //    }
        //}
        //
        //function checkValidTournament(tournament)
        //{
        //    profile.tournamentTitleError=false;
        //    profile.tournamentRoleError=false;
        //    profile.tournamentDateError=false;
        //    profile.tournamentTeamError=false;
        //    profile.tournamentLocationError=false;
        //    var errorOccured=false;
        //
        //    if(typeof tournament.name=='undefined')
        //    {
        //        profile.tournamentTitleError=true;
        //        errorOccured=true;
        //    }
        //
        //    if(typeof tournament.date=='undefined')
        //    {
        //        profile.tournamentDateError=true;
        //        errorOccured=true;
        //    }
        //
        //    if(typeof tournament.role=='undefined')
        //    {
        //        profile.tournamentRoleError=true;
        //        errorOccured=true;
        //    }
        //
        //    if(typeof tournament.team=='undefined')
        //    {
        //        profile.tournamentTeamError=true;
        //        errorOccured=true;
        //    }
        //
        //    if(typeof tournament.location=='undefined')
        //    {
        //        profile.tournamentLocationError=true;
        //        errorOccured=true;
        //    }
        //    return  errorOccured;
        //}
        //
        //function checkValidAcademy(academy)
        //{
        //    profile.academyNameError=false;
        //    profile.academyFromError=false;
        //    profile.academyToError=false;
        //    var errorOccured=false;
        //    if(typeof academy.name=='undefined')
        //    {
        //        profile.academyNameError=true;
        //        errorOccured=true;
        //    }
        //
        //    if(typeof academy.from=='undefined')
        //    {
        //        profile.academyFromError=true;
        //        errorOccured=true;
        //    }
        //
        //    if(typeof academy.to=='undefined')
        //    {
        //        profile.academyToError=true;
        //        errorOccured=true;
        //    }
        //    return  errorOccured;
        //}

        profile.addSportExperience=function(addMoreSport)
        {

            profile.details.userId=loggedInUser.info._id;
            profile.details.sportId=addSportsExperience.sportId;
            profile.details.player=loggedInUser.info.player;
            SportExperience.save({experience:profile.details},function(data){
                loggedInUser.info.mySports.push(data.sportId);
                loggedInUser.info.sportsExperience.push(data);
                $localForage.setItem('loggedInUser',loggedInUser.info);
                _.each(SportsList.list, function (sport) {
                    _.each(loggedInUser.info.mySports, function (localSport) {

                        if (localSport == sport._id) {
                            if(_.indexOf(_.map(mySports.sports,'_id'),localSport)==-1)
                            mySports.sports.push(sport);
                            fetchedVideoInfo.info.push({'sportId': sport._id, 'lastVideoFetchTime': ''})
                        }
                    })
                })
                if(addMoreSport)
                    profile.goToChooseSport();
                else {
                    if(loggedInUser.info.player)
                    $state.go('sidemenu.tabs.training');
                    else
                        $state.go('coachSidemenu.tabs.training');
                }

            },function(err){
            })
        }

    }
