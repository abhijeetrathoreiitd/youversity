angular.module('starter')

    .controller('reviewVideosCtrl',reviewVideosCtrl)
function reviewVideosCtrl($window, $state, $ionicPopup, $ionicSideMenuDelegate, $timeout, $rootScope, $ionicModal, $scope, $ionicNavBarDelegate) {
        var reviewVideos = this;
        reviewVideos.videos = 0;

        $scope.$on('$ionicView.enter', function (e) {
            console.log('bar shown')
            $ionicNavBarDelegate.showBar(true);
        });

        reviewVideos.playReviewVideo = function () {
            $rootScope.showTabs = false;
            $state.go('coachSidemenu.tabs.playReviewVideo');
        }
        // reviewVideos.selectSport = function(){
        //     reviewVideos.sportSelected = true;
        // }


        //-------------------------DESIGN CALCULATION START-------------------------//
        var devHeight = $window.innerHeight;
        var devWidth = $window.innerWidth;

        console.log('width:' + devWidth + 'height:' + devHeight);
        reviewVideos.logoDiv = {
            'margin-top': 0.1 * devHeight + 'px',
            'height': 0.2 * devHeight + 'px',
            'margin-bottom': 0.04 * devHeight + 'px'
        };
        reviewVideos.datum = {
            'left': devWidth + 'px'
        };
        reviewVideos.sportScroll = {
            'width': 0.7 * devWidth + 'px'
        };
        reviewVideos.placeholder = {
            'position': 'absolute',
            'top': 0.3 * (devHeight - 44 - 49) - 44 + 'px',
            'width': 100 + '%'
        };


        //-------------------------DESIGN CALCULATION END-------------------------//

        reviewVideos.goBack = function () {
            $rootScope.showTabs = true;
            $state.go('coachSidemenu.tabs.videoLib');
        }

    }
