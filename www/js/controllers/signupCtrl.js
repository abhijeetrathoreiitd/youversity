angular.module('starter')

    .controller('signupCtrl',signupCtrl)
function signupCtrl($q, $window, $state, $rootScope,$ionicLoading,User,$timeout,
                    loggedInUser,$localForage,loginService,UserService,gmailLogin) {
        var signup = this;

        if($state.current.name=='coachSidemenu.signup')
               signup.player = false;
        else
            signup.player = true;
        //-------------------------DESIGN CALCULATION START-------------------------//
        var devHeight = $window.innerHeight;
        var devWidth = $window.innerWidth;
        signup.signup = {'height': 0.08 * devHeight + 'px','margin-top':0.01*devHeight+'px'};
        signup.logoDiv = {'margin-top': 0.1 * devHeight + 'px', 'height': 0.2 * devHeight + 'px','margin-bottom':0.04*devHeight+'px'};
        signup.formDiv = {'margin-top': 0.02 * devHeight + 'px'};
        signup.fullHeight = {'height': devHeight + 'px'};
        signup.paddingHorizontal = {'padding-left':0.1*devWidth+'px','padding-right':0.1*devWidth+'px'}
        signup.joinButton = {'width': 100 + '%', 'margin-bottom': 0.02 * devHeight + 'px',
        'padding-left':0.1*devWidth+'px','padding-right':0.1*devWidth+'px'};
        signup.socialLoginButton = {'height': 0.1 * devHeight + 'px'};

        //-------------------------DESIGN CALCULATION END-------------------------//

        signup.goToLogin = function () {
            $state.go('sidemenu.login');
        }

        signup.goToChooseSport = function () {
            if($rootScope.registerAsCoach){
                $state.go('coachSidemenu.chooseSport');
            }
            else{
                $state.go('sidemenu.chooseSport');
            }
        }
        signup.selectPlayer = function(){
            signup.player = true;
        }
        signup.selectCoach = function(){
            signup.player = false;
        }



        signup.user={};
        signup.userExistsError=false;
        signup.signUpFunction=function()
        {
            signup.user.player=signup.player;
            signup.user.coach=!signup.player;
            $ionicLoading.show({'template':'Signing up'});
            User.save({email:signup.user.email,password:signup.user.password,user:signup.user},
                function(data){
                    loggedInUser.info=data.user;
                    loggedInUser.info.mySports=[];
                    loggedInUser.info.sportsExperience=[];

                    $localForage.setItem('loggedInUser',data.user)
                        .then(function(data)
                        {
                            console.log(data);
                        });

                    $ionicLoading.hide();

                    if(signup.player)
                        $state.go('sidemenu.chooseSport');
                    else
                        $state.go('coachSidemenu.chooseSport')
                },
                function(err){

                    $ionicLoading.hide();
                    signup.userExistsError=true;
                    $timeout(function(){

                        signup.userExistsError=false;
                    },3000);

                console.log(err);
            })
        }


}
