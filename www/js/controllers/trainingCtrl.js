angular.module('starter')

    .controller('trainingCtrl', trainingCtrl);

function trainingCtrl($window, $ionicScrollDelegate, myTrainingParams,
                      trainingPlanParams, TrainingPlanList, playerTrainingPlanList, SportsList,
                      $state,$stateParams, trainingPlanCategoryList, trainingPlan, trainingPlanCategory,
                      loggedInUser, $timeout, $rootScope, $ionicModal, $scope, $ionicNavBarDelegate, currentWeekParams) {
    var training = this;
    training.videos = 0;

    //Whether search bar is opened or not
    training.showDashBoard = false;
    training.searchOpen = false;
    training.searchOpenLate = false;
    training.menuDropped = false;
    training.backdrop = false;
    training.streamChosen = false;
    training.deltaRating = 5;
    training.cost = {
        min: '0',
        max: '10000',
        value: '10000'
    }
    training.duration = {
        min: '1',
        max: '20',
        value: '20'
    }


    training.customerRating = '';
    training.rate1 = false;
    training.rate2 = false;
    training.rate3 = false;
    training.rate4 = false;
    training.rate5 = false;


    training.costFilter = training.cost.value;
    training.durationFilter = training.duration.value;
    training.deltaPaidFilter = training.cost.value;
    training.paidFilter = training.cost.value;
    training.paidCheckBoxVal = true;
    training.ratingFilter = 5;


    $timeout(function () {
        training.loadMoreViews = true;
    }, 300);

    $scope.$on('$ionicView.enter', function (e) {
        console.log('bar shown')
        $ionicNavBarDelegate.showBar(true);
    });

    // training.toggleLeft = function () {
    //     console.log('toggle left')
    //     $ionicSideMenuDelegate.toggleLeft();
    // };


    $ionicModal.fromTemplateUrl('templates/player/trainingmodal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modal) {
        training.modal = modal;
    });
    training.showFilters = function () {
        training.modal.show();
    }
    training.hideFilters = function () {
        console.log('hide modal');
        training.modal.hide();
    }


    training.changePrevStatus = function (number) {

        $ionicScrollDelegate.scrollTop();
        if (number == 1)
            training.showDashBoard = true;
        else
            training.showDashBoard = false;
        training.status = number;
        $timeout(function () {
            training.prevStatus = number;
        }, 500);
        $ionicScrollDelegate.resize();

    }
    training.showSearch = function () {
        training.searchOpen = true;
        $timeout(function () {
            training.searchOpenLate = true;
        }, 400)
    }
    training.hideSearch = function () {
        training.searchOpen = false;
        $timeout(function () {
            training.searchOpenLate = false;
        }, 100)
    }

    training.selectSport = function (sport) {
        $rootScope.sportSelected = sport;
        training.streamChosen = false;
        training.getTrainingPlanCategory();

        training.backdrop = false;
        $timeout(function () {
            training.menuDropped = false;
        }, 500)
    }

    training.dropMenu = function () {
        training.backdrop = true;
        $timeout(function () {
            training.menuDropped = true;
        }, 100)
    }
    training.hideMenu = function () {
        training.backdrop = false;
        $timeout(function () {
            training.menuDropped = false;
        }, 500)
    }

    training.selectRating = function (rating) {
        training.deltaRating = rating;
    }


    training.sortBy = 'trainingName';
    training.deltaSortBy = 'trainingName';
    training.sortByFunction = function (sortBy) {
        training.deltaSortBy = sortBy;

    }


    training.changePaidFilter = function () {
        if (training.paidCheckBoxVal) {
            training.deltaPaidFilter = training.cost.value;
        }
        else {
            training.deltaPaidFilter = 0;
        }
    }


    training.applyFilter = function () {
        training.sortBy = training.deltaSortBy;
        training.modal.hide();
        training.costFilter = training.cost.value;
        training.durationFilter = training.duration.value;
        training.paidFilter = training.deltaPaidFilter;
        training.ratingFilter = training.deltaRating;


        $ionicScrollDelegate.resize();
    }
    //-------------------------DESIGN CALCULATION START-------------------------//
    var devHeight = $window.innerHeight;
    var devWidth = $window.innerWidth;

    console.log('width:' + devWidth + 'height:' + devHeight);
    training.logoDiv = {
        'margin-top': 0.1 * devHeight + 'px',
        'height': 0.2 * devHeight + 'px',
        'margin-bottom': 0.04 * devHeight + 'px'
    };
    training.fullDim={'width': devWidth+'px', 'height': devHeight+'px'};
    training.datum = {
        'left': devWidth + 'px'
    };
    training.sportScroll = {
        'width': 0.7 * devWidth + 'px'
    };
    training.placeholder = {
        'position': 'absolute',
        'top': 0.3 * (devHeight - 44 - 49) - 44 + 'px',
        'width': 100 + '%'
    };
    // $timeout(function () {
    //     var tabHeight1 = document.querySelector('#myplan').offsetHeight;
    //     var tabHeight2 = document.querySelector('#discover').offsetHeight;
    //     var minHeight = Math.max(devHeight - 88 - 49, tabHeight1, tabHeight2)
    //
    //     training.myplan = {
    //         'min-height': Math.max(minHeight, tabHeight1, tabHeight2) + 'px'
    //     }
    //     training.discover = {
    //         'min-height': Math.max(minHeight, tabHeight1, tabHeight2) + 'px'
    //     }
    //
    // }, 200);
    training.slidermin = {'left': 0.2 * (devWidth - 22) + 13 + 'px'};
    training.sliderWidth = devWidth - (0.2 * (devWidth - 22) + 20 + 48 + 22 + 3) - 30;
    training.slidervalue = {'left': 0.2 * (devWidth - 22) + 11 + 24 + 10 + ((parseInt(training.cost.value, 10) - 20) * training.sliderWidth / 80) + 'px'};
    training.slidervalue2 = {'left': 0.2 * (devWidth - 22) + 11 + 24 + 10 + ((parseInt(training.duration.value, 10) - 1) * training.sliderWidth / 19) + 'px'};
    training.general = {'height':devHeight +'px','width':devWidth +'px'};

    //-------------------------DESIGN CALCULATION END-------------------------//
    $scope.$watch(angular.bind(training, function () {
        return training.cost.value;
    }), function (value) {
        training.slidervalue = {'left': 0.2 * (devWidth - 22) + 11 + 24 + 10 + ((parseInt(training.cost.value, 10) - 20) * training.sliderWidth / 80) + 'px'};
    });
    $scope.$watch(angular.bind(training, function () {
        return training.duration.value;
    }), function (value) {
        training.slidervalue2 = {'left': 0.2 * (devWidth - 22) + 11 + 24 + 10 + ((parseInt(training.duration.value, 10) - 1) * training.sliderWidth / 19) + 'px'};
    });

    training.seeDetail = function (trainingPlan) {
        $rootScope.showTabs = false;
        console.log(trainingPlan)
        trainingPlanParams.params = trainingPlan
        currentWeekParams.week = 0;
        $state.go('sidemenu.tabs.trainingDetail');
    }

    training.seeOngoing = function () {
        $rootScope.showTabs = false;
        $state.go('sidemenu.tabs.mytrainingDetail');
    }

    training.chooseStream = function (category) {
        console.log(category);
        training.chosenStreamName = category.category;
        var categoryId = category._id;
        training.trainingPlanList = [];
        var queryObject;
        training.streamChosen = true;
        queryObject = {categoryId: categoryId, sport: $rootScope.sportSelected._id};


        trainingPlan.query(queryObject, function (data) {
            training.trainingPlanList = data;
            console.log(data);
        })

    }
    training.chooseStreamAgain = function(){
        training.streamChosen = false;
        training.chosenStreamName = '';

    }

    //==================================================================================
    //==============================PLAYER's ACTIONS====================================


    training.mySports = [];

    _.each(SportsList.list, function (sport) {
        _.each(loggedInUser.info.mySports, function (mySport) {
            if (mySport == sport._id)
                training.mySports.push(sport);
        })
    });

    training.trainingPlanCategoryList = trainingPlanCategoryList.category;


    training.getTrainingPlanCategory = function () {
        // if ($rootScope.sportSelected && _.indexOf(trainingPlanCategoryList.sportsList, $rootScope.sportSelected._id) == -1) {
        //     trainingPlanCategoryList.sportsList.push($rootScope.sportSelected._id);
        if (training.trainingPlanCategoryList.length == 0 && $rootScope.sportSelected) {
            trainingPlanCategory.query(function (data) {
                console.log(data);
                _.each(data, function (category) {
                    training.trainingPlanCategoryList.push(category);
                })
            }, function (err) {
                console.log(err);
            })
        }
        // }
        else {
            console.log('no sport selected');
        }
    }
    training.onGoingPlan = 0;
    training.allPlanStatus = [];


    training.getMyTrainingPlans=function() {


        TrainingPlanList.get({
                userId: loggedInUser.info._id
            },
            function (data) {

                training.playerTrainingPlanList = [];
                training.playerTrainingPlanIds = [];
                console.log(data);
                if (data.myData != null) {
                    _.each(data.myData.planList, function (plan) {
                        console.log(plan);
                        var percentageCompleted = (_.filter(plan.activity, function (act) {
                            return act.completed == true;
                        }).length * 100 / plan.activity.length);
                        if (percentageCompleted != 100)
                            training.allPlanStatus.push({
                                'planName': plan.trainingPlanId.trainingName,
                                percentageDone: percentageCompleted,
                                status: plan.status
                            });

                        plan.percentageCompleted = percentageCompleted;
                        switch (plan.status) {
                            case 'notStarted':
                                plan.statusText = 'start';
                                break;

                            case 'started':
                                plan.statusText = 'pause';
                                training.onGoingPlan++;
                                break;

                            case 'paused':
                                plan.statusText = 'start';
                                break;

                        }
                        training.playerTrainingPlanList.push(plan);
                        training.playerTrainingPlanIds.push(plan.trainingPlanId._id);


                    });

                }
            }, function (err) {
                console.log(err);
            })
    }

    training.getMyTrainingPlans();

    training.goToMyTrainingDetails = function (plan) {
        $rootScope.showTabs = false;
        myTrainingParams.params = plan;
        currentWeekParams.week = 0;//reset week to 0
        $state.go('sidemenu.tabs.mytrainingDetail');
    }


    //==================================================================================
    //==============================PLAYER's END=++++===================================


    // DASHBOARD STATS
    training.showSpinner = true;

    TrainingPlanList.get({
        userId: loggedInUser.info._id
    }, function (data) {
        if(data.myData) {
            var totalWeekTrained = 0;
            _.each(data.myData.planList, function (plan) {

                totalWeekTrained = totalWeekTrained + (_.keys(_.groupBy(_.filter(plan.activity, function (pl) {
                        return pl.completed == true;

                    }), 'day')).length) / 7;
                console.log(totalWeekTrained)

            });

            training.ongoingLabels = [];
            training.ongoingData = [];
            training.completedOptions = [];
            training.dots = [];
            _.each(training.playerTrainingPlanList, function (n) {
                if (n.status != 'ended') {

                    var percentArray = [];
                    percentArray[0] = n.percentageCompleted;
                    training.ongoingLabels.push(n.trainingPlanId.trainingName);
                    training.ongoingData.push(percentArray);
                    training.dots.push('');
                }
            })
            console.log(training.ongoingData);

            training.ongoingSeries = ['Series A'];
            training.completedOptions = {
                tooltips: {
                    enabled: false,
                    backgroundColor: 'rgba(151,187,205,1)',
                    multiKeyBackground: "#97BBCD"
                }
            }
            training.ongoingOptions = {
                tooltips: {
                    enabled: false,
                    backgroundColor: 'rgba(151,187,205,1)',
                    multiKeyBackground: "#97BBCD"
                },
                fullWidth: false,
                padding: 0,
                responsive: false,
                scaleShowLabels: false,
                scales: {
                    xAxes: [{
                        barThickness: 20,
                        display: false,
                        gridLines: {
                            display: false
                        },
                        ticks: {
                            max: 100,
                            min: 0
                        }
                    }],
                    yAxes: [{
                        display: false,
                        barThickness: 20
                    }]
                }

            }


            training.totalWeekTrained = totalWeekTrained;
            var totalCompletedPlans = _.filter(data.myData.planList, function (plan) {
                return plan.status == 'ended';
            });
            var totalCompletedPlanBeginner = 0;
            var totalCompletedPlanIntermediate = 0;
            var totalCompletedPlanAdvanced = 0;
            var totalCompletedPlanFitness = 0;//57f5fa67c36e258808cc8205
            var totalCompletedPlanStrategy = 0;//57f5fa62c36e258808cc8204
            var totalCompletedPlanSkills = 0;//57f5fa57c36e258808cc8203
            _.each(totalCompletedPlans, function (plan) {

                if (_.indexOf(plan.trainingPlanId.category, '57f5fa62c36e258808cc8204') != -1)
                    totalCompletedPlanStrategy++;

                if (_.indexOf(plan.trainingPlanId.category, '57f5fa67c36e258808cc8205') != -1)
                    totalCompletedPlanFitness++;

                if (_.indexOf(plan.trainingPlanId.category, '57f5fa57c36e258808cc8203') != -1)
                    totalCompletedPlanSkills++;

                switch (plan.trainingPlanId.level) {
                    case 'beginner':
                        totalCompletedPlanBeginner++;
                        break;
                    case 'intermediate':
                        totalCompletedPlanIntermediate++;
                        break;
                    case 'advanced':
                        totalCompletedPlanAdvanced++;
                        break;
                }
            });

            training.showPieChart = true;
            var totalPlans = data.myData.planList.length;
            training.completedPlans = totalCompletedPlans.length;
            training.labelLevelWise = ['Beginner', 'Intermediate', 'Advanced'];
            training.dataLevelWise = [totalCompletedPlanBeginner, totalCompletedPlanIntermediate, totalCompletedPlanAdvanced];
            training.labelCategoryWise = ['Strategy', 'Fitness', 'Skills'];
            training.dataCategoryWise = [totalCompletedPlanStrategy, totalCompletedPlanFitness, totalCompletedPlanSkills];
            training.colors = ['#e74c3c ', '#27ae60 ', '#3498db ', '#46BFBD ', '#FDB45C ', '#949FB1 ', '#4D5360 '];
        }
        training.showSpinner = false;

    });



    console.log($stateParams.openTab)
    if($stateParams.openTab=='')
    {
        training.status = 1;
        training.prevStatus = 1;
        training.changePrevStatus(1);
    }
    else
    {
        training.status = $stateParams.openTab;
        training.prevStatus = $stateParams.openTab;
        training.changePrevStatus($stateParams.openTab);
    }


}
