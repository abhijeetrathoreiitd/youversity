angular.module('starter')

    .controller('trainingDetailCtrl', trainingDetailCtrl)
function trainingDetailCtrl($window, Activity, $cordovaToast, trainingPlanParams, currentWeekParams, TrainingPlanList, loggedInUser, $rootScope, $state, $timeout, $ionicLoading) {
    var trainingDetail = this;
    trainingDetail.videos = 0;

    //-------------------------DESIGN CALCULATION START-------------------------//
    var devHeight = $window.innerHeight;
    var devWidth = $window.innerWidth;


    var doCalculations = function () {
        var height2 = document.querySelector('#coachInfo').offsetHeight;
        var height3 = document.querySelector('#description').offsetHeight;
        trainingDetail.activityScroller = {'height': devHeight - 44 - 44 - 44 - 44  - height2 - height3 + 'px'};
        trainingDetail.myactivityScroller = {'height': devHeight - 44 - 44 - height2 - height3 + 'px'};

        // trainingDetail.activities = {'height': document.querySelector('#activityList').offsetHeight + 'px'}

    };

    //-------------------------DESIGN CALCULATION END-------------------------//

    trainingDetail.return = function () {
        $rootScope.showTabs = true;
        console.log('return')
        $state.go('sidemenu.tabs.training',{openTab:3});
    }


    console.log(trainingPlanParams.params)
    var trainingPlanId = trainingPlanParams.params._id;
    trainingDetail.trainingBy = trainingPlanParams.params.trainingBy;
    trainingDetail.description = trainingPlanParams.params.description;
    trainingDetail.trainingName = trainingPlanParams.params.trainingName;
    trainingDetail.duration = trainingPlanParams.params.duration;
    trainingDetail.level = trainingPlanParams.params.level;
    trainingDetail.noOfSubscriber = trainingPlanParams.params.noOfSubscriber;


    trainingDetail.activityList = [];//array of array
    var totalActivityCount = 0, totalWeek;
    $ionicLoading.show({"template": "Loading"});
    Activity.query({trainingPlanId: trainingPlanId}, function (data) {
        totalWeek = data.length / 7;
        data=_.sortBy(data, [function(o) { return o.day; }]);


        for (var i = 1; i <= totalWeek; i++) {
            trainingDetail.activityList[i - 1] = _.filter(data, function (activity) {
                if (activity.activities.length != 0 && i == 1) {//running this loop only once to avoid double count of total activity
                    totalActivityCount++;
                }
                return activity.day > 7 * (i - 1) && activity.day <= 7 * i;
            })
        }

        $ionicLoading.hide();
        doCalculations();
    }, function (err) {
    }, function (err) {
        $ionicLoading.hide();
        console.log(err);
    })

    trainingDetail.week = 0;
    trainingDetail.selectWeek = function (week) {
        trainingDetail.week = week;
    }


    trainingDetail.week = currentWeekParams.week;
    trainingDetail.selectWeek = function (week) {
        trainingDetail.week = week;
        currentWeekParams.week = week;
    }

    trainingDetail.goToActivityDetails = function (details) {
        $state.go('sidemenu.tabs.activityDetails', {details: details});
    }


    trainingDetail.subscribePlanOnConfirm = function () {
        console.log(totalActivityCount)
        console.log(totalWeek * 7)
        if (totalActivityCount == totalWeek * 7) {
            $ionicLoading.show({'template': 'Subscribing Plan'})
            TrainingPlanList.save({
                userId: loggedInUser.info._id,
                sport: trainingPlanParams.params.sport,
                trainingPlanId: trainingPlanParams.params._id
            }, function (data) {
                $ionicLoading.hide();
                trainingDetail.return();
            }, function (err) {

                $ionicLoading.hide();
                console.log(err);
            })
        }
        else {
            console.log('cannot subscribe');
            if(window.cordova) {
                $cordovaToast.showShortBottom('Plan is not complete. Cannot subscribe');
            }
            else{
                alert('Plan is not complete. Cannot subscribe');
            }
        }
    }



    trainingDetail.subscribePlan = function () {
        if (window.cordova) {
            var alertDismissed = function (buttonIndex) {
                if (buttonIndex == 1) {
                    trainingDetail.subscribePlanOnConfirm();
                }
            };

            navigator.notification.confirm(
                'Are you sure you want to subscribe this plan',  // message
                alertDismissed,         // callback
                'Subscribe Plan',            // title
                ['OK', 'Cancel']     // buttonName
            );
        }
        else {
            if (window.confirm('Are you sure you want to subscribe this plan')) {
                trainingDetail.subscribePlanOnConfirm();
            }
            else {
                // They clicked no
            }
        }
    }


}
