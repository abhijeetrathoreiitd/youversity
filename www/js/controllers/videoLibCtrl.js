angular.module('starter')

    .controller('videoLibCtrl', videoLibCtrl)
function videoLibCtrl($window, uploadService, Video, $state, mySports, fetchedVideoInfo,
                      $ionicPopup, IonicClosePopupService, $timeout, fetchedVideoList, $rootScope,
                      $ionicModal, $scope, $ionicNavBarDelegate, loggedInUser, playerUploadedVideos, backend, $ionicLoading,
                      $cordovaToast, $stateParams, $ionicScrollDelegate, Subscribed, subscribedVideoList, subscribedVideoPackage,
                      myBookmarkedVideo, Review, $sce, ConvertYoutubeDuration, uploadThumbnail, allLevelTagsList, $cordovaCalendar) {


    var videoLib = this;
    videoLib.isCoach = false;//for hiding add package in uploadVideoModal


    videoLib.isPlayer = loggedInUser.info.player;
    //-------------------------DESIGN CALCULATION START-------------------------//
    var devHeight = $window.innerHeight;
    var devWidth = $window.innerWidth;
    videoLib.player = {'min-height': 360 * devWidth / 640 + 'px'};
    videoLib.fullDim = {'width': devWidth + 'px', 'height': devHeight + 'px'};
    videoLib.logoDiv = {
        'margin-top': 0.1 * devHeight + 'px',
        'height': 0.2 * devHeight + 'px',
        'margin-bottom': 0.04 * devHeight + 'px'
    };
    videoLib.datum = {
        'left': devWidth + 'px'
    };
    videoLib.sportScroll = {
        'width': 0.7 * devWidth + 'px'
    };
    videoLib.placeholder = {
        'position': 'absolute',
        'top': 0.3 * (devHeight - 44 - 49) - 44 + 'px',
        'width': 100 + '%'
    };
    videoLib.videoFrame = {'width': devWidth - 30 + 'px', 'height': 9 * (devWidth - 30) / 16 + 'px'};

    var tabHeightCalculate = function () {
        $timeout(function () {
            var tabHeight1 = document.querySelector('#general').offsetHeight;
            var tabHeight2 = document.querySelector('#subscribed').offsetHeight;
            var tabHeight3 = document.querySelector('#uploaded').offsetHeight;
            var minHeight = Math.max(devHeight - 88 - 49, tabHeight1, tabHeight2, tabHeight3)

            videoLib.general = {
                'min-height': Math.max(minHeight, tabHeight1, tabHeight2, tabHeight3) + 'px'
            }
            videoLib.subscribed = {
                'min-height': Math.max(minHeight, tabHeight1, tabHeight2, tabHeight3) + 'px'
            }
            videoLib.uploaded = {
                'min-height': Math.max(minHeight, tabHeight1, tabHeight2, tabHeight3) + 'px'
            }

        }, 200);
    }

    tabHeightCalculate();

    //-------------------------DESIGN CALCULATION END-------------------------//
    videoLib.videos = 0;
    videoLib.status = 1;
    videoLib.showDivision = 1;

    videoLib.prevStatus = 1;
    //button to upload more videos
    videoLib.showUploadButton = false;
    //Whether search bar is opened or not
    videoLib.searchOpen = false;
    videoLib.searchOpenLate = false;
    videoLib.loadMoreViews = false;
    videoLib.menuDropped = false;
    videoLib.backdrop = false;
    videoLib.expandlist = false;
    videoLib.showReviewButton = false;
    videoLib.selectedRating = 3;
    var filepath = '';
    var fullpath = '';
    videoLib.toggle = false;
    videoLib.showPaid = false;
    videoLib.deltaShowPaid = false;
    videoLib.searchResults = [];
    videoLib.showSpinner = false;
    videoLib.youtube = false;
    videoLib.youtubeVideoLoaded = false;


    $timeout(function () {
        videoLib.loadMoreViews = true;
    }, 300);

    function checkPermissionCallback(status) {
        if (!status.hasPermission) {
            var errorCallback = function () {
                console.warn('storage permission is not turned on');
            }

            permissions.requestPermission(
                permissions.WRITE_EXTERNAL_STORAGE,
                function (status) {
                    if (!status.hasPermission) errorCallback();
                },
                errorCallback);
        }
    }

    if (window.cordova) {
        if ($rootScope.android) {
            var permissions = cordova.plugins.permissions;
            permissions.hasPermission(permissions.WRITE_EXTERNAL_STORAGE, checkPermissionCallback, null);
        }
    }


    $scope.$on('$ionicView.enter', function (e) {
        $ionicNavBarDelegate.showBar(true);
    });

    // videoLib.toggleLeft = function () {
    //     console.log('toggle left')
    //     $ionicSideMenuDelegate.toggleLeft();
    // };


    $ionicModal.fromTemplateUrl('templates/player/videofiltermodal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modal) {
        videoLib.modal = modal;
    });
    videoLib.showFilters = function () {
        videoLib.modal.show();
    }
    videoLib.hideFilters = function () {

        videoLib.deltaSortBy = videoLib.sortBy;
        videoLib.modal.hide();
        $ionicScrollDelegate.resize();
    }
    videoLib.playVideo = function (video) {
        $rootScope.showTabs = false;
        if (!loggedInUser.info.player) {

            $state.go('coachSidemenu.tabs.playVideo', {
                videoInfo: video,
                activeTab: videoLib.status,
                redirectTo: 'videoLib'
            });
        }
        else {
            $state.go('sidemenu.tabs.playVideo', {
                videoInfo: video,
                activeTab: videoLib.status,
                redirectTo: 'videoLib'
            });
        }
    }
    // videoLib.selectSport = function(){
    //     videoLib.sportSelected = true;
    // }

    videoLib.goTovideoLib = function () {
        $state.go('sidemenu.tabs.videoLib');
    }

    videoLib.changePrevStatus = function (number) {

        $ionicScrollDelegate.scrollTop();
        videoLib.showDivision = 0;

        videoLib.status = number;

        $timeout(function () {
            videoLib.showDivision = videoLib.status;
        }, 200);

        if (videoLib.status != videoLib.prevStatus) {
            videoLib.clearSearch();
        }
        switch (number) {
            case 1:
                console.log('general');
                break;
            case 2:
                console.log('review');
                break;
            case 3:
                videoLib.showSpinner = true;
                console.log('uploaded');
                Video.get({
                        userId: loggedInUser.info._id,
                        lastVideoFetchTime: videoLib.playerUploadedVideos.lastVideoFetchTime
                    },
                    function (userVideos) {

                        videoLib.playerUploadedVideos.lastVideoFetchTime = userVideos.lastVideoFetchTime
                        _.each(userVideos.videoList, function (vdo) {
                            if (typeof vdo.review != 'undefined') {
                                console.log(vdo.review)
                                var textReviewLength = vdo.review.review.length -
                                    _.reject(_.map(vdo.review.review, 'review'), function (rev) {
                                        return typeof rev == 'undefined'
                                    }).length;
                                var audioReviewLength = vdo.review.review.length -
                                    _.reject(_.map(vdo.review.review, 'audioUrl'), function (rev) {
                                        return typeof rev == 'undefined'
                                    }).length;
                                vdo.reviewStatus = textReviewLength < audioReviewLength ? textReviewLength : audioReviewLength;
                                console.log(vdo.reviewStatus);
                            }
                            if (!vdo.url.includes('youtube.com'))
                                vdo.thumbnail = backend + '/uploads/' + vdo.thumbnail;
                            videoLib.playerUploadedVideos.videosList.push(vdo);
                        })
                        videoLib.showSpinner = false;
                    }, function (error) {
                        alert(error);
                        videoLib.showSpinner = false;
                    })
                break;

        }
        $timeout(function () {
            videoLib.prevStatus = number;
        }, 500)
    }

    videoLib.showUpload = function () {
        if ($rootScope.sportSelected) {
            $timeout(function () {
                videoLib.showUploadButton = true;
            }, 200)
        }
    }
    videoLib.hideUpload = function () {
        videoLib.showUploadButton = false;
    }
    videoLib.showReview = function () {
        $timeout(function () {
            videoLib.showReviewButton = true;
        }, 200)
    }
    videoLib.hideReview = function () {
        videoLib.showReviewButton = false;
    }
    videoLib.showSearch = function () {
        videoLib.searchOpen = true;
        $timeout(function () {
            videoLib.searchOpenLate = true;
        }, 400)
    }

    videoLib.hideSearch = function () {
        videoLib.searchOpen = false;
        $timeout(function () {
            videoLib.searchOpenLate = false;
        }, 100)
    }

    videoLib.fetchedVideoList = fetchedVideoList.video;
    videoLib.subscribedVideoList = subscribedVideoList.video;
    videoLib.subscribedVideoPackage = subscribedVideoPackage.video;

    videoLib.selectSport = function (sport) {
        videoLib.showSpinner = true;
        var allSubscribedVideoIds = [];
        $rootScope.sportSelected = sport;
        videoLib.selectTags(sport);
        videoLib.backdrop = false;
        $timeout(function () {
            videoLib.menuDropped = false;
        }, 500)
        var queryObject = _.find(fetchedVideoInfo.info, function (video) {
            return video.sportId == sport._id;
        });
        queryObject.userId = loggedInUser.info._id;
        //It does not show videos uploaded by LoggedInUser
        Video.getVideoBySportId(queryObject, function (data) {
            _.each(fetchedVideoInfo.info, function (list) {
                if (list.sportId == data.sportId) {
                    list.lastVideoFetchTime = data.lastVideoFetchTime;
                    _.each(data.videoList, function (video) {
                        if (!video.url.includes('youtube.com'))
                            video.thumbnail = backend + '/uploads/' + video.thumbnail
                        videoLib.fetchedVideoList.push(video);
                        tabHeightCalculate();
                    })

                    videoLib.subscribedVideoPackage.length = 0;
                    _.each(Subscribed.Subscribed, function (subs) {
                        var package = [];
                        var packageBy;
                        var dataInserted = false;
                        _.each(videoLib.fetchedVideoList, function (video) {
                            if (subs == video.videoBy._id && video.sport == $rootScope.sportSelected._id) {
                                package.push(video);
                                packageBy = video.videoBy.firstName;
                                allSubscribedVideoIds.push(video._id);
                                dataInserted = true;
                            }
                        })

                        if (dataInserted)
                            videoLib.subscribedVideoPackage.push({'packageBy': packageBy, 'package': package});
                    })


                    videoLib.subscribedVideoList.length = 0;
                    _.each(myBookmarkedVideo.bookmarkedVideos, function (book) {
                        _.each(videoLib.fetchedVideoList, function (video) {

                            //also checking whether the video is inside any package or not,
                            //if it is, do not include it in bookmarks list
                            if (book == video._id && _.indexOf(allSubscribedVideoIds, book) == -1) {
                                videoLib.subscribedVideoList.push(video);
                            }
                        })
                    })
                }
            })
            videoLib.showSpinner = false;

        }, function (err) {
            console.log(err);
            alert(err);
            videoLib.showSpinner = false;
        })


        if (!videoLib.showUploadButton && videoLib.status == 3) {
            videoLib.showUploadButton = true;
        }
    }

    videoLib.dropMenu = function () {
        videoLib.backdrop = true;
        $timeout(function () {
            videoLib.menuDropped = true;
        }, 100)
    }
    videoLib.hideMenu = function () {
        videoLib.backdrop = false;
        $timeout(function () {
            videoLib.menuDropped = false;
        }, 500)
    }
    videoLib.expand = function () {
        videoLib.expandlist = true;
    }
    videoLib.collapse = function () {
        videoLib.expandlist = false;
    }
    videoLib.selectRating = function (rating) {
        videoLib.selectedRating = rating;
    }
    videoLib.playReviewVideo = function () {
        $rootScope.showTabs = false;
        $state.go('coachSidemenu.tabs.playReviewVideo');
    }

    videoLib.addPackage = function () {
        var options =
        {
            title: 'Add Video Package',
            subTitle: 'Table-Tennis',
            cssClass: 'sportsPickPopup', // String, The custom CSS class name
            template: '<div add-package></div>'
        }
        packageAlertPopup = $ionicPopup.alert(options);

        IonicClosePopupService.register(packageAlertPopup);

    }

    videoLib.goToReview = function () {
        $state.go('coachSidemenu.tabs.reviewVideos')
    }
    videoLib.selectForReview = function () {
        $state.go('sidemenu.selectForReview')
    }


    //==================================================================================
    //==============================PLAYER's ACTIONS====================================

    videoLib.mySports = mySports.sports;
    videoLib.playerUploadedVideos = playerUploadedVideos;


    //==================================================================================
    //==============================Upload Player Video's ACTIONS====================================


    videoLib.selectVideo = function () {
        var options =
        {
            title: 'Choose a Video',
            cssClass: 'sportsPickPopup', // String, The custom CSS class name
            scope: $scope,
            template: '<div pick-video></div>',
        }
        videoAlertPopup = $ionicPopup.alert(options);

        IonicClosePopupService.register(videoAlertPopup);

    }
    var deltaSportSelected;
    videoLib.uploadVideo = function () {
        videoLib.youtube = false;
        videoLib.youtubeVideoLoaded = false;
        videoLib.youtubeLink = '';
        videoLib.videoInfo = {
            refToTags: [],
            title: '',
            url: '',
            sport: $rootScope.sportSelected._id,
            paidVideo: true,
            videoBy: loggedInUser.info._id,
            accountType: loggedInUser.info.player ? 'player' : 'coach',
            description: ''
        }
        videoLib.uploadVideoInfo = {};

        videoLib.mySportsListUpload = [];
        _.each(videoLib.mySports, function (sport) {
            if ($rootScope.sportSelected._id == sport._id)
                sport.selected = true;
            else
                sport.selected = false;
            videoLib.mySportsListUpload.push(sport)
        })
        videoLib.selectTags($rootScope.sportSelected)
        videoLib.selectVideo();

        deltaSportSelected=$rootScope.sportSelected;
    };

    videoLib.changeVideoType = function () {
        videoLib.videoInfo.paidVideo = !videoLib.videoInfo.paidVideo;
    }

    function successThumbnail(thumbnail) {
        var promise = uploadThumbnail.uploadImage(thumbnail);
        promise.then(function (thumbnail) {
            videoLib.videoInfo.thumbnail = thumbnail;
        })
    }

    function errorThumbnail(error) {
        console.log(error)
    }


    function successInfo(data) {
        videoLib.videoInfo.duration = data.duration;

        VideoEditor.createThumbnail(
            successThumbnail, // success cb
            errorThumbnail, // error cb
            {
                fileUri: fullpath, // the path to the video on the device
                outputFileName: 'output-name', // the file name for the JPEG image
                atTime: data.duration / 2, // optional, location in the video to create the thumbnail (in seconds)
                width: 100, // optional, width of the thumbnail
                height: 100, // optional, height of the thumbnail
                quality: 100 // optional, quality of the thumbnail (between 1 and 100)
            }
        );
    }

    function errorInfo(data) {
        console.log(data);
    }


    videoLib.getVideoFromGallery = function () {
        videoAlertPopup.close();
        navigator.camera.getPicture(onSuccess, onFail, {
            quality: 50,
            destinationType: Camera.DestinationType.FILE_URI,
            sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
            mediaType: Camera.MediaType.VIDEO
        });

        function onSuccess(videoPath) {
            fullpath = videoPath;
            filepath = videoPath;
            VideoEditor.getVideoInfo(
                successInfo, // success cb
                errorInfo, // error cb
                {
                    fileUri: 'file:' + fullpath, // the path to the video on the device
                }
            );


            videoLib.allLevelTagsList = allLevelTagsList.list;

            videoLib.showLevel2Div = false;
            videoLib.showLevel3Div = false;
            videoLib.deltaSelectedTags = [];
            videoLib.deltaSelectedTagsIds = {level1: [], level2: [], level3: []};
            // $rootScope.showTagsDirective=true;
            uploadvideomodal.show();


            $timeout(function () {
                var player = jwplayer("videoPlayerdivModal").setup({
                    "file": videoPath,
                    "image": "http://example.com/myImage.png",
                    "height": 360 * (devWidth - 40) / 640,
                    "width": devWidth - 40
                });

                jwplayer().on('play', function (e) {
                    //call increase view api here
                    playVideo.viewThisVideo(playVideo.videoInfo._id);
                });
                if (document.cookie.indexOf("jwplayerAutoStart") == -1) {
                    document.cookie = "jwplayerAutoStart=1";
                    player.on('ready', function () {
                        player.play();
                    });
                }
                else {
                }
            }, 200)
        }

        function onFail(message) {
            alert('Failed because: ' + message);
        }
    }

    $ionicModal.fromTemplateUrl('templates/player/uploadVideoModal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    })
        .then(function (modal) {
            uploadvideomodal = modal;
            console.log('upload videos modal');
        });


    // capture callback
    var captureSuccess = function (mediaFiles) {
        var i, len;
        for (i = 0, len = mediaFiles.length; i < len; i += 1) {
            filepath = mediaFiles[i].localURL;
            fullpath = mediaFiles[0].fullPath

            // do something interesting with the file
        }


        VideoEditor.getVideoInfo(
            successInfo, // success cb
            errorInfo, // error cb
            {
                fileUri: fullpath, // the path to the video on the device
            }
        );

        videoLib.allLevelTagsList = allLevelTagsList.list;

        videoLib.showLevel2Div = false;
        videoLib.showLevel3Div = false;
        videoLib.deltaSelectedTags = [];
        videoLib.deltaSelectedTagsIds = {level1: [], level2: [], level3: []};
        // $rootScope.showTagsDirective=true;
        uploadvideomodal.show();
        $timeout(function () {
            var player = jwplayer("videoPlayerdivModal").setup({
                "file": mediaFiles[0].fullPath,
                "image": "http://example.com/myImage.png",
                "height": 360 * (devWidth - 40) / 640,
                "width": devWidth - 40
            });

            jwplayer().on('play', function (e) {
                //call increase view api here
                playVideo.viewThisVideo(playVideo.videoInfo._id);
            });
            if (document.cookie.indexOf("jwplayerAutoStart") == -1) {
                document.cookie = "jwplayerAutoStart=1";
                player.on('ready', function () {
                    player.play();
                });
            }
            else {
            }
        }, 200)
        // var v = "<video controls='controls'>";
        // v += "<source src='" + mediaFiles[0].localURL + "' type='video/mp4'>";
        // v += "</video>";
        // document.querySelector("#videoArea").innerHTML = v;
    };

    // capture error callback
    var captureError = function (error) {
        navigator.notification.alert('Error code: ' + error.code, null, 'Capture Error');
    };

    videoLib.recordVideo = function () {
        videoAlertPopup.close();
        // start video capture
        navigator.device.capture.captureVideo(captureSuccess, captureError, {limit: 1});
    }
    videoLib.fromYoutube = function () {
        videoAlertPopup.close();
        videoLib.youtube = true;
        // start video capture
        videoLib.showLevel2Div = false;
        videoLib.showLevel3Div = false;
        videoLib.deltaSelectedTags = [];
        videoLib.deltaSelectedTagsIds = {level1: [], level2: [], level3: []};
        // $rootScope.showTagsDirective=true;
        uploadvideomodal.show();
    }
    videoLib.trustSrc = function (src) {
        console.log(src)
        return $sce.trustAsResourceUrl(src);
    }
    videoLib.loadYoutubeVideo = function () {
        videoLib.untrustedLink = videoLib.youtubeLink + '';

        if (videoLib.untrustedLink.indexOf('watch?v=') != -1 && videoLib.untrustedLink.indexOf('&') != -1) {
            var video_id = videoLib.untrustedLink.substring(videoLib.untrustedLink.lastIndexOf('watch?v=') + 8, videoLib.untrustedLink.indexOf('&'));
        }
        else if
        (videoLib.untrustedLink.indexOf('watch?v=') != -1) {
            var video_id = videoLib.untrustedLink.substring(videoLib.untrustedLink.lastIndexOf('watch?v=') + 8, videoLib.untrustedLink.length);
        }
        else {
            var video_id = videoLib.untrustedLink.substring(videoLib.untrustedLink.lastIndexOf('/') + 1, videoLib.untrustedLink.length);
        }

        videoLib.videoInfo.thumbnail = 'http://img.youtube.com/vi/' + video_id + '/3.jpg';
        videoLib.untrustedLink = 'https://www.youtube.com/embed/' + video_id;
        videoLib.trustedLink = videoLib.trustSrc(videoLib.untrustedLink + '?showinfo=0');
        $timeout(function () {
            videoLib.youtubeVideoLoaded = true;
        }, 200)
    }
    videoLib.uploadYoutubeVideo = function () {
        videoLib.videoInfo.url = videoLib.untrustedLink;

        var videoId = videoLib.videoInfo.url.substring((videoLib.videoInfo.url.lastIndexOf('/') + 1))
        var promise = ConvertYoutubeDuration.convertTime(videoId);
        promise.then(function (duration) {
            videoLib.videoInfo.duration = duration;
        }).then(function () {


            videoLib.titleError = false;
            videoLib.descriptionError = false;
            console.log('uploading youtube video to AWS');

            if (videoLib.videoInfo.title != '' && videoLib.videoInfo.description != '') {
                $ionicLoading.show({'template': 'Uploading Video...'});
                Video.save({videoInfo: videoLib.videoInfo}, function (data) {
                    // deferred.resolve(data);
                    $ionicLoading.hide();
                    if (window.cordova) {
                        $cordovaToast.showShortBottom('Video Uploaded Successfully')
                    }
                    else {
                        alert('Video Uploaded Successfully');
                    }

                    videoLib.hideUploadVideoModal();
                    var videoObject = {
                        _id: data._id,
                        accountType: data.accountType,
                        createdAt: data.createdAt,
                        description: data.description,
                        dislikes: data.dislikes,
                        likes: data.likes,
                        numberOfViews: data.numberOfViews,
                        paidVideo: data.paidVideo,
                        refToFeedback: data.refToFeedback,
                        refToPackage: data.refToPackage,
                        refToTags: data.refToTags,
                        sport: data.sport,
                        subscribers: data.subscribers,
                        title: data.title,
                        updatedAt: data.updatedAt,
                        url: data.url,
                        duration: data.duration,
                        videoBy: loggedInUser.info,

                    };

                    if (!videoObject.url.includes('youtube.com')) {
                        videoObject.thumbnail = backend + '/uploads/' + data.thumbnail
                    }
                    else {
                        videoObject.thumbnail = data.thumbnail
                    }

                    videoLib.playerUploadedVideos.videosList.push(videoObject);
                }, function (err) {
                    console.log(err);
                });

            }
            else {
                if (videoLib.videoInfo.title == '')
                    videoLib.titleError = true;
                if (videoLib.videoInfo.description == '')
                    videoLib.descriptionError = true;

            }

        })

    }
    videoLib.uploadThisVideo = function () {
        console.log('uploading video to AWS S3')
        $ionicLoading.show({'template': '<div> Uploading {{percentageUploaded|number:0}}%</div>'});
        var test = uploadService.uploadImage(filepath, videoLib.videoInfo);
        test.then(function (data) {
            $ionicLoading.hide();
            if(window.cordova) {
                $cordovaToast.showShortBottom('Video Uploaded Successfully');
            }
            else{
                alert('Video Uploaded Successfully');
            }
            videoLib.hideUploadVideoModal();
            var videoObject = {
                _id: data._id,
                accountType: data.accountType,
                createdAt: data.createdAt,
                description: data.description,
                dislikes: data.dislikes,
                likes: data.likes,
                numberOfViews: data.numberOfViews,
                paidVideo: data.paidVideo,
                refToFeedback: data.refToFeedback,
                refToPackage: data.refToPackage,
                refToTags: data.refToTags,
                sport: data.sport,
                subscribers: data.subscribers,
                title: data.title,
                updatedAt: data.updatedAt,
                url: data.url,
                duration: data.duration,
                videoBy: loggedInUser.info
            };

            if (!videoObject.url.includes('youtube.com')) {
                videoObject.thumbnail = backend + '/uploads/' + data.thumbnail
            }
            else {
                videoObject.thumbnail = data.thumbnail
            }

            videoLib.playerUploadedVideos.videosList.push(videoObject);
        });
    }


    videoLib.selectTags = function (sport) {
        videoLib.selectedVideoTags = [];
        videoLib.selectedVideoTags = _.filter(tagList, function (tags) {
            tags.selected = false;
            return tags.sportId == sport._id
        })
    }

    videoLib.hideUploadVideoModal = function () {
        $rootScope.showTagsDirective = false;
        $rootScope.sportSelected=deltaSportSelected;
        uploadvideomodal.hide();
    }
    videoLib.addTags = function (tag) {
        tag.selected = !tag.selected;
        if (tag.selected)
            videoLib.videoInfo.refToTags.push(tag._id)
        else
            videoLib.videoInfo.refToTags = _.reject(videoLib.videoInfo.refToTags, function (myTag) {
                myTag == tag._id;
            })
    }

    videoLib.changeSport = function (sport) {
        videoLib.videoInfo.sport = sport._id;
        $rootScope.sportSelected = sport;
        _.each(videoLib.mySportsListUpload, function (mySport) {
            if (sport._id == mySport._id)
                mySport.selected = true;
            else
                mySport.selected = false;

        })
        videoLib.selectTags(sport)
    }

    videoLib.sortBy = -'numberOfViews';
    videoLib.deltaSortBy = -'numberOfViews';
    videoLib.sortByFunction = function (sortBy) {
        videoLib.deltaSortBy = sortBy;

    }

    videoLib.showFilterLevel2Div = false;
    videoLib.showFilterLevel3Div = false;
    videoLib.deltaFilterTags = [];
    videoLib.deltaFilterTagsIds = {level1: [], level2: [], level3: []};
    videoLib.deltaAppliedFilter = [];
    videoLib.tagsFilter = [];

    videoLib.filterByTags = function (tags) {
        tags.selected = !tags.selected;

        if (tags.selected)
            videoLib.deltaTagsFilter.push(tags._id)
        else
            videoLib.deltaTagsFilter = _.reject(videoLib.deltaTagsFilter, function (tagId) {
                return tagId == tags._id;
            })
    }

    videoLib.applyFilter = function () {
        console.log(videoLib.deltaAppliedFilter)
        videoLib.sortBy = videoLib.deltaSortBy;
        videoLib.appliedFilter = videoLib.deltaAppliedFilter;
        videoLib.modal.hide();
        console.log(videoLib.appliedFilter)
        videoLib.showPaid = videoLib.deltaShowPaid;

        $ionicScrollDelegate.resize();
    }


    if ($stateParams.activeTab != '') {
        videoLib.status = $stateParams.activeTab;
        videoLib.showDivision = $stateParams.activeTab;
        videoLib.selectSport($rootScope.sportSelected);//for updating the videolist with latest changes
    }

    videoLib.clearSearch = function () {
        videoLib.searchResults = [];
        videoLib.searchTitle = '';
        videoLib.showSpinner = false;
    }

    videoLib.search = function (title) {
        videoLib.showSpinner = true;
        if (videoLib.searchResults.length != 0) {
            videoLib.clearSearch();
        }
        else {
            if (title.length > 2) {
                Video.searchVideoByTitle({title: title, sportId: $rootScope.sportSelected._id}, function (data) {
                        switch (videoLib.status) {
                            case 1:
                                videoLib.searchResults = data;
                                if (videoLib.searchResults.length == 0) {
                                    if(window.cordova) {
                                        $cordovaToast.showShortBottom('No results found');
                                    }
                                    else{
                                        alert('No results found');
                                    }
                                }
                                break;
                            case 2:
                                videoLib.allBookmarkedVideos = [];
                                _.each(videoLib.subscribedVideoPackage, function (item) {
                                    _.each(item.package, function (video) {

                                        if (!video.url.includes('youtube.com')) {
                                            video.thumbnail = backend + '/uploads/' + video.thumbnail
                                        }
                                        videoLib.allBookmarkedVideos.push(video);
                                    })
                                })
                                _.each(videoLib.subscribedVideoList, function (video) {
                                    if (!video.url.includes('youtube.com')) {
                                        video.thumbnail = backend + '/uploads/' + video.thumbnail
                                    }

                                    videoLib.allBookmarkedVideos.push(video);
                                })

                                videoLib.searchResults = _.intersectionBy(data, videoLib.allBookmarkedVideos, '_id');
                                if (videoLib.searchResults.length == 0) {
                                    if(window.cordova) {
                                        $cordovaToast.showShortBottom('No results found');
                                    }
                                    else{
                                        alert('No results found');
                                    }
                                }
                                break;
                            case 3:
                                videoLib.searchResults = _.intersectionBy(data, videoLib.playerUploadedVideos.videosList, '_id');
                                if (videoLib.searchResults.length == 0) {
                                    if(window.cordova) {
                                        $cordovaToast.showShortBottom('No results found');
                                    }
                                    else{
                                        alert('No results found');
                                    }               
                                }
                                break;
                        }

                    },
                    function (err) {
                        console.log(err);
                    })
                videoLib.showSpinner = false;
            }
            else {
                alert('minimum 3 character required');
                videoLib.showSpinner = false;
            }
        }
    }


    videoLib.selectCoachForReview = function (video) {
        if (typeof video.review == 'undefined' || video.review.length < 2) {
            $scope.coachList = [];
            var getCoachPromise = Review.getAllCoachesForReview({sportId: $rootScope.sportSelected._id}, function (data) {
                _.each(data, function (coach) {
                    $scope.coachList.push({coach: coach, checkBoxVal: false});
                })

            }, function (err) {
                console.log(err);
            })


            $scope.videoForReview = video;

            var options =
            {
                title: 'Select coach for Review',
                subTitle: 'Only one coach',
                cssClass: 'sportsPickPopup', // String, The custom CSS class name
                template: '<send-for-review video-for-review="videoForReview" coach-list="coachList"></send-for-review>',
                scope: $scope
            }
            getCoachPromise['$promise'].then(function () {

                reviewAlertPopup = $ionicPopup.alert(options);

                IonicClosePopupService.register(reviewAlertPopup);
            })

        }
        else {
            console.log('only 2 review can be added');
            if(window.cordova) {
                $cordovaToast.showShortBottom('only 2 coaches can review a video');
            }
            else{
                alert('only 2 coaches can review a video');
            }
        }
    }


}
