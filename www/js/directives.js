angular.module('starter')
    .directive('pickSport', function ($state, $ionicActionSheet, $rootScope) {
        return {
            templateUrl: 'templates/player/choose-a-sport.html',
            link: function (scope) {
                scope.afterSportChosen = function () {
                    $rootScope.sportSelected = true;
                    alertPopup.close();
                }
            }
        };
    })

    .directive('pickImage', function ($state, $ionicActionSheet, $rootScope) {
        return {
            templateUrl: 'templates/player/choose-an-image.html',
            link: function (scope) {
                scope.afterImageChosen = function () {
                    $rootScope.imageSelected = true;
                    imageAlertPopup.close();
                }
            }
        };
    })

    .directive('pickVideo', function ($state, $ionicActionSheet, $rootScope) {
        return {
            templateUrl: 'templates/player/choose-a-video.html',
            link: function (scope) {
                var filepath;
                scope.afterVideoChosen = function () {
                    $rootScope.videoSelected = true;
                    videoAlertPopup.close();
                    uploadvideomodal.show();
                }
            }
        };
    })

    .directive('skipscope', function ($state, $ionicActionSheet, $rootScope) {
        return {
            templateUrl: 'templates/player/skip-scope.html',
            link: function (scope) {
                scope.closePopup = function () {
                    skipPopup.close();
                }
                scope.goToHome = function () {
                    $state.go('sidemenu.tabs.videoLib');
                }

            }
        };
    })

    .directive('enterTournament', function (loggedInUser, $rootScope, Tournament,myStats) {
        return {
            templateUrl: 'templates/player/enter-a-tournament.html',
            link: function (scope) {
                scope.afterTourAdded = function () {
                    tournamentAlertPopup.close();
                }
                scope.tournament = {};

                scope.addTournament = function () {
                    console.log(scope.tournament)
                    console.log($rootScope.sportSelected)
                    scope.tournament.sportId = $rootScope.sportSelected._id;
                    scope.tournament.userId = loggedInUser.info._id;
                    if (!checkValidTournament(scope.tournament)) {
                        Tournament.save({tournament: scope.tournament}, function (result) {
                            console.log(result);
                            myStats.list.tournamentList.push(result);
                            scope.tournament = {};
                            scope.afterTourAdded();
                        }, function (err) {
                            console.log(err);
                        })
                    }
                }

                function checkValidTournament(tournament) {
                    scope.tournamentTitleError = false;
                    scope.tournamentRoleError = false;
                    scope.tournamentDateError = false;
                    scope.tournamentTeamError = false;
                    scope.tournamentLocationError = false;
                    var errorOccured = false;

                    if (typeof tournament.name == 'undefined') {
                        scope.tournamentTitleError = true;
                        errorOccured = true;
                    }

                    if (typeof tournament.date == 'undefined') {
                        scope.tournamentDateError = true;
                        errorOccured = true;
                    }

                    if (typeof tournament.role == 'undefined') {
                        scope.tournamentRoleError = true;
                        errorOccured = true;
                    }

                    if (typeof tournament.team == 'undefined') {
                        scope.tournamentTeamError = true;
                        errorOccured = true;
                    }

                    if (typeof tournament.location == 'undefined') {
                        scope.tournamentLocationError = true;
                        errorOccured = true;
                    }
                    return errorOccured;
                }
            }
        };
    })

    .directive('addPackage', function (Package) {
        return {
            restrict: 'E',
            scope: {
                packageInfo: '=',
                packageList: '='
            },
            templateUrl: 'templates/coach/add-a-package.html',
            link: function (scope) {
                var selectedTags = [];
                scope.addPackage = function () {

                    scope.packageInfo.tags = selectedTags;
                    Package.save({packageInfo: scope.packageInfo}, function (data) {
                        scope.packageList.push(data);
                    }, function (err) {
                        console.log(err);
                    })
                    packageAlertPopup.close();
                }

                scope.addTag = function (tag) {
                    if (_.indexOf(selectedTags, tag._id) == -1) {
                        selectedTags.push(tag._id);
                        tag.selected = true;
                    }
                    else {
                        selectedTags.splice(_.indexOf(selectedTags, tag._id), 1);
                        tag.selected = false;
                    }
                }
            }
        };
    })

    .directive('sendForReview', function (Review, $cordovaToast) {
        return {
            restrict: 'E',
            templateUrl: 'templates/coach/send-for-review.html',
            scope: {
                videoForReview: '=',
                coachList: '='
            },
            link: function (scope) {
                var coachId = [];
                scope.videoSent = function () {
                    for (var i = 0; i < coachId.length; i++)//i<2 as only 2 review are allowed
                    {
                        Review.submitForReview({
                            review: {
                                videoId: scope.videoForReview._id,
                                videoBy: scope.videoForReview.videoBy._id,
                                coachId: coachId[i]
                            }
                        }, function (data) {
                            if (typeof scope.videoForReview.reviewStatus == 'undefined')
                                scope.videoForReview.reviewStatus = 1;
                            else if (scope.videoForReview.reviewStatus = 1)
                                scope.videoForReview.reviewStatus = 2;
                            if (typeof scope.videoForReview.review == 'undefined') {
                                scope.videoForReview.review = [];
                            }

                            scope.videoForReview.review.push({
                                videoId: scope.videoForReview._id,
                                videoBy: scope.videoForReview.videoBy._id,
                                coachId: coachId[i]
                            })
                        }, function (err) {
                            console.log(err);
                        })
                    }

                    reviewAlertPopup.close();
                }

                scope.selectCoachForReview = function (coach) {
                    if (_.indexOf(coachId, coach.coach._id) == -1) {
                        if (coachId.length < 2) {
                            coachId.push(coach.coach._id);
                        }
                        else {
                            coach.checkBoxVal = false;
                            if(window.cordova){
                                $cordovaToast.showShortBottom('Only 2 coach can be selected');
                            }
                            else{
                                alert('Only 2 coach can be selected');
                            }
                        }
                    }
                    else {
                        coachId.splice(_.indexOf(coachId, coach.coach._id), 1);
                    }
                }

            }
        };
    })

    .directive('addAcademy', function ($rootScope,loggedInUser,Academy,myStats) {
        return {
            templateUrl: 'templates/player/add-an-academy.html',
            link: function (scope) {
                scope.afterAcademyAdd = function () {
                    academyAlertPopup.close();
                }
                scope.academy={};

                scope.addAcademy = function () {
                    scope.academy.sportId = $rootScope.sportSelected._id;
                    scope.academy.userId = loggedInUser.info._id;
                    if (!checkValidAcademy(scope.academy)) {

                        Academy.save({academy:scope.academy},function(data)
                        {
                            console.log(data);

                            myStats.list.tournamentList.push(data);
                            scope.academy = {};
                            scope.afterAcademyAdd();

                        },function(err)
                        {
                            console.log(err);
                        })
                    }
                }


                function checkValidAcademy(academy) {
                    scope.academyNameError = false;
                    scope.academyFromError = false;
                    scope.academyToError = false;
                    scope.academyLocationError = false;
                    var errorOccured = false;
                    if (typeof academy.name == 'undefined') {
                        scope.academyNameError = true;
                        errorOccured = true;
                    }

                    if (typeof academy.from == 'undefined') {
                        scope.academyFromError = true;
                        errorOccured = true;
                    }

                    if (typeof academy.to == 'undefined') {
                        scope.academyToError = true;
                        errorOccured = true;
                    }

                    if (typeof academy.location == 'undefined') {
                        scope.academyLocationError = true;
                        errorOccured = true;
                    }
                    return errorOccured;
                }

            }
        };
    })

    .directive('askQuestion', function () {
        return {
            templateUrl: 'templates/player/ask-a-question.html',
            link: function (scope) {
                scope.afterQuestionSubmit = function () {
                    questionAlertPopup.close();
                }
            }
        };
    })

    .directive('hideTabs', function ($rootScope) {
        return {
            restrict: 'A',
            link: function ($scope, $el) {
                $rootScope.hideTabs = 'tabs-item-hide';
                $scope.$on('$destroy', function () {
                    $rootScope.hideTabs = '';
                });
            }
        };
    })
    .directive('tagsName', function (allLevelTagsList) {
        return {
            templateUrl: 'templates/player/tags-name.html',
            scope: {
                tagsLevel1: '=',
                tagsLevel2: '=',
                tagsLevel3: '=',
                pageType: '@'
            },
            link: function (scope) {
                var tagLevel2Id = [];
                var tagLevel1Id = [];
                console.log(scope.pageType)
                scope.tagsLevel3Name = _.map(_.filter(allLevelTagsList.list.level3Tags, function (tags) {
                    return _.indexOf(scope.tagsLevel3, tags._id) != -1;
                }), 'tagLevel3');

                tagLevel2Id = _.uniq(_.map(_.filter(allLevelTagsList.list.level3Tags, function (tags) {
                    return _.indexOf(scope.tagsLevel3, tags._id) != -1;
                }), 'tagLevel2Id'));


                scope.tagsLevel2Name = _.map(_.filter(allLevelTagsList.list.level2Tags, function (tags) {
                    return (_.indexOf(scope.tagsLevel2, tags._id) != -1 && _.indexOf(tagLevel2Id, tags._id) == -1 );
                }), 'tagLevel2');

                tagLevel1Id = _.uniq(_.map(_.filter(allLevelTagsList.list.level2Tags, function (tags) {
                    return (_.indexOf(scope.tagsLevel2, tags._id) != -1);
                }), 'tagLevel1Id'));

                scope.tagsLevel1Name = _.map(_.filter(allLevelTagsList.list.level1Tags, function (tags) {
                    return _.indexOf(scope.tagsLevel1, tags._id) != -1 && _.indexOf(tagLevel1Id, tags._id) == -1;
                }), 'tagLevel1');
            }
        };
    })
    .directive('ratingDirective', function () {
        return {
            templateUrl: 'templates/player/ratingDirective.html',
            scope: {
                averageRating: '='
            },
            link: function (scope) {
                console.log(scope.averageRating)
            }
        };
    });
