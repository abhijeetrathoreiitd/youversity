angular.module('starter')
    .directive('feedbackDirective', function ($window, $state, $ionicModal, $rootScope, loggedInUser, $timeout, AppReview) {
        return {
            restrict: 'A',
            template: '',
            scope: {},
            link: function (scope, elem) {
                elem.bind('click', showFeedbackModal);
                scope.data = {};
                scope.data.feedback = '';
                scope.disableName = false;


                var devHeight = $window.innerHeight;
                var devWidth = $window.innerWidth;
                scope.scrollHeight = {'height': devHeight - (9 * devWidth / 16) + 'px'};


                scope.hideFeedbackModal = function () {

                    console.log('close feedback modal');
                    scope.sendFeedbackModal.hide();
                    $timeout(function () {
                        $rootScope.showfeedbackDirective = true;
                    }, 100);
                };
                scope.sendFeedback = function () {
                    scope.feedbackIsRequired = false;
                    scope.nameIsRequired = false;

                    if (scope.data.feedback == '') {
                        scope.feedbackIsRequired = true;
                        return;
                    }
                    if (scope.data.username == '') {
                        scope.nameIsRequired = true;
                        return;
                    }
                    console.log(scope.data.feedback);
                    console.log(scope.data.username);


                    var reviewObject = {
                        email: typeof loggedInUser.info != 'undefined' ? loggedInUser.info.email : 'guest',
                        name: scope.data.username,
                        state: $state.current.name,
                        feedback: scope.data.feedback
                    }


                    AppReview.save(reviewObject, function (result) {
                            console.log(result);
                            scope.data.feedback = '';
                            scope.hideFeedbackModal();
                        }, function (err) {
                            console.log(err);
                        }
                    )
                }

                function showFeedbackModal() {
                    $ionicModal.fromTemplateUrl('templates/player/sendFeedbackModal.html', {
                        scope: scope,
                        animation: 'slide-in-up'
                    })
                        .then(function (modal) {
                            scope.sendFeedbackModal = modal;
                            scope.sendFeedbackModal.show();
                            // $rootScope.showfeedbackDirective=false;
                            if (loggedInUser.info) {
                                console.log('logged in user',loggedInUser.info.firstName);
                                scope.data.username = loggedInUser.info.firstName;
                                scope.disableName = true;
                            }
                            else {
                                console.log('no user');
                                scope.data.username = '';
                                scope.disableName = false;
                            }
                        });
                };

            }
        };
    })




















