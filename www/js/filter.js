/**
 * Created by youstart on 08-Sep-16.
 */

angular.module('starter')
    .filter('capitalize', function () {
        return function (input, all) {
            return (!!input) ? input.replace(/([^\W_]+[^\s-]*) */g, function (txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            }) : '';
        }
    })
    .filter('likeVideoFilter', function (playerVideoStats) {
        return function (input) {
            if (_.indexOf(playerVideoStats.likedVideos, input) == -1)

                return false;
            else
                return true;
        }
    })
    .filter('dislikeVideoFilter', function (playerVideoStats) {
        return function (input) {
            if (_.indexOf(playerVideoStats.dislikedVideos, input) == -1)

                return false;
            else
                return true;
        }
    })
    .filter('viewVideoFilter', function (playerVideoStats) {
        return function (input) {
            if (_.indexOf(playerVideoStats.viewedVideos, input) == -1)
                return true;
            else
                return false;
        }
    })
    .filter('subscribedVideoFilter', function (Subscribed) {
        return function (input) {
            return input.filter(function (video) {
                if (_.indexOf(Subscribed.Subscribed, video.videoBy._id) == -1) {
                    return false;
                }
                else {
                    return true;
                }
            })
        }
    })
    .filter('bookmarkVideoFilter', function (myBookmarkedVideos) {

        return function (input) {
            return input.filter(function (video) {
                if (_.indexOf(myBookmarkedVideos.bookmarkedVideos, video) == -1) {
                    return false;
                }
                else {
                    return true;
                }
            })
        }

    })
    .filter('filterDataBySportId', function () {

        return function (videoArray, sportId) {
            if (videoArray) {
                return videoArray.filter(function (video) {
                    if (video.sport == sportId || video.sportId == sportId)
                        return true;
                    else
                        return false;
                })
            }
        }

    })
    .filter('filterExperienceBySportId', function () {

        return function (input, sportId) {
            if (input) {
                return input.filter(function (exp) {
                     if(exp.sportId == sportId)
                         return true;
                    else return false;
                })
            }
        }

    }) .filter('experienceMapping', function (sportsExperience) {

    return function (input) {
        if (input) {
            return  _.find(sportsExperience.list,function(sportExp)
            {
                return sportExp.experience==input;
            }).title
            
        }
    }

})
    .filter('searchVideo', function () {
        return function (videoArray, videoId) {
            if (videoId)
                return videoArray.filter(function (video) {
                    if (video._id == videoId) {
                        return true
                    }
                    else return false;
                })
            else
                return videoArray;
        }

    })
    .filter('filterVideoByTags', function () {
        return function (videoArray, tagsArray) {
            if (tagsArray.length != 0) {
                if (videoArray) {
                    return videoArray.filter(function (video) {
                        if (_.difference(video.refToTags, tagsArray).length < video.refToTags.length)
                            return true;
                        else return false;
                    })
                }
            }
            else
                return videoArray;
        }


    })
    .filter('filterVideoByLevel1Tags', function () {
        return function (videoArray, tagsLevel1) {
            if (typeof tagsLevel1!='undefined' && tagsLevel1.length != 0) {
                if (videoArray) {
                    return videoArray.filter(function (video) {
                        if (typeof video.tagsLevel1!='undefined' && _.difference(video.tagsLevel1, tagsLevel1).length < video.tagsLevel1.length)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    })
                }
            }
            else {
                return videoArray;
            }
        }


    })
    .filter('filterVideoByLevel2Tags', function () {
        return function (videoArray, tagsLevel2) {
            if (typeof tagsLevel2!='undefined' && tagsLevel2.length != 0) {
                if (videoArray) {
                    return videoArray.filter(function (video) {
                        if (typeof video.tagsLevel2!='undefined' &&_.difference(video.tagsLevel2, tagsLevel2).length < video.tagsLevel2.length)
                            return true;
                        else return false;
                    })
                }
            }
            else
                return videoArray;
        }


    })
    .filter('filterVideoByLevel3Tags', function () {
        return function (videoArray, tagsLevel3) {
            if (typeof tagsLevel3!='undefined' && tagsLevel3.length != 0) {
                if (videoArray) {
                    return videoArray.filter(function (video) {
                        if (typeof video.tagsLevel3!='undefined' &&_.difference(video.tagsLevel3, tagsLevel3).length < video.tagsLevel3.length)
                            return true;
                        else return false;
                    })
                }
            }
            else
                return videoArray;
        }


    })
    .filter('filterByCost', function () {

        return function (array, costFilter) {
            if (array) {
                return array.filter(function (item) {
                    if (item.cost <= costFilter)
                        return true;
                    else
                        return false;
                })
            }
        }

    })
    .filter('filterPaidVideos', function () {

        return function (array, filter) {
            if (array) {
                return array.filter(function (item) {
                    if (filter && item.paidVideo)
                        return true;
                    else if (filter && !item.paidVideo)
                        return false;

                    else
                        return item;
                })
            }
        }

    })
    .filter('filterByDuration', function () {

        return function (array, durationFilter) {
            if (array) {
                return array.filter(function (item) {
                    if (item.duration <= durationFilter)
                        return true;
                    else
                        return false;
                })
            }
        }
    })
    .filter('filterByRating', function () {

    return function (array, ratingFilter) {
        if (array ) {
            return array.filter(function (item) {
                if (item.rating <= ratingFilter)
                    return true;
                else if (typeof item.rating=='undefined')
                    return item
                else
                    return false;
            })
        }
    }
})
    .filter('secondsToHMS', function () {
    return function (duration) {
        if (duration) {
            var h = Math.floor(duration / 3600);
            var m = Math.floor(duration % 3600 / 60);
            var s = Math.floor(duration % 3600 % 60);
            if (h > 0) {
                return h + ':' + m + ':' + s
            }
            else {
                if (s > 9)
                    return m + ':' + s
                else
                    return m + ':0' + s
            }
        }
        else {
            return '--:--';
        }
    }
})
    .filter('filterOutMySubscribedPlans', function () {

    return function (plans, subscribedPlan) {

        if (subscribedPlan.length != 0) {
            if (plans) {
                return plans.filter(function (plan) {
                    if (_.indexOf(subscribedPlan, plan._id) != -1) {
                        return false;
                    }
                    else {
                        return true;
                    }
                })
            }
        }
        else
            return plans;
    }

})
   



