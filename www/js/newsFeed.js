/*
Newsfeed Events
 Player
 New Registration
 Player updates profile information / picture
 Player subscribes to a video
 Player likes a video
 Player uploads a video
 Player subscribes to a training plan
 Player starts a training plan
 Player completes a training plan
 Player posts a comment on other player’s video
 Coach
 New Registration
 Coach updates profile information / picture
 Coach subscribes to a video
 Coach likes a video
 Coach uploads a video
 Coach uploads a training plan
 Coach subscribes to a training plan
 Coach provides review / feedback
 Coach posts a comment on player’s / other coach’s video
 */
//
// var newsFeed=[
//     {code:1,newsFeed:'Registration'},//done//tested
//     {code:2,newsFeed:'Player updates profile information / picture'},
//     {code:3,newsFeed:'Player subscribes to a video'},
//     {code:4,newsFeed:'Player likes a video'},//done
//     {code:5,newsFeed:'Player uploads a video'},//done//tested
//     {code:6,newsFeed:'Player subscribes to a training plan'},//done
//     {code:7,newsFeed:'Player starts a training plan'},//done tested
//     {code:8,newsFeed:'Player completes a training plan'},//done tested
//     {code:9,newsFeed:'Player posts a comment on other player’s video'},//done//tested
//     {code:10,newsFeed:'Coach uploads a training plan'},//done//tested
//     {code:11,newsFeed:'Coach provides review / feedback'},//done
// ]


var newsFeed=[
    {code:1,newsFeed:' has joined Sparup.'},//done//tested
    {code:2,newsFeed:'Player updates profile information / picture'},
    {code:3,newsFeed:'Player subscribes to a video'},
    {code:4,newsFeed:' has liked a video '},//done
    {code:5,newsFeed:' has uploaded a video'},//done//tested
    {code:6,newsFeed:' has subscribed to a training plan'},//done
    {code:7,newsFeed:' has started a training plan'},//done tested
    {code:8,newsFeed:' has completed a training plan'},//done tested
    {code:9,newsFeed:' has posted a comment on video'},//done//tested
    {code:10,newsFeed:' has uploaded a training plan'},//done//tested
    {code:11,newsFeed:' has reviewed '},//done
]