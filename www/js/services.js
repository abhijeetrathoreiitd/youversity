angular.module('starter')


//factory SERVICES


    .factory("loggedInUser", function () {
        return {}
    })
    .factory("myStats", function () {
        return {
            list: {}
        }
    })
    .factory("SportsList", function () {
        return {
            list: []
        }
    })
    .factory("sportsPreExperience", function () {
        return {
            list: []
        }
    })
    .factory("sportsExperience", function () {
        return {
            list: []
        }
    })


    .factory("Unit1", function () {
        return {units: [ 'Km',
            'Km/hour',
            'Times',
            'Times/hour',
            'Times/min',
            'Hours',
            'Minutes',
            'Seconds']}
    })
    .factory("Unit2", function () {
        return {units: ['hour', 'minutes', 'service']}
    })
    .factory("addSportsExperience", function () {
        return {}
    })
    .factory("allLevelTagsList", function () {
        return {}
    })
    .factory("myNewsFeed", function () {
        return {
            newsFeedList: []
        }
    })
    .factory("trainingPlanCategoryList", function () {
        return {
            category: [],
            sportsList: []
        }
    })
    .factory("trainingPlanParams", function () {// stores the sports of player/coach
        return {}
    })
    .factory("userProfileParams", function () {// stores the sports of player/coach
        return {}
    })
    .factory("currentWeekParams", function () {// stores the sports of player/coach
        return {
            week: 0
        }
    })// used to store the value of week selected in myTrainingDetails and trainingDetails page.
    .factory("myTrainingParams", function () {// stores the sports of player/coach
        return {}
    })
    .factory("mySports", function () {// stores the sports of player/coach
        return {
            sports: []
        }
    })
    .factory("playerTrainingPlanList", function () {// stores the sports of player/coach
        return {
            list: [],
            lastFetchTime: null,
            trainingPlanIds: []
        }
    })
    .factory("coachTrainingPlanList", function () {// stores the sports of player/coach
        return {
            list: [],
            lastFetchTime: null,
            trainingPlanIds: []
        }
    })
    .factory("ActivityListByTrainingPlan", function () {// stores the sports of player/coach
        return {
            list: []
        }
    })
    .factory("fetchedVideoInfo", function () {// Stores fetched videos info
        return {
            info: []
        }
    })
    .factory("fetchedVideoList", function () {// Stores Videos feed
        return {
            video: []
        }
    })
    .factory("subscribedVideoList", function () {// Stores Videos feed
        return {
            video: []
        }
    })
    .factory("subscribedVideoPackage", function () {// Stores Videos feed
        return {
            video: []
        }
    })
    .factory("subscribedVideoPackage", function () {// Stores Videos feed
        return {
            video: []
        }
    })
    .factory('playerUploadedVideos', function () {
        return {
            videosList: []
        }
    })


    //service to users viewed videos, liked and disliked


    .factory('playerVideoStats', function () {
        return {
            viewedVideos: [],
            likedVideos: [],
            dislikedVideos: []
        }
    })
    .factory('Subscribed', function () {
        return {}
    })
    .factory('myBookmarkedVideo', function () {
        return {
            bookmarkedVideos: []
        }
    })
    .factory('searchByTitle', function () {
        return {
            titles: []
        }
    })
    .factory('videoForReview', function () {
        return {
            videoList: [],
            lastVideoFetchTime: ''
        }
    })
    .factory('findPersonResult', function () {
        return {
            coaches: []
        }
    })
    .factory('findPlayerResult', function () {
        return {
            players: []
        }
    })
    .factory('Following', function () {
        return {}
    })


    //RESOURCE SERVICES


    .factory('AppReview', function ($resource, backend) {
        return $resource(backend + '/appReview', null); // Note the full endpoint address
    })
    .factory('NewsFeed', function ($resource, backend) {
        return $resource(backend + '/newsfeed?newFetchTime=:newFetchTime&oldFetchTime=:oldFetchTime&fetchCase=:fetchCase&userId=:userId', null); // Note the full endpoint address
    })
    .factory('trainingPlan', function ($resource, backend) {
        return $resource(backend + '/trainingPlan?sportId=:sportId&categoryId=:categoryId', null,
            {
                'coachTrainingPlan': {
                    method: 'GET',
                    url: backend + '/coachTrainingPlan?coachId=:coachId',
                    isArray: true
                }
            }); // Note the full endpoint address
    })
    .factory('trainingPlanCategory', function ($resource, backend) {
        return $resource(backend + '/trainingPlanCategory?sportId=:sportId'); // Note the full endpoint address
    })
    .factory('getVideoStats', function ($resource, backend) {
        return $resource(backend + '/videoStats?userId=:userId', {userId: 'userId'}); // Note the full endpoint address
    })
    .factory('likeVideo', function ($resource, backend) {
        return $resource(backend + '/likeVideo?userId=:userId&videoId=:videoId&like=:like&dislike=:dislike'); // Note the full endpoint address
    })
    .factory('dislikeVideo', function ($resource, backend) {
        return $resource(backend + '/dislikeVideo?userId=:userId&videoId=:videoId&dislike=:dislike&like=:like'); // Note the full endpoint address
    })
    .factory('viewVideo', function ($resource, backend) {
        return $resource(backend + '/viewVideo?userId=:userId&videoId=:videoId&view=:view'); // Note the full endpoint address
    })
    .factory('User', function ($resource, backend) {
        return $resource(backend + '/user/:id', null,
            {
                'fbSignUp': {
                    method: 'POST',
                    url: backend + '/user/fbSignUp',
                    isArray: false
                },
                'checkType': {
                    method: 'POST',
                    url: backend + '/userType',
                    isArray: false
                }
            }); // Note the full endpoint address
    })
    .factory('Login', function ($resource, backend) {
        return $resource(backend + '/login/:id'); // Note the full endpoint address
    })
    .factory('SportExperience', function ($resource, backend) {
        return $resource(backend + '/sportExperience/:id'); // Note the full endpoint address
    })
    .factory('Video', function ($resource, backend) {
        return $resource(backend + '/video?userId=:userId&lastVideoFetchTime=:lastVideoFetchTime', {
                userId: 'userId',
                lastVideoFetchTime: 'lastVideoFetchTime'
            },
            {
                'getVideoBySportId': {//This api does not fetch video uploaded by loggedIn Player
                    method: 'GET',
                    url: backend + '/getVideoBySportId?sportId=:sportId&lastVideoFetchTime=:lastVideoFetchTime&userId=:userId',
                    isArray: false
                },
                'searchVideoByTitle': {//This api does not fetch video uploaded by loggedIn Player
                    method: 'GET',
                    url: backend + '/searchVideoByTitle?sportId=:sportId&title=:title',
                    isArray: true
                }
            }); // Note the full endpoint address
    })
    .factory('Follow', function ($resource, backend) {
        return $resource(backend + '/follow/:userId', null,

            {
                'unfollow': {
                    method: 'GET',
                    url: backend + '/unfollow?userId=:userId&following=:following',
                    isArray: false
                }
            }); // Note the full endpoint address
    })
    .factory('Subscribe', function ($resource, backend) {
        return $resource(backend + '/subscribe/:userId', null, {userId: '@userId'}); // Note the full endpoint address
    })
    .factory('Bookmark', function ($resource, backend) {
        return $resource(backend + '/bookmark/:userId', null, {userId: '@userId'}); // Note the full endpoint address
    })
    .factory('Review', function ($resource, backend) {
        return $resource(backend + '/review', null,
            {
                'submitForReview': {
                    method: 'POST',
                    url: backend + '/submitForReview',
                    isArray: false
                },

                'getAllCoachesForReview': {
                    method: 'GET',
                    url: backend + '/getAllCoachesForReview?sportId=:sportId',
                    isArray: true
                },

                'getVideoForReview': {
                    method: 'POST',
                    url: backend + '/videoForReview',
                    isArray: false
                },
                'sendReview': {
                    method: 'PUT',
                    url: backend + '/submitReview',
                    isArray: false
                },
                'myReview': {
                    method: 'GET',
                    url: backend + '/myReview?coachId=:coachId',
                    isArray: false
                }
            }); // Note the full endpoint address
    })
    .factory('Package', function ($resource, backend) {
        return $resource(backend + '/package?sportId=:sportId&packageBy=:packageBy', null,
            {
                'updateVideoInPackage': {
                    method: 'PUT',
                    url: backend + '/updateVideoInPackage',
                    isArray: false
                }
            }
        ); // Note the full endpoint address
    })
    .factory('FindPerson', function ($resource, backend) {
        return $resource(backend + '/findperson?sportId=:sportId&searchText=:searchText', null,
            {}
        ); // Note the full endpoint address
    })
    .factory('experienceList', function ($resource, backend) {
        return $resource(backend + '/experienceList', null,
            {}
        ); // Note the full endpoint address
    })
    .factory('preexperienceList', function ($resource, backend) {
        return $resource(backend + '/preexperienceList', null,
            {}
        ); // Note the full endpoint address
    })
    .factory('Activity', function ($resource, backend) {
        return $resource(backend + '/activity?trainingPlanId=:trainingPlanId', null,
            {
                'updateActivityCoach': {
                    method: 'PUT',
                    url: backend + '/activity',
                    isArray: false
                }
            }
        ); // Note the full endpoint address
    })
    .factory('TrainingPlanList', function ($resource, backend) {
        return $resource(backend + '/myTrainingPlan?userId=:userId', null,
            {
                'updateTrainingPlanList': {
                    method: 'PUT',
                    url: backend + '/myTrainingPlan',
                    isArray: false
                },

                'myTrainingPlanUpdateStatus': {
                    method: 'GET',
                    url: backend + '/myTrainingPlanUpdateStatus?myPlanId=:myPlanId&planId=:planId&status=:status&updateStartTime=:updateStartTime&userId=:userId',
                    isArray: false
                },
                'rateMyTrainingPlan': {
                    method: 'GET',
                    url: backend + '/rateMyTrainingPlan?myPlanId=:myPlanId&rating=:rating&userId=:userId&trainingPlanId=:trainingPlanId&feedback=:feedback',
                    isArray: false
                }


            }
        ); // Note the full endpoint address
    })
    .factory('Comment', function ($resource, backend) {
        return $resource(backend + '/comment', null); // Note the full endpoint address
    })
    .factory('TagsAllLevel', function ($resource, backend) {
        return $resource(backend + '/getAllLevelTags', null); // Note the full endpoint address
    })
    .factory('GetUserStats', function ($resource, backend) {
        return $resource(backend + '/getUserStats?coachId=:coachId', null); // Note the full endpoint address
    })
    .factory('Sports', function ($resource, backend) {
        return $resource(backend + '/sports', null); // Note the full endpoint address
    })
    .factory('Tournament', function ($resource, backend) {
        return $resource(backend + '/tournament', null); // Note the full endpoint address
    })
    .factory('Academy', function ($resource, backend) {
        return $resource(backend + '/academy', null); // Note the full endpoint address
    })




    //LOGIN SERVICES


    .service('UserService', function () {
        // For the purpose of this example I will store user data on ionic local storage but you should save it on a database
        var setUser = function (user_data) {
            window.localStorage.starter_facebook_user = JSON.stringify(user_data);
        };

        var getUser = function () {
            return JSON.parse(window.localStorage.starter_facebook_user || '{}');
        };

        return {
            getUser: getUser,
            setUser: setUser
        };
    })
    .service('gmailLogin', function (loggedInUser, $rootScope, $localForage, $cordovaToast, $cordovaOauth, $http, $ionicLoading, User, $state, loginService) {

        var self = this;
        self.login = function () {
            var result1 = {};

            $cordovaOauth.google("218130774681-hljgd1e060oqs83rlo5oim39bsu3tb4p.apps.googleusercontent.com", ["email", "profile"])
                .then(function (result) {
                    result1 = result;
                }, function (error) {
                    console.log(error);
                })
                .then(function () {
                    var config = {
                        headers: {
                            'Authorization': 'Bearer ' + result1.access_token
                        }
                    };
                    $http.get('https://www.googleapis.com/oauth2/v2/userinfo', config).then(function (data) {
                        console.log('user data obtained');
                        User.checkType({email: data.data.email}, function (socialData) {
                            if (!socialData.socialLogin) {
                                console.log('social login doesnt exist');
                                self.confirmType(data.data);
                            }
                            else {
                                console.log('social login exists');
                                self.signup(data.data, socialData.player);
                            }

                        })
                    }, function (err) {
                        $ionicLoading.hide();
                        $cordovaToast.showShortBottom('Error in network').then(function (success) {
                            // success
                        }, function (error) {
                            // error
                        })
                    })
                })
        }
        self.confirmType = function (data) {
            var player = false;
            var onConfirm = function (buttonIndex) {
                if (buttonIndex == 1) {
                    player = false;
                    self.signup(data, player);
                }
                if (buttonIndex == 2) {
                    player = true;
                    self.signup(data, player);
                }
            }
            navigator.notification.confirm(
                'Please select your login profile type', // message
                onConfirm,            // callback to invoke with index of button pressed
                'Select Profile',           // title
                ['Coach', 'Player']     // buttonLabels
            );
        }
        self.signup = function (data, type) {
            loggedInUser.info = {
                'email': data.email,
                'userId': data.id,
                'name': data.name,
                'mySports': [],
                'player': type
            };
            User.fbSignUp({user: loggedInUser.info, player: type}, function (data) {
                console.log('fbsignup run');
                $localForage.setItem('loggedInUser', data.userData).then(function (forageData) {
                    loggedInUser.info = forageData;
                    loggedInUser.info.mySports = _.map(forageData.sportsExperience, 'sportId');//contains only list of sports ID
                });


                if (data.firstLogin) {
                    if (data.userData.player)
                        $state.go('sidemenu.chooseSport',{showButton:true});
                    else
                        $state.go('coachSidemenu.chooseSport',{showButton:true})
                }
                else {
                    console.log('not first login');
                    loginService.login();
                }
                $ionicLoading.hide();
            })
        }
    })

    .service("uploadAudioReviewService", function ($q, $ionicLoading, backend, Video, $cordovaToast, Review) {
        var service = {};
        service.uploadAudio = uploadAudio;
        return service;
        function uploadAudio(audioURL, videoReviewObject) {
            var deferred = $q.defer();
            var fileSize;
            var percentage;
            // Find out how big the original file is

            // Find out how big the original file is

            var extension = audioURL.split('.').pop();
            //if (extension == 'mov' || extension == 'mp4' || extension == 'MOV' || extension == 'MP4') {
            //    uploadFile();
            //}
            //else {
            //    $cordovaToast.showShortBottom('Upload Videos Only');
            //    $ionicLoading.hide();
            //}
            function uploadFile() {


                function win(r) {
                    console.log('upload success');
                    videoReviewObject.audioUrl = r.response
                    Review.sendReview(videoReviewObject, function (data) {
                        $cordovaToast.showShortBottom('Thanks for providing your feedback');
                    }, function (err) {
                        console.log(err);
                    })

                }

                function fail(error) {
                    alert("An error has occurred: Code = " + error.code);
                }

                var uri = encodeURI(backend + '/uploadAudioReview');

                var options = new FileUploadOptions();
                options.fileKey = "file";
                options.fileName = audioURL.substr(audioURL.lastIndexOf('/') + 1);
                options.mimeType = "audio/mp3";
                options.name = "file";

                var headers = {'headerParam': 'headerValue'};

                options.headers = headers;

                var ft = new FileTransfer();
                ft.onprogress = function (progressEvent) {
                    if (progressEvent.lengthComputable) {
                        var perc = Math.floor(progressEvent.loaded / progressEvent.total);
                        console.log(perc);

                    } else {

                    }
                };
                ft.upload(audioURL, uri, win, fail, options);

                var uploadOptions = {
                    params: {'upload_preset': "zhtib3yi"}
                };
            }

            uploadFile();

            return deferred.promise;
        }

    })
    .service("uploadService", function ($q,$rootScope, $ionicLoading, backend, Video, $cordovaToast, $timeout) {
        var service = {};

        service.uploadImage = uploadImage;
        return service;
        function uploadImage(imageURI, videoInfo) {
            $rootScope.percentageUploaded=0;
            var deferred = $q.defer();
            var fileSize;
            var percentage;
            // Find out how big the original file is

            // Find out how big the original file is

            var extension = imageURI.split('.').pop();
            if (extension == 'mov' || extension == 'mp4' || extension == 'MOV' || extension == 'MP4') {
                uploadFile();
            }
            else {
                $cordovaToast.showShortBottom('Upload Videos Only');
                $ionicLoading.hide();
            }
            function uploadFile() {


                function win(r) {
                    videoInfo.url = r.response;
                    Video.save({videoInfo: videoInfo}, function (data) {
                        console.log('video uploaded');
                        deferred.resolve(data);
                    }, function (err) {
                        console.log(err);
                    });

                }

                function fail(error) {
                    alert("An error has occurred: Code = " + error.code);
                }

                var uri = encodeURI(backend + '/uploadImage');

                var options = new FileUploadOptions();
                options.fileKey = "file";
                options.fileName = imageURI.substr(imageURI.lastIndexOf('/') + 1);
                options.mimeType = "video/mp4";
                options.name = "file";

                var headers = {'headerParam': 'headerValue'};

                options.headers = headers;

                var ft = new FileTransfer();
                ft.onprogress = function (progressEvent) {
                    if (progressEvent.lengthComputable) {
                        $timeout(function(){
                            $rootScope.percentageUploaded = progressEvent.loaded *100 / progressEvent.total;
                            console.log($rootScope.percentageUploaded) 
                        },100)
                    } else {

                    }
                };
                ft.upload(imageURI, uri, win, fail, options);

                var uploadOptions = {
                    params: {'upload_preset': "zhtib3yi"}
                };
            }

            return deferred.promise;
        }

    })
    .service('logoutService', function ($injector, $state, $rootScope, $ionicSideMenuDelegate, fetchedVideoInfo,
                                        fetchedVideoList, playerUploadedVideos, playerVideoStats, $localForage, mySports, $ionicLoading) {

        this.logout = function () {

            $localForage.clear();
            var services = ['loggedInUser', 'addSportsExperience']
            _.each(services, function (ser) {
                var test = $injector.get(ser)
                Object.keys(test).forEach(function (key) {
                    delete test[key];
                });
            })
            mySports.sports = [];
            fetchedVideoList.video = [];
            playerUploadedVideos.videosList = [];
            playerUploadedVideos.lastVideoFetchTime = ''
            playerVideoStats.viewedVideos = [];
            playerVideoStats.likedVideos = [];
            playerVideoStats.dislikedVideos = [];
            fetchedVideoInfo.info = [];
            $rootScope.sportSelected = null;

            if (window.cordova) {
                // If logged in from Facebook, logout from it
                facebookConnectPlugin.getLoginStatus(function (success) {
                    console.log('was logged in from facebook')
                    if (success.status === 'connected') {
                        facebookConnectPlugin.logout(function () {
                                console.log('facebook logout success')
                                $ionicLoading.hide();
                            },
                            function (fail) {
                                console.log('facebook logout error')
                                $ionicLoading.hide();
                            });
                    }
                })
            }

            $state.go('sidemenu.login');
            $ionicSideMenuDelegate.toggleLeft();
        }

    })
    .service('loginService', function (getVideoStats, Subscribe, loggedInUser, playerVideoStats, $ionicLoading,
                                       Subscribed, Follow, Following, $state, fetchedVideoInfo, mySports, SportsList, Bookmark, myBookmarkedVideo, createCalenderOnAppInstall) {

        this.login = function () {
            var promiseGetVideoStats = getVideoStats.get({userId: loggedInUser.info._id}, function (data) {

                    playerVideoStats.viewedVideos = data.viewedVideos;
                    playerVideoStats.dislikedVideos = data.dislikedVideos;
                    playerVideoStats.likedVideos = data.likedVideos;

                    $ionicLoading.hide();
                }
                , function (err) {
                    console.log(err)
                })

            promiseGetVideoStats['$promise'].then(function () {
                var subscribeVideoPromise = Subscribe.query({userId: loggedInUser.info._id}, function (data) {
                    Subscribed.Subscribed = _.map(data, 'subscribed')[0]
                }, function (err) {
                })

                subscribeVideoPromise['$promise'].then(function () {

                    _.each(SportsList.list, function (sport) {
                        _.each(loggedInUser.info.mySports, function (localSport) {
                            if (localSport == sport._id) {
                                mySports.sports.push(sport);
                                fetchedVideoInfo.info.push({'sportId': sport._id, 'lastVideoFetchTime': ''})
                            }
                        })
                    })
                }).then(function () {
                    Follow.get({userId: loggedInUser.info._id}, function (data) {
                        Following.list = data.following;
                        console.log(Following)
                    })
                }).then(function () {
                    var BookmarkPromise = Bookmark.query({userId: loggedInUser.info._id}, function (data) {
                            myBookmarkedVideo.bookmarkedVideos = data;

                        },
                        function (err) {
                            console.log(err);
                        })

                    BookmarkPromise['$promise'].then(function () {
                        if (mySports.sports.length == 0) {
                            if ((loggedInUser.info.player))
                                $state.go('sidemenu.chooseSport',{showButton:true});
                            else
                                $state.go('coachSidemenu.chooseSport',{showButton:true})
                        }
                        else {
                            if (loggedInUser.info.player)
                                $state.go('sidemenu.tabs.training');
                            else
                                $state.go('coachSidemenu.tabs.training');
                        }

                    }).then(function () {
                        createCalenderOnAppInstall.createCalender();
                    })

                })
            })
        }
    })
    .service('ConvertYoutubeDuration', function ($http, $q) {
        this.convertTime = function (videoId) {

            var defer = $q.defer()
            var duration;
            $http.get('https://www.googleapis.com/youtube/v3/videos?id=' + videoId + '&part=contentDetails&key=AIzaSyA4prsLwSg6lOQ_IsLSUdM0Ox7QLNCHZrs').then(function (data) {
                duration = data.data.items[0].contentDetails.duration;


            }).then(function () {

                var a = duration.match(/\d+/g);

                if (duration.indexOf('M') >= 0 && duration.indexOf('H') == -1 && duration.indexOf('S') == -1) {
                    a = [0, a[0], 0];
                }

                if (duration.indexOf('H') >= 0 && duration.indexOf('M') == -1) {
                    a = [a[0], 0, a[1]];
                }
                if (duration.indexOf('H') >= 0 && duration.indexOf('M') == -1 && duration.indexOf('S') == -1) {
                    a = [a[0], 0, 0];
                }

                duration = 0;

                if (a.length == 3) {
                    duration = duration + parseInt(a[0]) * 3600;
                    duration = duration + parseInt(a[1]) * 60;
                    duration = duration + parseInt(a[2]);
                }

                if (a.length == 2) {
                    duration = duration + parseInt(a[0]) * 60;
                    duration = duration + parseInt(a[1]);
                }

                if (a.length == 1) {
                    duration = duration + parseInt(a[0]);
                }
                // return duration
                defer.resolve(duration);
            })
            return defer.promise;

        }
    })
    .service("uploadThumbnail", function ($q, $ionicLoading, backend, $cordovaToast) {
        var service = {};
        service.uploadImage = uploadImage;
        return service;
        function uploadImage(imageURI) {
            var deferred = $q.defer();
            var fileSize;
            var percentage;
            uploadFile();
            function uploadFile() {
                function win(r) {
                    deferred.resolve(r.response);
                    $ionicLoading.hide();
                    // $cordovaToast.showShortCenter('Image uploaded successfully')
                }

                function fail(error) {
                    // alert("An error has occurred: Code = " + error.code);
                    $cordovaToast.showShortCenter('Error in image upload');
                }

                var uri = encodeURI(backend + '/uploadThumbnail');

                var options = new FileUploadOptions();
                options.fileKey = "file";
                options.fileName = imageURI.substr(imageURI.lastIndexOf('/') + 1);
                options.mimeType = "image/jpeg";
                options.name = "file";

                var headers = {'headerParam': 'headerValue'};

                options.headers = headers;

                var ft = new FileTransfer();
                ft.onprogress = function (progressEvent) {
                    if (progressEvent.lengthComputable) {
                        var perc = Math.floor(progressEvent.loaded / progressEvent.total);

                    }
                };

                ft.upload(imageURI, uri, win, fail, options);

                var uploadOptions = {
                    params: {'upload_preset': "zhtib3yi"}
                };


            }

            return deferred.promise;
        }

    })
    .service("createCalenderOnAppInstall", function ($cordovaCalendar, calenderName) {


        this.createCalender = function () {

            if (window.cordova) {
                $cordovaCalendar.listCalendars().then(function (result) {
                    // success
                    if (_.indexOf(_.map(result, 'name'), calenderName) == -1) {

                        $cordovaCalendar.createCalendar({
                            calendarName: calenderName,
                            calendarColor: '#FF0000'
                        }).then(function (result) {
                        }, function (err) {

                            console.log(err)
                        });

                        console.log('calender created');
                    }
                    else {
                        console.log('calender exists');
                    }

                }, function (err) {
                    // error
                });
            }
        }
    })



