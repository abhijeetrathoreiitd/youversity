/**
 * Created by youstart on 30-Aug-16.
 */
var sportsList =
    [
        {
            "_id": "57c006ecc6740498040294bf",
            "sportName": "table tennis",
            "refToTags": ["57c009f4c651e61827e0f5dc", "57c00a04c651e61827e0f5dd", "57c00a11c651e61827e0f5de"]
        },
        {
            "_id": "57c006afc6740498040294ba",
            "sportName": "badminton",
            "refToTags": ["57c00968c651e61827e0f5d9", "57c0099fc651e61827e0f5da", "57c009acc651e61827e0f5db"]
        },
        {
            "_id": "57c006e7c6740498040294be",
            "sportName": "lawn tennis",
            "refToTags": ["57c00a87c651e61827e0f5e0", "57c00ac4c651e61827e0f5e1", "57c00ad4c651e61827e0f5e2", "57c00ad9c651e61827e0f5e3"]
        },
        {
            "_id": "57c006e1c6740498040294bd",
            "sportName": "basketball",
            "refToTags": ["57c00b1ec651e61827e0f5e4", "57c00b2ec651e61827e0f5e5", "57c00b40c651e61827e0f5e6", "57c00b4dc651e61827e0f5e7"]
        },
        {
            "_id": "57c006d4c6740498040294bc",
            "sportName": "volleyball",
            "refToTags": ["57c00baec651e61827e0f5e8", "57c00bb2c651e61827e0f5e9", "57c00bb7c651e61827e0f5ea", "57c00bc3c651e61827e0f5eb"]
        },
        {
            "_id": "57c006c4c6740498040294bb",
            "sportName": "football",
            "refToTags": ["57c00c15c651e61827e0f5ec", "57c00c21c651e61827e0f5ed", "57c00c28c651e61827e0f5ee", "57c00c2fc651e61827e0f5ef"]
        }
    ]

// var sportsPreExperience=[
// "Just learn the sport",
// "Prepare for a tournament",
// "Improve specific skills",
// "Enhance my sports fitness",
// "Learn from various coaches",
// "Improve my tournament (game-day) performance"
// ]
// var sportsExperience=
// [
// "Never played the game, trying for first time",
// "Been playing for a few months with my friends",
// "Ready to challenge decent players on courts",
// "Ready to play an open tournament",
// "Need to improve game in next tournament",
// "I am ready for state/national level",
// ]


var tagList =
    [
        {"_id": "57c00968c651e61827e0f5d9", "tagName": "alley", "sportId": "57c006afc6740498040294ba"},
        {"_id": "57c0099fc651e61827e0f5da", "tagName": "balk", "sportId": "57c006afc6740498040294ba"},
        {"_id": "57c009acc651e61827e0f5db", "tagName": "backcourt", "sportId": "57c006afc6740498040294ba"},
        {"_id": "57c009f4c651e61827e0f5dc", "tagName": "stroke", "sportId": "57c006ecc6740498040294bf"},
        {"_id": "57c00a04c651e61827e0f5dd", "tagName": "topspin", "sportId": "57c006ecc6740498040294bf"},
        {"_id": "57c00a11c651e61827e0f5de", "tagName": "twiddle", "sportId": "57c006ecc6740498040294bf"},
        {"_id": "57c00a6ac651e61827e0f5df", "tagName": "alley", "sportId": "57c006ecc6740498040294be"},
        {"_id": "57c00a87c651e61827e0f5e0", "tagName": "alley", "sportId": "57c006e7c6740498040294be"},
        {"_id": "57c00ac4c651e61827e0f5e1", "tagName": "deuce court", "sportId": "57c006e7c6740498040294be"},
        {"_id": "57c00ad4c651e61827e0f5e2", "tagName": "linesman", "sportId": "57c006e7c6740498040294be"},
        {"_id": "57c00ad9c651e61827e0f5e3", "tagName": "forecourt", "sportId": "57c006e7c6740498040294be"},
        {"_id": "57c00b1ec651e61827e0f5e4", "tagName": "alley-oop", "sportId": "57c006e1c6740498040294bd"},
        {"_id": "57c00b2ec651e61827e0f5e5", "tagName": "buzzer beater", "sportId": "57c006e1c6740498040294bd"},
        {"_id": "57c00b40c651e61827e0f5e6", "tagName": "field goal", "sportId": "57c006e1c6740498040294bd"},
        {"_id": "57c00b4dc651e61827e0f5e7", "tagName": "fast break", "sportId": "57c006e1c6740498040294bd"},
        {"_id": "57c00baec651e61827e0f5e8", "tagName": "ace", "sportId": "57c006d4c6740498040294bc"},
        {"_id": "57c00bb2c651e61827e0f5e9", "tagName": "assist", "sportId": "57c006d4c6740498040294bc"},
        {"_id": "57c00bb7c651e61827e0f5ea", "tagName": "attack", "sportId": "57c006d4c6740498040294bc"},
        {"_id": "57c00bc3c651e61827e0f5eb", "tagName": "attack error", "sportId": "57c006d4c6740498040294bc"},
        {"_id": "57c00c15c651e61827e0f5ec", "tagName": "fumble", "sportId": "57c006c4c6740498040294bb"},
        {"_id": "57c00c21c651e61827e0f5ed", "tagName": "punk", "sportId": "57c006c4c6740498040294bb"},
        {"_id": "57c00c28c651e61827e0f5ee", "tagName": "kickoff", "sportId": "57c006c4c6740498040294bb"},
        {"_id": "57c00c2fc651e61827e0f5ef", "tagName": "sack", "sportId": "57c006c4c6740498040294bb"}
    ]


        [
        {experience: "Never played the game, trying for first time", title: "Beginner - I"},
        {experience: "Been playing for a few months with my friends", title: "Beginner - II"},
        {experience: "Ready to challenge decent players on courts", title: "Intermediate  - I"},
        {experience: "Ready to play an open tournament", title: "Intermediate  - II"},
        {experience: "Need to improve game in next tournament", title: "Expert - I"},
        {experience: "I am ready for state/national level", title: "Expert - II"}
        ]