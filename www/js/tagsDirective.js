angular.module('starter')
.directive('tagsModal', function ($ionicModal,$rootScope,allLevelTagsList,$timeout) {
    return {
        restrict: 'A',
        template:'',
        scope: {
            itemInfo: '=',
            deltaSelectedTags:'=',
            deltaSelectedTagsIds:'=',
            showLevel2Div:'=',
            showLevel3Div:'=',
        },
        link: function (scope,elem) {
            console.log('tags directive');


            var TagsList={};
            var currentTagLevel1='';
            var currentTagLevel2='';
            var currentTagLevel3='';
            scope.currentLevel = 0;
            showTagsModal = function () {

                console.log(scope.itemInfo)

                $rootScope.showTagsDirective=false;

                $ionicModal.fromTemplateUrl('templates/player/applyTagsModal.html', {
                    scope: scope,
                    animation: 'slide-in-up'
                })
                    .then(function (modal) {
                        applyTagsModal = modal;
                        console.log('upload videos modal');
                        TagsList = allLevelTagsList.list;

                        applyTagsModal.show();
                    }).then(function()
                {
                    scope.getLevel1Tags($rootScope.sportSelected._id);});
            };
            // scope.showTagsModal();

            elem.bind('click',showTagsModal);

            scope.hideTagsModal = function () {

                applyTagsModal.hide();
                $timeout(function(){$rootScope.showTagsDirective=true},100);

            };

            scope.getLevel3Tags = function (tagInfo, add) {
                scope.currentLevel = 2;

                if(add){
                    scope.addLevel2Tag(currentTagLevel2)
                }
                else {
                    _.each(scope.level2tags, function (tag) {
                        if (tag._id == tagInfo._id) {
                            tag.selected = true;
                        }
                        else
                            tag.selected = false;
                    })

                    scope.level3tags = _.filter(TagsList.level3Tags, function (tag) {
                        tag.selected = false;
                        return tag.tagLevel2Id == tagInfo._id;
                    })

                    scope.showLevel3Div = true;
                    currentTagLevel2 = tagInfo;
                }
            };
            scope.getLevel2Tags = function (tagInfo, add) {
                scope.currentLevel = 1;

                if(add){
                    scope.addLevel1Tag(currentTagLevel1);
                }
                else {
                    _.each(scope.level1tags, function (tag) {
                        if (tag._id == tagInfo._id) {
                            tag.selected = true;
                        }
                        else
                            tag.selected = false;
                    })


                    scope.level2tags = _.filter(TagsList.level2Tags, function (tag) {
                        tag.selected = false;
                        return tag.tagLevel1Id == tagInfo._id;
                    })
                    scope.level3tags = [];
                    scope.showLevel2Div = true;
                    scope.showLevel3Div = false;
                    currentTagLevel1 = tagInfo;
                }
            };
            scope.getLevel1Tags = function (sportId) {

                scope.level1tags = _.filter(TagsList.level1Tags, function (tag) {
                    tag.selected = false;
                    return tag.sportId == sportId;
                })
                scope.showLevel2Div = false;
                scope.showLevel3Div = false;

            };
            scope.addLevel1Tag = function (tagInfo) {
                var index = _.findIndex(scope.deltaSelectedTags, function (tags) {
                    return tagInfo._id == tags.level1;
                })

                if (index == -1) {
                    scope.deltaSelectedTags.push({level1: tagInfo._id, level1Tag: tagInfo.tagLevel1});
                    scope.deltaSelectedTagsIds.level1.push(tagInfo._id);
                }

            };
            scope.addLevel2Tag = function (tagInfo) {
                scope.addLevel1Tag(currentTagLevel1);
                var index = _.findIndex(scope.deltaSelectedTags, function (tags) {
                    return tagInfo.tagLevel1Id == tags.level1;
                })

                if (index != -1) {

                    if (typeof scope.deltaSelectedTags[index].level2TagsList == 'undefined')//checking whether it already has some tags or not
                        scope.deltaSelectedTags[index].level2TagsList = [];

                    var tagIndex = _.findIndex(scope.deltaSelectedTags[index].level2TagsList, function (tags) {
                        return tagInfo._id == tags.level2;
                    })
                    if (tagIndex == -1) {
                        scope.deltaSelectedTags[index].level2TagsList.push({
                            level2: tagInfo._id,
                            level2Tag: tagInfo.tagLevel2
                        });
                        scope.deltaSelectedTagsIds.level2.push(tagInfo._id)
                    }
                }


            };
            scope.highlightLevel3Tag = function(tagInfo){
                scope.currentLevel = 3;
                tagInfo.selected=!tagInfo.selected;
                currentTagLevel3 = tagInfo;
            }
            scope.addLevel3Tag = function () {
                scope.addLevel2Tag(currentTagLevel2);

                _.each(scope.level3tags, function (tag) {
                        if (tag._id == currentTagLevel3._id) {
                            tag.selected = true;
                        }
                        else
                            tag.selected = false;
                    })
                    var level2Index = -1, level1Index = -1;
                    _.each(scope.deltaSelectedTags, function (level1, index) {
                        if (level2Index == -1) {
                            level2Index = _.findIndex(level1.level2TagsList, function (level2) {
                                if (level2.level2 == currentTagLevel3.tagLevel2Id) {
                                    level1Index = index;
                                    return level2;
                                }
                            })

                            if (level1Index != -1) {
                                if (typeof scope.deltaSelectedTags[level1Index].level2TagsList[level2Index].level3TagsList == 'undefined')//checking whether it already has some tags or not
                                {
                                    scope.deltaSelectedTags[level1Index].level2TagsList[level2Index].level3TagsList = [];
                                }


                                var tagIndex = _.findIndex(scope.deltaSelectedTags[level1Index].level2TagsList[level2Index].level3TagsList, function (tags) {
                                    return currentTagLevel3._id == tags.level3;
                                })
                                if (tagIndex == -1) {
                                    scope.deltaSelectedTags[level1Index].level2TagsList[level2Index].level3TagsList.push(
                                        {
                                            level3: currentTagLevel3._id,
                                            level3Tag: currentTagLevel3.tagLevel3
                                        }
                                    )

                                    scope.deltaSelectedTagsIds.level3.push(currentTagLevel3._id)
                                }
                            }
                        }
                    })
                

            };
            scope.removeLevel1Tags = function (removeLevel1TagId) {
                scope.deltaSelectedTags = _.reject(scope.deltaSelectedTags, function (tags) {
                    return tags.level1 == removeLevel1TagId;
                })
                scope.deltaSelectedTagsIds.level1 = _.reject(scope.deltaSelectedTagsIds.level1, function (tags) {
                    return tags == removeLevel1TagId;
                })

            };
            scope.removeLevel2Tags = function (removeLevel1TagId, removeLevel2TagId) {
                var indexLevel1 = _.findIndex(scope.deltaSelectedTags, function (tags) {
                    return tags.level1 == removeLevel1TagId;
                })

                scope.deltaSelectedTags[indexLevel1].level2TagsList = _.reject(scope.deltaSelectedTags[indexLevel1].level2TagsList, function (tags) {
                    return tags.level2 == removeLevel2TagId;
                })


                scope.deltaSelectedTagsIds.level2 = _.reject(scope.deltaSelectedTagsIds.level2, function (tags) {
                    return tags == removeLevel2TagId;
                })
            };
            scope.removeLevel3Tags = function (removeLevel1TagId, removeLevel2TagId, removeLevel3TagId) {

                var indexLevel1 = _.findIndex(scope.deltaSelectedTags, function (tags) {
                    return tags.level1 == removeLevel1TagId;
                })

                var indexLevel2 = _.findIndex(scope.deltaSelectedTags[indexLevel1].level2TagsList, function (tags) {
                    return tags.level2 == removeLevel2TagId;
                })

                scope.deltaSelectedTags[indexLevel1].level2TagsList[indexLevel2].level3TagsList =
                    _.reject(scope.deltaSelectedTags[indexLevel1].level2TagsList[indexLevel2].level3TagsList, function (tags) {
                        return tags.level3 == removeLevel3TagId;
                    })

                scope.deltaSelectedTagsIds.level3 = _.reject(scope.deltaSelectedTagsIds.level3, function (tags) {
                    return tags == removeLevel3TagId;
                })
            };
            scope.applyTags=function(){
                
                scope.itemInfo.tagsLevel1=scope.deltaSelectedTagsIds.level1;
                scope.itemInfo.tagsLevel2=scope.deltaSelectedTagsIds.level2;
                scope.itemInfo.tagsLevel3=scope.deltaSelectedTagsIds.level3;
                console.log(scope.itemInfo.tagsLevel1);
                if(scope.itemInfo.tagsLevel1.length==0){
                    $rootScope.showTagsDirective=false;
                }
                scope.hideTagsModal();

            };

        }
    };
})




















